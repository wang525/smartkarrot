import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SurveyConstants } from '../survey.constants';
import { ImageUploadService } from '../../../../core/service/image-upload/image-upload.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as _ from 'lodash';

@Component({
  selector: 'app-config-thankyou',
  templateUrl: './config-thankyou.component.html',
  styleUrls: ['./config-thankyou.component.scss'],
  providers: [ImageUploadService]
})
export class ConfigThankyouComponent implements OnInit {
  showError: boolean;

  constructor(private imageUploadSrvc: ImageUploadService, private _sanitizer: DomSanitizer) { }

  fontSelected: any = {};
  fontList: Array<object> = [];
  @Input() thankyouData;
  @Input() isWebApp;
  @Input() surveyType: string;
  @Input() surveyName: string;

  @Output() surveyNameChange = new EventEmitter();
  @Output() tabSelectedChange = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();
  isDraft: boolean = false;

  ngOnInit() {
    this.surveyType = this.surveyType ? this.surveyType : 'survey';
    this.fontList = [...SurveyConstants.FontList];
    this.fontSelected = _.filter(this.fontList, { key: this.thankyouData.fontStyle })[0];
  }

  onFontSelect(event) {
    this.fontSelected = _.filter(this.fontList, { key: event })[0];
  }

  fileChange(event) {
    let file: File = event.target.files[0];
    if (file['type'] == 'image/jpeg' || file.type == 'image/png')
      this.uploadFile(file);
    else
      alert('Please upload Jpeg or Png format. ')
  }

  uploadFile(file) {
    this.thankyouData.imageUrl = this._sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));

    this.imageUploadSrvc.getImageUrl().subscribe(async (resp: any) => {
      await this.imageUploadSrvc.uploadImageUrl(resp.url, file)
      this.thankyouData.imageUrl = this.imageUploadSrvc.getImageLoadUrl() + resp.key;
    });
  }

  saveSurvey() {
    this.showError = !this.surveyName || (/^ *$/.test(this.surveyName)) ? true : false;
    !this.showError && this.save.emit();
  }

  cancelSurvey() {
    this.cancel.emit();
  }

  onNameChange(event) {
    this.showError = !event ? true : false;
    this.surveyNameChange.emit(event)
  }

  pageChange(event) {
    this.tabSelectedChange.emit(event)
  }

}
