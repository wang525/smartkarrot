import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SurveyConstants } from '../survey.constants';
import { ImageUploadService } from '../../../../core/service/image-upload/image-upload.service';
import { DomSanitizer } from '@angular/platform-browser';
import * as _ from 'lodash';

@Component({
  selector: 'app-config-question',
  templateUrl: './config-question.component.html',
  styleUrls: ['./config-question.component.scss'],
  providers: [ImageUploadService]
})
export class ConfigQuestionComponent implements OnInit {

  fontList: any[] = [];
  fontSelected: any = {};
  showError: boolean;
  isDraft: boolean = false;
  @Input() questionData;
  @Input() surveyName: string;
  @Input() surveyType: string;
  @Input() isWebApp;
  @Input() question;
  @Output() tabSelectedChange = new EventEmitter();
  @Output() surveyNameChange = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();

  constructor(private imageUploadSrvc: ImageUploadService, private _sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.fontList = [...SurveyConstants.FontList];
    this.fontSelected = _.filter(this.fontList, { key: this.questionData.fontStyle })[0];
    this.surveyType = this.surveyType ? this.surveyType : 'survey';
  }

  onFontSelect(event) {
    this.fontSelected = _.filter(this.fontList, { key: event })[0];
  }

  saveSurvey() {
    this.showError = !this.surveyName || (/^ *$/.test(this.surveyName)) ? true : false;
    !this.showError && this.save.emit();
  }
  cancelSurvey() {
    this.cancel.emit();
  }

  onNameChange(event) {
    this.showError = !event ? true : false;
    this.surveyNameChange.emit(event)
  }

  pageChange(event) {
    this.tabSelectedChange.emit(event)
  }

  onChange(event) {
    this.question = Object.assign({}, event)
  }


  fileChange(event) {
    let file: File = event.target.files[0];
    if (file['type'] == 'image/jpeg' || file.type == 'image/png')
      this.uploadFile(file);
    else
      alert('Please upload Jpeg or Png format. ')
  }

  uploadFile(file) {
    this.questionData.imageUrl = this._sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));

    this.imageUploadSrvc.getImageUrl().subscribe(async (resp: any) => {
      await this.imageUploadSrvc.uploadImageUrl(resp.url, file)
      this.questionData.imageUrl = this.imageUploadSrvc.getImageLoadUrl() + resp.key;
    });
  }
}
