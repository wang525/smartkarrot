import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "../../../core/service/common/common.service";
import { ReferralService } from "../../../core/service/referral/referral.service";
declare var $;

@Component({
  selector: "app-referral-list",
  templateUrl: "./referral-list.component.html",
  styleUrls: ["./referral-list.component.scss"]
})
export class ReferralListComponent implements OnInit {
  @Output() editRefData = new EventEmitter();
  hideBar: boolean;
  headName = [
    { name: "Referral", url: "/features/referral-list", active: true }
  ];

  listReferralData: any;
  referralIdStatus: any;
  deleteReferralRule: any;
  deleteId: any;
  path: any;
  isDrafted = "";

  singleReferralData: any;
  duplicateReferralData: any;

  constructor(
    public commonService: CommonService,
    public referralService: ReferralService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.hideBar = false;
  }

  ngOnInit() {
    $("body").removeClass("modal-open");
    $(".modal-backdrop").remove();

    this.commonService.setHeaderName(this.headName);

    /* get list user in segments from api  */
    this.referralService.getListReferral().then(res => {
      this.listReferralData = res["referralRules"];
      console.log(res["referralRules"]);
      console.log(res);

      for (var i = 0; i < this.listReferralData.length; i++) {
        var date = new Date();
        var startDate = new Date(this.listReferralData[i].startDate),
          mnth = ("0" + (startDate.getMonth() + 1)).slice(-2),
          day = ("0" + startDate.getDate()).slice(-2);
        var startSaveDate = [day, mnth, startDate.getFullYear()].join("-");
        var endSaveDate;
        if (this.listReferralData[i].endDate) {
          var endDate = new Date(this.listReferralData[i].endDate),
            mnth = ("0" + (endDate.getMonth() + 1)).slice(-2),
            day = ("0" + endDate.getDate()).slice(-2);
          endSaveDate = [day, mnth, endDate.getFullYear()].join("-");
        } else {
          endSaveDate = "-";
        }

        this.listReferralData[i].startDate = startSaveDate;
        this.listReferralData[i].endDate = endSaveDate;
      }
    });
    /* get the segment id of the individual segment */
    this.path = this.route.params.subscribe(params => {
      this.isDrafted = params["id"];
      console.log("this.isDrafted" + this.isDrafted);
    });
  }
  // Stop playing youtube video
  stopPlayingVideo() {
    $("#YtPlayerModal").on("hidden.bs.modal", function(e) {
      $("#YtPlayerModal iframe").attr(
        "src",
        $("#YtPlayerModal iframe").attr("src")
      );
    });
  }

  /* go to the Referral Details page */
  gotoReferralDetail() {
    this.router.navigate(["/features/referral-details"]);
  }

  /* save referral data */
  saveReferralData() {}

  /* change the status from/to Enable to/from Disable */
  changeStatus(referralId, status) {
    let data = {};
    data["referralRuleId"] = referralId;
    data["ruleStatus"] = status;
    this.referralService.putReferralIdStatus(data).then(res => {
      this.referralIdStatus = res;
      let referralIdData = this.listReferralData.filter(elem => {
        return elem.referralRuleId == referralId;
      });

      referralIdData[0]["isDraft"] = status;
    });
  }

  /* edit referral */
  editReferral(id) {
    this.router.navigate(["features/referral-details", id]);
    // this.editRefData.emit();
  }

  /* duplicate referral rule */
  duplicateReferral(id) {
    let data = {};
    data["referralRuleId"] = id;
    /* get the individual referral rule */
    this.referralService.getSingleReferralRule(data).then(res => {
      this.singleReferralData = res;
      delete this.singleReferralData.referralRuleId;
      delete this.singleReferralData.createdAt;
      let refName = this.singleReferralData["programId"];
      this.duplicateReferralData = this.singleReferralData;
      this.duplicateReferralData["programId"] = "Copy of <" + refName + ">";

      /* update the referral rule */
      this.referralService
        .sendSaveReferrals(this.duplicateReferralData)
        .then(res => {
          if (res == "SUCCESS") {
            /* get referral list */
            this.referralService.getListReferral().then(res => {
              this.listReferralData = res["referralRules"];
              for (var i = 0; i < this.listReferralData.length; i++) {
                var date = new Date();
                var startDate = new Date(this.listReferralData[i].startDate),
                  mnth = ("0" + (startDate.getMonth() + 1)).slice(-2),
                  day = ("0" + startDate.getDate()).slice(-2);
                var startSaveDate = [day, mnth, startDate.getFullYear()].join(
                  "-"
                );

                var endSaveDate;
                if (this.listReferralData[i].endDate) {
                  var endDate = new Date(this.listReferralData[i].endDate),
                    mnth = ("0" + (endDate.getMonth() + 1)).slice(-2),
                    day = ("0" + endDate.getDate()).slice(-2);
                  endSaveDate = [day, mnth, endDate.getFullYear()].join("-");
                } else {
                  endSaveDate = "-";
                }
                this.listReferralData[i].startDate = startSaveDate;
                this.listReferralData[i].endDate = endSaveDate;
              }
            });
          } else {
            console.log("duplicate data failed.");
            $("#failDuplicateReferral").trigger("click");
          }
        });
    });
  }

  /* delete individual referral ID */
  deleteReferral(id) {
    this.deleteId = id;
    $("#deleteReferral").trigger("click");
  }

  deletSelectedReferral($event) {
    $event.preventDefault();

    let data = {};
    data["referralRuleId"] = this.deleteId;
    this.referralService.deleteReferralRul(data).then(res => {
      this.deleteReferralRule = res;
    });

    /* delete selected referral from the screen list */
    let deletedReferral =
      this.listReferralData &&
      this.listReferralData.find(referral => {
        return referral.referralRuleId == this.deleteId;
      });

    if (deletedReferral) {
      this.listReferralData.splice(
        this.listReferralData.indexOf(deletedReferral),
        1
      );
    }
  }

  changeDateType(date) {
    if (date != "") {
      var reDate = new Date(date);
      var month = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
      ][reDate.getMonth()];
      var day = reDate.getDate();
      var reDay = day < 10 ? "0" + day : day;
      return reDay + " " + month + ", " + reDate.getFullYear();
    }
  }
}
