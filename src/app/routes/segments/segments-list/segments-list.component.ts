import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { SegmentService } from "../../../core/service/segment/segment.service";
import { CommonService } from "../../../core/service/common/common.service";
import { DomSanitizer } from "@angular/platform-browser";
import { StorageService } from "../../../auth/services/storage.service";

declare var $;

@Component({
  selector: "app-segments-list",
  templateUrl: "./segments-list.component.html",
  styleUrls: ["./segments-list.component.scss"]
})
export class SegmentsListComponent implements OnInit {
  youtubeUrl: any;
  appId: string;
  orgId: string;

  constructor(
    public segmentService: SegmentService,
    private router: Router,
    public commonService: CommonService,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private storageService: StorageService
  ) {
    this.youtubeUrl = sanitizer.bypassSecurityTrustResourceUrl(
      "https://www.youtube.com/embed/kbAQidXLaWs" + "?autoplay=0&showinfo=0"
    );
  }

  //response: any;
  segments: Array<any> = [];
  segmentsTmp: Array<any> = [];

  indviSeg: any;
  private sendData: any = {};
  private deleteSegment: any;
  private deleteID: any = {};

  path: any;
  isDrafted = "";

  private hideBar = false;
  strArray = [];

  headName = [
    { name: "User Segments", url: "segments/list", active: true },
    { name: "User Group", url: "segments/usergrouplist", active: false },
    { name: "Geo Fencing", url: "segments/geofencing", active: false }
  ];

  ngOnInit() {
    $("body").removeClass("modal-open");
    $(".modal-backdrop").remove();

    /* set header name to the header bar */
    this.commonService.setHeaderName(this.headName);

    this.hideBar = false;
    /* get the segment id of the individual segment */
    this.path = this.route.params.subscribe(params => {
      this.isDrafted = params["id"];
    });

    /* get list user in segments from api  */
    this.segmentService.getListUserSegments().then(res => {
      console.log("segment list ====>", res);
      this.getListUserSegment(res);
    });
  }

  stopPlayingVideo() {
    $("#YtPlayerModal").on("hidden.bs.modal", function(e) {
      $("#YtPlayerModal iframe").attr(
        "src",
        $("#YtPlayerModal iframe").attr("src")
      );
    });
  }
  parseDate(dateStr) {
    if (isNaN(dateStr)) {
      //Checked for numeric
      var dt = new Date(dateStr);
      if (isNaN(dt.getTime())) {
        //Checked for date
        return dateStr; //Return string if not date.
      } else {
        return dt; //Return date **Can do further operations here.
      }
    } else {
      return dateStr; //Return string as it is number
    }
  }

  getListUserSegment(data) {
    console.log(data);
    let tmpSegments = data;
    this.strArray = [];

    if (tmpSegments) {
      for (var i = 0; i < tmpSegments.length; i++) {
        let segment = tmpSegments[i].segment;
        this.appId = tmpSegments[i].appId;
        this.orgId = tmpSegments[i].orgId;
        console.log(segment);
        segment["str"] = "";
        let strScreen = "";

        // let createdAtVal = new Date(segment["createTime"]);
        // segment["createTime"] =
        //   createdAtVal.getDate() +
        //   "-" +
        //   (createdAtVal.getMonth() + 1) +
        //   "-" +
        //   createdAtVal.getFullYear();

        let segId = { segmentId: segment["segmentId"] };
        segId["appId"] = segment["appId"];
        this.segmentService.getCountsUserSegments(segId).then(res => {
          console.log(res);
          for (var j = 0; j < tmpSegments.length; j++) {
            if (
              res["segment"] &&
              tmpSegments[j].segment["segmentId"] == res["segment"]["segmentId"]
            ) {
              tmpSegments[j].segment["userCount"] =
                res["segment"]["countOfUsers"];
            }
          }
        });

        /* filters */
        if (segment["filters"]) {
          /* user attributes string */
          if (segment["filters"]["userAttributes"]) {
            let userAttributes = segment["filters"]["userAttributes"];

            for (var j = 0; j < userAttributes.length; j++) {
              let userAttribute = userAttributes[j];
              strScreen +=
                userAttribute["attribute"] +
                " " +
                userAttribute["condition"] +
                " ";
              if (
                userAttribute["values"] &&
                userAttribute["values"].length > 0
              ) {
                for (var k = 0; k < userAttribute["values"].length; k++) {
                  var date = userAttribute["values"][k];
                  var isDate = this.parseDate(date);
                  if (isDate instanceof Date) {
                    // if (isDate) {
                    strScreen +=
                      isDate.getFullYear() +
                      "-" +
                      (isDate.getMonth() + 1) +
                      "-" +
                      isDate.getDate() +
                      " ";
                  } else {
                    strScreen += userAttribute["values"][k] + " ";
                  }
                }
              }
              strScreen += " And ";
            }
          }

          /* device attributes string */
          if (segment["filters"]["deviceAttributes"]) {
            let deviceAttributes = segment["filters"]["deviceAttributes"];

            for (var j = 0; j < deviceAttributes.length; j++) {
              let deviceAttribute = deviceAttributes[j];
              strScreen +=
                deviceAttribute["attribute"] +
                " " +
                deviceAttribute["condition"] +
                " ";
              if (
                deviceAttribute["values"] &&
                deviceAttribute["values"].length > 0
              ) {
                for (var k = 0; k < deviceAttribute["values"].length; k++) {
                  strScreen += deviceAttribute["values"][k] + " ";
                }
              }
              strScreen += " And ";
            }
          }

          /* app events string */
          if (segment["filters"]["appEvents"]) {
            let appEvents = segment["filters"]["appEvents"];

            for (var j = 0; j < appEvents.length; j++) {
              let appEvent = appEvents[j];
              strScreen +=
                appEvent["conditionForEvent"] +
                " " +
                appEvent["event"] +
                " " +
                appEvent["conditionForCount"] +
                " " +
                appEvent["countOfTimes"] +
                " Times " +
                appEvent["conditionForDuration"] +
                " " +
                appEvent["countOfDays"] +
                " Days";
              if (appEvent["attributes"] && appEvent["attributes"].length > 0) {
                for (var k = 0; k < appEvent["attributes"].length; k++) {
                  strScreen +=
                    appEvent["attributes"][k]["attribute"] +
                    " " +
                    appEvent["attributes"][k]["condition"] +
                    " " +
                    appEvent["attributes"][k]["value"] +
                    " ";
                }
              }
              strScreen += " And ";
            }
          }
        }
        this.strArray.push(strScreen);
        this.segments.push(segment);
        console.log(this.segments);
      }
      this.segments["appId"] = tmpSegments["appId"];
      this.changeStrWidth();
    }
  }

  editUserSegment(segmentId) {
    this.segmentService.setTmpUserListData({});
    this.segmentService.setTmpUserSaveListData(null);
    this.segmentService.setEditSegmentState(true);
    this.router.navigate(["/segments/edit", segmentId]);
  }

  goToCreateSegment($event) {
    $event.preventDefault();
    this.segmentService.setTmpUserListData({});
    this.segmentService.setTmpUserSaveListData(null);
    this.router.navigate(["/segments/create"]);
  }

  // goToUserCount($event) {
  //   $event.preventDefault();
  //   this.segmentService.setTmpUserListData({});
  //   this.segmentService.setTmpUserSaveListData(null);
  //   this.router.navigate(["/segments/usercount"]);
  // }

  isUserCont(count) {
    if (typeof count === "undefined") {
      return false;
    } else {
      return true;
    }
  }

  hideHowItWorkz($event) {
    $("#howItWorkz").css("display", "none");
  }

  gotoUserCount(segId, appId) {
    let sendData = {
      segmentId: segId,
      appId: appId
    };
    this.segmentService.setTmpUserListData({});
    this.segmentService.setTmpUserSaveListData(null);
    this.segmentService.setUserListData(sendData);
    this.segmentService.setEditSegmentState(true);
    this.router.navigate(["/segments/usercount"]);
  }

  /* duplicate user segments */
  duplicateUserSegment(segment) {
    /* get the individual segment data */
    this.sendData["segment"] = segment;
    this.sendData["appId"] = this.appId;
    this.sendData["userId"] = this.storageService.getCurrentUser().user.userId;
    this.sendData[
      "orgId"
    ] = this.storageService.getCurrentUser().user.organizationId;
    console.log(this.sendData);
    this.segmentService.getDuplicateSegment(this.sendData).then(res => {
      this.segments = [];
      this.getListUserSegment(res);
    });
  }

  /* delete user segment */
  deleteUserSegment(segmentId, appId) {
    let id = {};
    id["segmentId"] = segmentId;
    id["appId"] = appId;
    this.deleteID = id;

    $("#deleteSegment").trigger("click");
  }

  /* confirm to delete Segment */
  deletSelectedSegment($event) {
    $event.preventDefault();

    /* delete selecte segment from the screen list */
    let deletedSegment =
      this.segments &&
      this.segments.find(segment => {
        return segment.segmentId == this.deleteID["segmentId"];
      });

    if (deletedSegment) {
      this.segments.splice(this.segments.indexOf(deletedSegment), 1);
    }

    this.segmentService.deleteUserSegment(this.deleteID).then(res => {
      this.deleteSegment = res;
    });
  }

  changeScreenSize(event) {
    this.changeStrWidth();
  }

  changeStrWidth() {
    if (this.strArray) {
      if (window.innerWidth > 1700) {
        for (var i = 0; i < this.strArray.length; i++) {
          let strLength = this.strArray[i];
          if (strLength) {
            if (strLength.length > 120) {
              strLength = strLength.substring(0, 120);
            }
            this.segments[i]["str"] = strLength;
          }
        }
      } else if (window.innerWidth > 1500) {
        for (var i = 0; i < this.strArray.length; i++) {
          let strLength = this.strArray[i];
          if (strLength) {
            if (strLength.length > 110) {
              strLength = strLength.substring(0, 110);
            }
            this.segments[i]["str"] = strLength;
          }
        }
      } else if (window.innerWidth > 1350) {
        for (var i = 0; i < this.strArray.length; i++) {
          let strLength = this.strArray[i];
          if (strLength) {
            if (strLength.length > 70) {
              strLength = strLength.substring(0, 70);
            }
            this.segments[i]["str"] = strLength;
          }
        }
      } else {
        for (var i = 0; i < this.strArray.length; i++) {
          let strLength = this.strArray[i];
          if (strLength) {
            if (strLength.length > 60) {
              strLength = strLength.substring(0, 60);
            }
            this.segments[i]["str"] = strLength;
          }
        }
      }
    }
  }

  changeTitleWidth(title) {
    if (title) {
      if (window.innerWidth > 1700) {
        let strLength = title;
        if (strLength.length > 70) {
          strLength = strLength.substring(0, 70);
        }
        return strLength;
      } else if (window.innerWidth > 1500) {
        let strLength = title;
        if (strLength.length > 60) {
          strLength = strLength.substring(0, 60);
        }
        return strLength;
      } else if (window.innerWidth > 1450) {
        let strLength = title;
        if (strLength.length > 55) {
          strLength = strLength.substring(0, 55);
        }
        return strLength;
      } else {
        let strLength = title;
        if (strLength.length > 50) {
          strLength = strLength.substring(0, 50);
        }
        return strLength;
      }
    }
  }
}
