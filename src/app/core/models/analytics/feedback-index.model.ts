export interface FeedbackIndexModel {
  "status": number,
  "thisVersion": {
    "ratingCounts": number[],
    "average": number
  },
  "allVersion": {
    "ratingCounts": number[],
    "average": number
  }
}

export interface FeedbackIndexRatingModel {
  x: number,
  y: number
}

export class FeedbackIndexReviewsModel {
  type: string;
  total_rating: any;
  average: number;
  ratings: [
    {
      id: number;
      percentage: number;
      count: number;
    }
  ]
}
export class FeedbackIndexTrendingModel {
  status: number;
  ratings: [
      {
        date: string;
        average: number;
      }
  ]
}
export class FeedbackIndexTrendingModel2 {
  date: number;
  value: number;
}
export interface FeedbackIndexTrendingRatingModel {
  date: Date,
  average: number
}


