export class SignedOutModel {
  accessToken: string;
  deviceId: string;

  public constructor(init?: Partial<SignedOutModel>) {
    Object.assign(this, init);
  }
}
