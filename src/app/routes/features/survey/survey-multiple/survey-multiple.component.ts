import { Component, OnInit, ElementRef, ViewChild, AfterViewInit, SimpleChanges, OnChanges } from '@angular/core';
import { SurveyConstants } from '../survey.constants';
import { SurveyQuestionComponent } from '../survey-question/survey-question.component';
import { Router, ActivatedRoute } from '@angular/router';
import { SurveyService } from '../../../../core/service/survey/survey.service';
import { ConfigThankyouComponent } from '../config-thankyou/config-thankyou.component';
import { ConfigWelcomeComponent } from '../config-welcome/config-welcome.component';
import { ConfigQuestionComponent } from '../config-question/config-question.component';
import * as _ from 'lodash';
declare var $;

@Component({
  selector: 'app-survey-multiple',
  templateUrl: './survey-multiple.component.html',
  styleUrls: ['./survey-multiple.component.scss']
})
export class SurveyMultipleComponent implements OnInit {
  @ViewChild(SurveyQuestionComponent) surveyQuestion: SurveyQuestionComponent;
  @ViewChild(ConfigWelcomeComponent) welcomePage: ConfigWelcomeComponent;
  @ViewChild(ConfigQuestionComponent) questionPage: ConfigQuestionComponent;
  @ViewChild(ConfigThankyouComponent) thankyouPage: ConfigThankyouComponent;

  tabSelected: string;
  isWebApp: boolean = false;
  surveyMode: string = 'create';
  questions: Array<object> = [];
  welcomeConfig: Object = {};
  questionConfig: Object = {};
  thankyouConfig: Object = {};
  sideBarList = SurveyConstants.MultipleInputSideBar;
  isWelcomeSelected: boolean = false;
  surveyName: any;
  surveyId: any;
  applyAll: boolean = true;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private surveyService: SurveyService
  ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {
      if (params && params['id']) {
        this.surveyMode = 'edit';
        this.surveyService.getSurveyDetails(params['id']).subscribe((resp: any) => {
          this.surveyName = resp.survey.surveyName;
          this.surveyId = params['id']
          this.formatQuestions(resp.survey.questions);
          this.editScreenConfig(resp.survey.screenConfig);
          this.tabSelected = this.sideBarList[0].key;
        });
      } else {
        this.initializeScreenConfig(
          SurveyConstants.defaultQuestion,
          {
            welcome: SurveyConstants.welcomeConfig,
            question: SurveyConstants.questionConfig,
            thankyou: SurveyConstants.thankyouConfig
          });
        this.tabSelected = this.sideBarList[0].key;
      }
    });
  }

  formatQuestions(questions) {
    questions.map(question => {
      if (!question.isFollowUpQuestion && question.followUp && question.followUp.length) {
        question.followUp = question.followUp.map(each => {
          let followupObj = _.filter(questions, { questionId: each.followUpQuestionId })[0] || {};
          return Object.assign(each, followupObj);
        });
      }
    });
    this.questions = _.filter(questions, { isFollowUpQuestion: false });
  }

  applySelected(event?) {
    this.applyAll = event;
  }

  updateColorFont() {
    this.updateScreenConfig();
    this.questionConfig['color'] = this.welcomeConfig['color'];
    this.questionConfig['fontStyle'] = this.welcomeConfig['fontStyle'];

    this.thankyouConfig['color'] = this.welcomeConfig['color'];
    this.thankyouConfig['fontStyle'] = this.welcomeConfig['fontStyle'];
  }

  onTabChange(event) {
    if (this.tabSelected == 'questions')
      this.updateSurveyQuestion(false);

    if (this.tabSelected == 'config-welcome' && this.applyAll)
      this.updateColorFont();

    this.tabSelected = event;
  }

  editScreenConfig(screenConfig) {
    let config: any = {};
    let welcomeConfig = _.filter(screenConfig, { "configType": "welcomeScreen" });
    config.question = _.filter(screenConfig, { "configType": "questionScreen" })[0];
    config.thankyou = _.filter(screenConfig, { "configType": "thankYouScreen" })[0];

    this.isWelcomeSelected = welcomeConfig ? true : false;
    config.welcome = welcomeConfig ? welcomeConfig[0] : SurveyConstants.welcomeConfig;
    this.initializeScreenConfig(false, config);

  }

  initializeScreenConfig(question, screenConfig) {
    question && this.questions.push(Object.assign({}, question));
    this.welcomeConfig = Object.assign({}, screenConfig.welcome);
    this.questionConfig = Object.assign({}, screenConfig.question);
    this.thankyouConfig = Object.assign({}, screenConfig.thankyou);
  }

  navigate() {
    this.router.navigate(['/features/survey-list']);
  }

  cancel() {
    $('#surveyCancelModal').modal('show');
  }

  save(draft = false) {
    this.updateScreenConfig();
    if (this.surveyQuestion)
      this.questions = Object.assign(this.questions, this.surveyQuestion.questionsList);
    if (!this.validateAllQuestion()) {

      if (this.surveyMode == 'edit') {
        this.surveyService.editSurvey(draft, this.isWebApp, this.surveyName, this.updateSurveyQuestion(true), [this.welcomeConfig, this.questionConfig, this.thankyouConfig], this.surveyId)
          .subscribe(resp => {
            $('#surveySuccessModal').modal('show');
          })
      } else {
        this.surveyService.createSurvey(draft, this.isWebApp, this.surveyName, this.updateSurveyQuestion(true), [this.welcomeConfig, this.questionConfig, this.thankyouConfig])
          .subscribe(resp => {
            $('#surveySuccessModal').modal('show');
          })
      }
    }
  }

  updateScreenConfig() {
    if (this.welcomePage)
      this.welcomeConfig = Object.assign(this.welcomeConfig, this.welcomePage.welcomeData);

    if (this.questionPage)
      this.questionConfig = Object.assign(this.questionConfig, this.questionPage.questionData);

    if (this.thankyouPage)
      this.thankyouConfig = Object.assign(this.thankyouConfig, this.thankyouPage.thankyouData);

    return this.isWelcomeSelected ? [this.welcomeConfig, this.questionConfig, this.thankyouConfig] : [this.questionConfig, this.thankyouConfig]
  }

  updateSurveyQuestion(omit = true) {
    let questions = [];
    if (this.surveyQuestion)
      this.questions = Object.assign(this.questions, this.surveyQuestion.questionsList);
    if (omit && this.questions.length) {
      questions = _.map(this.questions, (ques: any) => {
        if (ques.followUp && ques.followUp.length) {
          ques.followUp = _.map(ques.followUp, (followQues) => this.getFollowUpObject(followQues));//_.omit(followQues, ['qid', 'modelSelected', 'smileyNumber']));
        }
        return _.omit(ques, ['qid', 'modelSelected', 'smileyNumber']);
      });
    }
    return questions;
  }

  getFollowUpObject(followup) {
    return {
      "onResponse": followup.onResponse,
      "question": {
        "inputType": followup.inputType,
        "question": followup.question,
        "imageUrl": followup.imageUrl,
        "options": followup.options
      }
    }
  }

  openQuestion() {
    this.tabSelected = 'questions';
  }

  validateAllQuestion() {
    let error = false;
    this.questions.map(ques => {
      if (!ques['question'] || (/^ *$/.test(ques['question'])))
        error = true;
      ques['followUp'] && ques['followUp'].map(fques => {
        if (!fques['question'] || (/^ *$/.test(fques['question'])))
          error = true;
      });
    });
    if (error)
      $('#surveyErrorModal').modal('show');
    return error;
  }
}
