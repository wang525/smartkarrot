import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigThankyouComponent } from './config-thankyou.component';

describe('ConfigThankyouComponent', () => {
  let component: ConfigThankyouComponent;
  let fixture: ComponentFixture<ConfigThankyouComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigThankyouComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigThankyouComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
