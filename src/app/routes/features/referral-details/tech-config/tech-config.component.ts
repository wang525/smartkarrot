import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { stepData } from "../../referral-details/referral-data";

@Component({
  selector: "app-tech-config",
  templateUrl: "./tech-config.component.html",
  styleUrls: ["./tech-config.component.scss"]
})
export class TechConfigComponent implements OnInit {
  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;

  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();
  @Output() draftRefData = new EventEmitter();
  techData: Object;
  programId: string;
  constructor(private router: Router) {}

  ngOnInit() {
    // console.log(this.referralData);
    // console.log(this.referralData["programId"]);
    this.programId = this.referralData["programId"];
    this.techData = this.referralData["techConfiguration"];
  }

  /* change the step status */
  changeBackSetp() {
    this.changeStepStatus.emit(stepData.STEP8_ERROR_SCREEN);
  }

  changeNextStep() {
    this.router.navigate(["features/referral-list"]);
  }

  saveData() {
    this.saveRefData.emit();
  }
  saveAsDraft() {
    this.draftRefData.emit();
  }
}
