import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '../../../../core/service/analytics/analytics.service';
import { RetentionIndexModel } from '../../../../core/models/analytics/retention-index.model';
import { AppDataModel } from '../../../../core/models/analytics/app-data.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-retention-index',
  templateUrl: './retention-index.component.html',
  styleUrls: ['./retention-index.component.scss']
})
export class RetentionIndexComponent implements OnInit {
  retentionIndexData: RetentionIndexModel;
  startDate: string = this.analyticsService.getFormattedDate(
    new Date().setDate(new Date().getDate() - 42)
  );
  updatingGraph: boolean = false;
  endDate: string = this.analyticsService.getFormattedDate(new Date());
  allSortOptions : Object;
  sortOptions:string[];
  selectedSortOption: string = 'day';
  sortSubOptions:string[];
  selectedSortSubOption:string = 'Last 7 days';

  // ToDo - Update code once API works properly
  // appId: string = this.analyticsService.getAppId();
  appId: string = '4ed062f1-fc38-4f32-a714-b63dc5399626';
  // appId: string = "6bbe46fa-33df-4664-a08e-2f8fdf3c8257";
  unit: string = "day";
  filterDate: string = "6w";
  activeApp : any;
  subscription: Subscription;

  constructor(private analyticsService: AnalyticsService) {

    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();
    this.appId = this.activeApp.appId;
  }

  ngOnInit() {
    this.sortOptions = ['day','week','month'];
    this.allSortOptions = {
      'day': [
          "Last 7 days",
          "Last 14 days",
          "Last 21 days",
          "Last 30 days"
        ],
      'week': [
          "Last week",
          "Last 3 weeks",
          "Last 6 weeks",
          "Last 9 weeks",
          "Last 12 weeks"
        ],
      'month': [
          "Last month",
          "Last 2 months",
          "Last 3 months"
        ]
    };
    this.changeSubOptions();
    this.subscription = this.analyticsService.getActiveAppInfo().subscribe(activeApp => { 
      this.activeApp = activeApp.app;
      this.appId = this.activeApp.appId;
      this.changeSubOptions();
    });
  }

  changeSubOptions(){
    if (this.updatingGraph) { return; }

    this.retentionIndexData = null;
    if (this.selectedSortOption) {          
      this.sortSubOptions = this.allSortOptions[this.selectedSortOption];
      this.selectedSortSubOption = this.sortSubOptions[0];
    }
    // this.unit = this.selectedSortOption;
    this.changeParams();
  }

  getLastDate(days) {
    var date = new Date();
    var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
    // var day =last.getDate();
    // var month=last.getMonth()+1;
    // var year=last.getFullYear();

    return this.analyticsService.getFormattedDate(last);
  }

  changeParams(){
    if (this.updatingGraph) { return; }

    this.updatingGraph = true;
    this.retentionIndexData = null;
    this.endDate = this.analyticsService.getFormattedDate(new Date());
    let d = new Date();

    let multiplier:number = 1;

    let date_string = this.selectedSortSubOption;
    
    switch(this.selectedSortSubOption){
      case 'Last week':
      date_string = 'Last 1 week';
      break;

      case 'Last month':
      date_string = 'Last 1 month';
      break;
    }

    let selectedDay:number = parseFloat(date_string.match(/[\d.]+/)[0]);
    let numberOfDays:number = 1;
    if (this.selectedSortOption=='week') {
      multiplier = 7;
    } else if (this.selectedSortOption=='month') {
      multiplier = 31;
    }

    numberOfDays = selectedDay * multiplier;
    let last_date = this.getLastDate(numberOfDays);
    this.startDate = last_date;

    this.unit = this.selectedSortOption;
    
    this.updateGraph();
  }

  changeParamsOld() {
    this.endDate = this.analyticsService.getFormattedDate(new Date());
    let d = new Date();
    if (this.filterDate == '3m' || this.filterDate == '6m') {
      if (d.getMonth() < 3) {
        d.setFullYear(d.getFullYear() - 1);
        d.setMonth(d.getMonth() + 12 - <any>this.filterDate.substring(0, 1));
      } else {
        d.setMonth(d.getMonth() - <any>this.filterDate.substring(0, 1));
      }
      this.unit = 'month';
    }
    if (this.filterDate == '6w') {
      d.setDate(d.getDate() - 42);
      this.unit = 'week';
    }
    this.startDate = this.analyticsService.getFormattedDate(d);
   

    this.updateGraph();
  }

  updateGraph() {
    this.analyticsService
      .getAppRetentionIndex({
        startDate: this.startDate,
        endDate: this.endDate,
        appId: this.appId,
        unit: this.unit,
        "platforms": [ "Android", "iOS" ]
      })
      .subscribe(data => {
        this.updatingGraph = false;
        this.retentionIndexData = data;
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
