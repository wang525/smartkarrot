import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '../../../../core/service/analytics/analytics.service';
import { EngagementScoreGroupsModel} from '../../../../core/models/analytics/engagement-score-groups.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-eng-score-relative',
  templateUrl: './eng-score-relative.component.html',
  styleUrls: ['./eng-score-relative.component.scss']
})
export class EngScoreRelativeComponent implements OnInit {

  appId: string = '4ed062f1-fc38-4f32-a714-b63dc5399626';
  activeApp : any;
  timeSpan: string = 'week';
  subscription: Subscription;
  data: EngagementScoreGroupsModel;

  constructor(private analyticsService: AnalyticsService) { 

    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();
    this.appId = this.activeApp.appId;
    this.updateGraph();

    this.subscription = this.analyticsService.getActiveAppInfo().subscribe(activeApp => { 
    
      this.activeApp = activeApp.app;
      this.appId = this.activeApp.appId;
      this.updateGraph();
    });
  }

  ngOnInit() {
  }

  updateGraph() {
    this.data = null;
    this.analyticsService
      .getAppEngagementScoreGroups({
        appId: this.appId,
        timeSpan: this.timeSpan,
        "platforms": [ "Android", "iOS" ]
      })
      .subscribe(data => {
        this.data = data;
      });
  }

  updateTimeSpan(timeSpan) {
    this.timeSpan = timeSpan;
    this.updateGraph();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}