import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyMultipleComponent } from './survey-multiple.component';

describe('SurveyMultipleComponent', () => {
  let component: SurveyMultipleComponent;
  let fixture: ComponentFixture<SurveyMultipleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyMultipleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyMultipleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
