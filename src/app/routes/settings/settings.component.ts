import { Component, OnInit, AfterViewInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "../../core/service/common/common.service";
import { AuthService } from "../../auth/services/auth.service";
import { EmailValidator } from "@angular/forms";
import { StorageService } from "../../auth/services/storage.service";
import { journeyModel } from "../journey/journey.model";
const emailREGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
declare var $: any;

@Component({
  selector: "app-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"]
})
export class SettingsComponent implements AfterViewInit, OnInit {
  journeyModel: journeyModel = {
    emailId: "",
    deviceId: ""
  };
  form: FormGroup;
  headName = [{ name: "Settings", url: "/settings", active: true }];
  constructor(
    public commonService: CommonService,
    private router: Router,
    private authService: AuthService,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    console.log("setting");
    /* set header name to the header bar */
    this.commonService.setHeaderName(this.headName);
    // document.querySelector('body').style['background-color'] = ' #333';
  }

  ngAfterViewInit() {
    $(function() {
      $("#scroll").slimScroll({
        height: "auto",
        color: "#00f",
        size: "2px",
        alwaysVisible: true
      });
    });
  }

  changePassword() {
    console.log("change password");
    //  this.router.navigate(['settings/change-password']);
  }

  gotToApps() {
    this.router.navigate(["settings/apps"]);
  }

  goToMyAccount() {
    this.router.navigate(["settings/my-account"]);
    // window.location.reload();
  }

  goToRolesAndPermission() {
    this.router.navigate(["settings/roles"]);
  }

  goToUsers() {
    this.router.navigate(["settings/users"]);
  }

  goToBillingAndPayment() {
    this.router.navigate(["settings/billingAndPayment"]);
  }

  goToAlertsAndNotifications() {
    this.router.navigate(["settings/alertAndNotifications"]);
  }
  requestInvite(email) {
    try {
      this.journeyModel.emailId = email;
      this.journeyModel.deviceId = this.storageService.generateUUID();
      this.authService.requestInvite(this.journeyModel).subscribe(
        data => {
          const status = data["status"];
          switch (status) {
            case "INVALID_INPUT":
              alert(status);
              break;
            case "INVALID_PASSWORD":
              alert(status);
              break;
            case "USER_NOT_FOUND":
              alert(status);
              break;
            case "ERROR":
              alert("Something went wrong");
              break;
            case "SUCCESS":
              this.storageService.setCurrentUser(data);
              $("#sucessModal").modal("show");
              break;
            default:
              alert("Something went wrong");
          }
        },
        error => {
          console.log(error);
        }
      );
    } catch (error) {}
  }
}
