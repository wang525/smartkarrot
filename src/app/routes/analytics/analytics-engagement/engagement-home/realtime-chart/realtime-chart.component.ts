import { Component, OnChanges, ElementRef, ViewChild, Input } from '@angular/core';
import { RealTimeEngagementModel } from '../../../../../core/models/analytics/real-time-engagement.model';
import { AnalyticsService } from '../../../../../core/service/analytics/analytics.service';
import * as d3 from 'd3';

@Component({
  selector: 'app-realtime-chart',
  templateUrl: './realtime-chart.component.html',
  styleUrls: ['./realtime-chart.component.scss']
})
export class RealtimeChartComponent implements OnChanges {

  @ViewChild('chart')
  private chartContainer: ElementRef;

  @Input()
  data: RealTimeEngagementModel;

  @Input()
  mapSpacing: string;

  margin = {top: 20, right: 20, bottom: 30, left: 40};

  constructor(private analyticsService: AnalyticsService) {
    if (!this.data && this.analyticsService.realTimeChartData) {
      this.data = this.analyticsService.realTimeChartData;
    }
  }

  ngOnChanges(): void {
    if (!this.data) { return; }

    this.createChart();
  }

  private createChart(): void {
    const element = this.chartContainer.nativeElement;
    let data = this.data.realTimeUsers;

    d3.select(element).select('svg').remove();

    const svg = d3.select(element).append('svg')
        .attr('width', element.offsetWidth)
        .attr('height', element.offsetHeight);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    let padding = 0;
    if (this.mapSpacing == 'large') {
      padding = 0.5;
    }

    console.log(this.data);

    const x = d3
      .scaleBand()
      .rangeRound([0, contentWidth])
      .padding(padding)
      .domain(data.map(d => <any>d.minutesBeforeNow));

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([-0, d3.max(data, d => d.userCount)]);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + contentHeight + ')')
      .call(d3.axisBottom(x))
      .attr('x', 6)
      .attr('dx', '0.71em')
      .attr('text-anchor', 'end');

    g.append('g')
      .attr('class', 'axis axis--y')
      .call(d3.axisLeft(y).ticks(5))
      .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end');

    g.selectAll('.bar')
      .data(data)
      .enter().append('rect')
        .attr('class', 'bar')
        .attr('style', 'fill: #CBB3F6')
        .attr('x', d => x(<any>d.minutesBeforeNow))
        .attr('y', d => y(<any>d.userCount))
        .attr('width', x.bandwidth())
        .attr('height', d => contentHeight - y(d.userCount) + (d.userCount == 0 ? 2 : 0));
  }
}
