import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TechConfigComponent } from './tech-config.component';

describe('TechConfigComponent', () => {
  let component: TechConfigComponent;
  let fixture: ComponentFixture<TechConfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TechConfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechConfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
