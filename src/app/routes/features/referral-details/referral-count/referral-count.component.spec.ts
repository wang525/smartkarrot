import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReferralCountComponent } from './referral-count.component';

describe('ReferralCountComponent', () => {
  let component: ReferralCountComponent;
  let fixture: ComponentFixture<ReferralCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReferralCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReferralCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
