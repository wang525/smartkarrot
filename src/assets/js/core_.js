$(document).mouseup(function(e) 
{
    var container = $(".AppFilterWidget");

    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) 
    {
        container.slideUp('fast');
    }
});

$(document).ready(function () {
    $('.nav-list li .collapse').on('show.bs.collapse', function () {
        $(this).parent('li').addClass('active');
    });
    $('.nav-list li .collapse').on('hide.bs.collapse', function () {
        $(this).parent('li').removeClass('active');
    });

    $('.open-attribute, .close-attribute').on('click', function () {
        $('.add-new-attribute-Widget').toggle();
    });

    $(".select").select2({
        minimumResultsForSearch: -1,
        placeholder: function () {
            $(this).data('placeholder');
        },
    });

    function ratingSelect(rating) {
        var originalOption = rating.element;
        return "<i class='icon mr-2 icon-"+ $(originalOption).data('icon') +"'></i>" + rating.text;
        // return "<img class='flag' src='assets/images/flags/pol.png' alt='" + rating.text + "' />" + rating.text;
    };

    $(".ratingSelect").select2({
        templateResult: ratingSelect,
        templateSelection: ratingSelect,
        minimumResultsForSearch: -1,
        escapeMarkup: function (m) {
            return m;
        },
        placeholder: $(this).data('placeholder')
    });

    $(".DateSelect").select2({
        templateSelection: ratingSelect,
        minimumResultsForSearch: -1,
        escapeMarkup: function (m) {
            return m;
        },
        placeholder: $(this).data('placeholder')
    });

    $(".FollowSelect").select2({
        templateResult: ratingSelect,
        templateSelection: ratingSelect,
        minimumResultsForSearch: -1,
        escapeMarkup: function (m) {
            return m;
        },
        placeholder: $(this).data('placeholder')
    });

    $(".FollowSelect2").select2({
        templateResult: ratingSelect,
        templateSelection: ratingSelect,
        minimumResultsForSearch: -1,
        escapeMarkup: function (m) {
            return m;
        },
        placeholder: $(this).data('placeholder')
    });

    var userFilteredAttribute;
    var andOperator = '<span class="condition">AND</span> ';
    var notOperator = '<span class="condition">NOT</span> ';
    var userCategorySelected = '<span class="defaultRuleStyles">Users under</span> ';;
    var ageRulesTemplate = '<span class="font-bold usersAge"> age group between 18yrs - 25yrs</span> ';
    var genderRulesTemplate = '<span class="font-bold userGender">are Male</span> ';
    var firstCountryRulesTemplate = '<span class="font-bold userFirstCountryOption">from Spain</span> ';
    var postFirstCountryRulesTemplate = '<span class="font-bold userFirstCountryOption">Japan</span> ';
    var countryRelation = '<span class="defaultRuleStyles text-uppercase userFirstCountryOption">OR</span> ';
    var languageRulesTemplate = '<span class="font-bold languageFormatType">selected English language</span> ';
    var notificationRulesTemplate = '<span class="font-bold eventsNotification"> Notification receive At least 5 times In between Date 20 SEP 2018 to 19 NOV 2018</span> ';
    var executionRulesTemplate = '<span class="font-bold eventsExecution"> executed in app purchase at least 3 times in the last 7 Days with attributes t-shirt size is 42</span> ';
    var deviceTemplate = '<span class="font-bold deviceFormatType">App downloaded from Google Play Store</span> ';
    var rulesCount = 88214;

    var filter;

    $('.filter').on('change', function () {
        filter = $(this).val();

        $(this).parents("form").find('.filtered').removeClass('active');

        switch (filter) {
            case 'Age':
                $(this).parents("form").find('.age').toggleClass('active');
                break;

            case 'Gender':
                $(this).parents("form").find('.gender').toggleClass('active');
                break;

            case 'Language':
                $(this).parents("form").find('.language').toggleClass('active');
                break;

            case 'Country':
                $(this).parents("form").find('.country').toggleClass('active');
                break;

            case 'appStore':
                $(this).parents("form").find('.appStore').toggleClass('active');
                break;
            case 'notificationRecieved':
                $(this).parents("form").find('.notificationRecieved').toggleClass('active');
                break;
            case 'hasExecuted':
                $(this).parents("form").find('.hasExecuted').toggleClass('active');
                break;
        }

        $('.add-filter').removeClass('disabled');
        userFilteredAttribute = filter;
    });




    // var filterTrigger = 0;
    // for(let i = 0 ; i <filterChildSelectElement.length; i++){
    //     if(filterChildSelectElement[i].find(".form-control").val() === " "){
    //         filterTrigger++;
    //         console.log(filterTrigger);
    //     }
    // }



    var condition = '<div class="d-flex justify-content-center align-items-center divider checkDivider"><div class="check-switch"><div class="radio-container d-flex align-items-center"><div class="form-check"><input class="form-check-input" type="radio" name="condition" id="and" value="and"><label class="form-check-label" for="and">And</label></div><div class="form-check"><input class="form-check-input" type="radio" name="condition" id="not" value="not" ><label class="form-check-label" for="not">Not</label></div></div></div><a href="javascript:void(0);" class="remove-filter"></a></div>';

    var FilterState = false;

    $('.add-filter').on('click', function () {
        var filteredInput = $(this).parents(".tab-pane.show").find(".filtered.active .form-control");
        filteredInput.each(function (index, value) {
            if (filteredInput[index].value.length != 0) {
                FilterState = true;
                $('.attribute-filtered').hide();
                if (userFilteredAttribute === 'Age') {
                    $(".ageAttributeBlock").show();
                    $('.after-filter .userGroup').addClass("active");
                    $(".rulesSentence").append(userCategorySelected);
                    $(".rulesSentence").append(ageRulesTemplate);
                } else
                if (userFilteredAttribute === 'Gender') {
                    $(".genderAttributeBlock").show();
                    $('.after-filter .userGroup').addClass("active");
                    $(".rulesSentence").append(genderRulesTemplate);
                } else
                if (userFilteredAttribute === 'Language') {
                    $(".languageAttributeBlock").show();
                    $('.after-filter .userGroup').addClass("active");
                    $(".rulesSentence").append(languageRulesTemplate);
                } else
                if (userFilteredAttribute === 'Country') {
                    $(".countryAttributeBlock").show();
                    $('.after-filter .userGroup').addClass("active");
                    $(".rulesSentence").append(firstCountryRulesTemplate + countryRelation + postFirstCountryRulesTemplate);
                } else
                if (userFilteredAttribute === 'appStore') {
                    $(".deviceOsAttributeBlock").show();
                    $('.after-filter .deviceGroup').addClass("active");
                    $(".rulesSentence").append(deviceTemplate);
                } else
                if (userFilteredAttribute === 'hasExecuted') {
                    $(".executionEventBlock").show();
                    $('.after-filter .appEventsGroup').addClass("active");
                    $(".rulesSentence").append(executionRulesTemplate);
                } else
                if (userFilteredAttribute === 'notificationRecieved') {
                    $(".notificationEventBlock").show();
                    $('.after-filter .appEventsGroup').addClass("active");
                    $(".rulesSentence").append(notificationRulesTemplate);
                } else {
                    $('.attribute-filtered').show();
                    FilterState = false;
                }
            }
            if (filteredInput[index].value.length === 0) {
                $("#InputAlertOptions").modal("show");
                FilterState = false;
            }
        });
        if(FilterState === true){
            $(condition).appendTo('.after-filter');
            FilterState = false;
        }
    });

    $('.after-filter').on('change', 'input:radio[name="condition"]', function () {
        rulesCount = rulesCount - 3582;
        var check = $(this).val();
        $(".addAttribute").hide();
        $(".rulesCount").text(rulesCount);
        setTimeout(function () {
            $(".divider").find(".add-filter").addClass("disabled");
            setTimeout(function () {
                $('.attribute-filtered').show();
            }, 100);
        }, 0);
        if (check === "not") {
            $(".rulesSentence").append(notOperator);

        }
        if (check === "and") {
            $(".rulesSentence").append(andOperator);
        }
        if (userFilteredAttribute === 'Age') {
            $(".ageAttributeBlock").find(".divider").removeClass("disabled");
            $(".ageAttributeBlock").find(".connectedOperator").text(check);
        } else
        if (userFilteredAttribute === 'Gender') {
            $(".genderAttributeBlock").find(".divider").removeClass("disabled");
            $(".genderAttributeBlock").find(".connectedOperator").text(check);
        } else
        if (userFilteredAttribute === 'Language') {
            $(".languageAttributeBlock").find(".divider").removeClass("disabled");
            $(".languageAttributeBlock").find(".connectedOperator").text(check);
        } else
        if (userFilteredAttribute === 'Country') {
            $(".countryAttributeBlock").find(".divider").removeClass("disabled");
            $(".countryAttributeBlock").find(".connectedOperator").text(check);
        } else
        if (userFilteredAttribute === 'appStore') {
            $(".deviceOsAttributeBlock").find(".divider").removeClass("disabled");
            $(".deviceOsAttributeBlock").find(".connectedOperator").text(check);
        } else
        if (userFilteredAttribute === 'notificationRecieved') {
            $(".notificationEventBlock").find(".divider").removeClass("disabled");
            $(".notificationEventBlock").find(".connectedOperator").text(check);
        } else
        if (userFilteredAttribute === 'hasExecuted') {
            $(".executionEventBlock").find(".divider").removeClass("disabled");
            $(".executionEventBlock").find(".connectedOperator").text(check);
        }
        $('.checkDivider').remove();
        $(".attribute-filtered").find(".filter").val('').trigger('change');
    });
});

$('.checkDivider').on('click', '.remove-filter', function(){
    
})

$(".addAttributes").click(function (e) {
    e.preventDefault();
    $(this).parent().parent().find(".addAttribute").show();
});


$('.add-more').on('click', function () {
    $(this).parents(".attributeSelectionForm").find('div').removeClass("frezzedOptions");
});

var userAttributeLength, deviceAttributeLength, appEventsLength;
$('.remove-filter').on('click', function () {
    console.log('goo');
    $(this).parent().parent().hide();
    $(this).parent().addClass("disabled");
    userAttributeLength = $(".userGroup").find(".attributeBlock:visible").length;
    deviceAttributeLength = $(".deviceGroup").find(".attributeBlock:visible").length;
    appEventsLength = $(".appEventsGroup").find(".attributeBlock:visible").length;
    if (userAttributeLength === 0) {
        $(".userGroup").removeClass("active");
    };
    if (deviceAttributeLength === 0) {
        $(".deviceGroup").removeClass("active");
    };
    if (appEventsLength === 0) {
        $(".appEventsGroup").removeClass("active");
    };
});
$(".reset-filter").on('click', function () {
    $(this).parents(".tab-pane").find(".addAttribute").hide()
    setTimeout(function () {
        $(".add-filter").addClass("disabled");
    }, 10);
    $(this).parents(".tab-pane").find(".filter").val(null).trigger("change");
});

//Wizard
$('.pcacs-tabs').click(function () {
    var pastep = $(this).attr('title');
    $(this).parents('.pacs').hide();
    $("." + pastep).show();
    $('.stepswizard li[title=' + pastep + ']').prev('li').addClass('completed').removeClass('active');
    $('.stepswizard li[title=' + pastep + ']').next('li').removeClass('completed active');
    $('.stepswizard li[title=' + pastep + ']').removeClass('completed').addClass('active');
    $('.stepswizard li[title=' + pastep + ']').parent().find('has-sub').removeClass('completed');
    $('.stepswizard li[title=' + pastep + ']').parents('li').prev('li').addClass('completed').removeClass('active');
    $('.stepswizard li[title=' + pastep + ']').parents('li').next('li').removeClass('completed active');
    $('.stepswizard li[title=' + pastep + ']').parents('li').removeClass('completed');
    $('.stepswizard li[title=' + pastep + ']').parents('li').addClass('active');
    $('.stepswizard li[title=' + pastep + ']').next('li').children().find('li').removeClass('completed active');
    preventDefault();
});

$('#data-table-content').DataTable({
    "paging": false,
    "info": false,
    "scrollY": 'calc(100vh - 170px)',
    "scrollCollapse": true,
    "searching": false
});

$('#user-table-content').DataTable({
    "paging": false,
    "info": false,
    "scrollY": 'calc(100vh - 235px)',
    "scrollCollapse": true,
    "searching": false,
    "order": [[ 1, "desc" ]],
    "columnDefs": [
        { "orderable": false, "targets": 0 },
        { "orderable": false, "targets": 7 }
    ]
});

$(function () {
    $('.scrollbar, .dataTables_scrollBody, .add-new-attribute, .addAccountInputMob, .referralCardContainer, .select2-results__options').slimScroll({
        size: '3px',
        color: '#000000',
        height: '100%',
    });
});

$(".add-new-attribute-Widget").css({
    'width': ($(".new-rule").width() - 10 + 'px')
});

// $('.carousel').carousel(pause);


$('.stepswizard li').on('click', function () {
    if ($(this).hasClass('strike-though') || $(this).hasClass('has-sub')) {
        preventDefault();
    } else {
        if ($(this).prev('li').hasClass('strike-though')) {} else {
            $(this).prev('li').addClass('completed');
        }
        $(this).prevAll('li').addClass('completed').removeClass('active');
        $(this).nextAll('li').removeClass('completed active');
        $(this).nextAll('li').children().find('li').removeClass('completed active');
        $(this).prevAll('li').children().find('li').addClass('completed');
        $(this).prevAll('li').children().find('li').removeClass('active');
        $(this).parents('li').prevAll('li').addClass('completed').removeClass('active');
        $(this).parents('li').nextAll('li').removeClass('completed active');
        $(this).parents('li').addClass('active');
        $(this).parents('li').removeClass('completed');
        $(this).addClass('active').removeClass('completed');
        var pastep = $(this).attr('title');
        if (pastep != undefined) {
            $('.pacs').hide();
            $("." + pastep).show();
        };
    }
});

$('.checkSlider').on('change', function () {
    // alert($(this).val());
    $(this).closest('.referralCard').toggleClass('disable');
    $(this).closest('.switchBlock').find('.s').toggleClass('enable');
})

$('.referral-body').on('keyup', function () {
    var val = $(this).val();
    $('.chat-body').text('').append(val);
});

$('.upload').on('click', function () {
    // var val = $(this).find('input').val();
    //  alert(val)
});

$('.toolTip').tooltipster({
    plugins: ['sideTip'],
    theme: 'tooltipster-borderless'
});

function format(state) {
    if (!state.id) return state.text; // optgroup
    return state.text + "<img class='flag' src='assets/images/flags/" + $(state.element).data('foo') + ".png'/>";
}

$(".countrySelection").select2({
    templateResult: format,
    templateSelection: format,
    minimumResultsForSearch: -1,
    escapeMarkup: function (m) {
        return m;
    },
    placeholder: function () {
        $(this).data('placeholder');
    }
});

$('input.datePicker').datepicker({
    format: 'dd/mm/yyyy',
    autoclose: true,
});

$('.Hide-Notifi').on('click', function () {
    $('.know-more-list').fadeOut();
});

$('.clearSelect').on('click', function () {
    $(this).parent().find(".select").val(null).trigger("change");
    $(this).parent().find(".countrySelection").val(null).trigger("change");
});

$('.includeState').on('click', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).text('Include Back');
    } else {
        $(this).addClass('active');
        $(this).text('Exclude');
    }
})

$('.UserState').on('click', function () {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
    } else {
        $(this).addClass('active');
    }
})

var attributeSelectionValue, postattributeSelectionValue;
var selectedAttributeTab = "#userAttribute",
    postSelectionAttribute;
$("#attributeTab a").each(function (index) {
    $(this).on("click", function (e) {
        e.preventDefault();
        $(".add-filter").addClass("disabled");
        postSelectionAttribute = selectedAttributeTab;
        selectedAttributeTab = $(this).attr('href');
        attributeSelectionValue = $(postSelectionAttribute).find(".filter").val();
        postattributeSelectionValue = $(selectedAttributeTab).find(".filter").val();
        setTimeout(function () {
            $(postSelectionAttribute).addClass("postSelection");
            $(selectedAttributeTab).removeClass("postSelection");
            if (attributeSelectionValue != '' && postSelectionAttribute === "#userAttribute" || attributeSelectionValue != '' && postSelectionAttribute === "#deviceAttribute" || attributeSelectionValue != '' && postSelectionAttribute === "#appEvents") {
                $('#clearlastOptions').modal('show');
            }
            if (postattributeSelectionValue != '' && selectedAttributeTab === "#userAttribute" || postattributeSelectionValue != '' && selectedAttributeTab === "#deviceAttribute" || postattributeSelectionValue != '' && selectedAttributeTab === "#appEvents") {
                $(selectedAttributeTab).find(".add-filter").removeClass("disabled");
            }
        }, 200);
    });
});

$(".clearLastInput").on('click', function (e) {
    $(postSelectionAttribute).not(".active").find(".filter").val(null).trigger("change");
    $(postSelectionAttribute).find(".add-filter").addClass("disabled");
});


//****************************************************************************** */

$('.stepswizard li, .next').on('click', function () {
    $(this).prev('li.active').addClass('completed').next('li').attr('data-target', '#referalCarousel');
});

$('.NumberUpContainer').click(function () {
    var elem = $(this).closest('.IncreDecreWidget').siblings('input');
    var NumberLength = elem.val();
    NumberLength++;
    elem.val(NumberLength);
});

$('.NumberDownContainer').click(function () {
    var elem = $(this).closest('.IncreDecreWidget').siblings('input');
    var NumberLength = elem.val();
    NumberLength > 0 ? NumberLength-- : NumberLength;
    elem.val(NumberLength);
});

$(document).on('click', 'input[name="referSuccess"]', function () {
    if ($(this).val() === '3') {
        $('.EventSidePanel').addClass('active');
    } else {
        $('.EventSidePanel').removeClass('active');
        $('.eventsName').text($(this).next().text());
    }
});

$('.EventSidePanelList').on('click', function () {
    // alert($(this).find('span').text());
    $('.eventsName').text($(this).find('span').text());
    $('.EventSidePanel').removeClass('active');
});

$('.CloseEventPanel').on('click', function () {
    $('.EventSidePanel').removeClass('active');
});

$('.referral-body').on('keyup', function () {
    var val = $(this).val();
    $('.ChatReferalText').text(val);
});

$('.ChatReferalURLInput').on('keyup', function () {
    $('.ChatReferalLink').text($(this).val() + '.aktion.link');
});

$('.HomeReferTitle').on('keyup', function () {
    $('.MobileHomeReferTitle').text($(this).val());
});

$('.HomeReferBody').on('keyup', function () {
    $('.MobileHomeReferBody').text($(this).val());
});

$('.CountReferTitle').on('keyup', function () {
    $('.MobileCountReferTitle').text($(this).val());
});

$('.WithdrawalReferTitle').on('keyup', function () {
    $('.MobileWithdrawalReferTitle').text($(this).val());
});

$('.CountReferBody').on('keyup', function () {
    $('.MobileCountReferBody').text($(this).val());
});

$('.withdrawal-limit-input').on('keyup', function () {
    $('.withdrawal-limit').text($(this).val());
});

$('.error-text-input').on('keyup', function () {
    $('.error-text').text($(this).val());
});

function filePreview1(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.user-default').css('background-image', 'url(' + e.target.result + ')');
            $('.upload-container').css('background-image', 'url(' + e.target.result + ')');
            $('.upload-container.survey').css('background', 'none');
            $('.upload-container img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$('.UploadFile').on('change', function () {
    filePreview1(this);
});

$('.withdraw-upload').on('change', function () {
    filePreview2(this);
});

function filePreview2(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.withdrawal-image').attr('src', e.target.result);
            $('.upload-container').css('background-image', 'url(' + e.target.result + ')');
            $('.upload-container.survey').css('background', 'none');
            $('.upload-container img').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$('.error-upload').on('change', function () {
    filePreview3(this);
});

function filePreview3(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.error-image').attr('src', e.target.result);
            $('.uplaoded-image').css('background-image', 'url(' + e.target.result + ')');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$('.HomeColorPalette').on('keyup', function (e) {
    if ($(this).val().length < 7) {
        preventDefault();
    } else {
        if ($(this).val().startsWith("#")) {
            $(".mobile-head-home, .share-box").css("background-color", $(this).val());
            $('.HomeColorPalette').val($(this).val());
        } else {
            alert('This is not a valid color code');
        }
    }
});

$('.CountColorPalette').on('keyup', function (e) {
    if ($(this).val().length < 7) {
        preventDefault();
    } else {
        if ($(this).val().startsWith("#")) {
            $(".mobile-head-count, .withdrawalIconContainer").css("background-color", $(this).val());
            $('.CountColorPalette').val($(this).val());
        } else {
            alert('This is not a valid color code');
        }
    }
});

$('.WithdrawalColorPalette1').on('keyup', function (e) {
    if ($(this).val().length < 7) {
        preventDefault();
    } else {
        if ($(this).val().startsWith("#")) {
            $(".mobile-head-withdrawal").css("background-color", $(this).val());
            $('.WithdrawalColorPalette1').val($(this).val());
        } else {
            alert('This is not a valid color code');
        }
    }
});

$('.WithdrawalColorPalette2').on('keyup', function (e) {
    if ($(this).val().length < 7) {
        preventDefault();
    } else {
        if ($(this).val().startsWith("#")) {
            $(".add-footer-withdrawal").css("background-color", $(this).val());
            $('.WithdrawalColorPalette2').val($(this).val());
        } else {
            alert('This is not a valid color code');
        }
    }
});

$('.field-widget').on('keyup', '.AddAccountInput', function (e) {
    var InputTitle = $(this).parents(".form-group").find(".AddAccountInput").attr('title');
    $('.addAccountInputMob').find('.' + InputTitle).text($(this).val());
});

$('.withdrawal-slider-check').on('change', function () {
    $('.enable-withdrawal').toggleClass('strike-though');
});

$('.webapp-check').on('change', function () {
    if ($(this).is(':checked')) {
        $('.mobapp-container').hide();
        $('.webapp-container').show();
    } else {
        $('.mobapp-container').show();
        $('.webapp-container').hide();
    }
})

$('.add-account-field').on('click', function () {
    $('.field-widget').append('<div class="field-container"><h5 class="font-bold black-text white-bg">Field Name</h5><div class="w-50 position-relative"><div class="form-group"><input title="AddAccountInput' + ($('.addAccountInputMob li').length + 1) + '" type="text" class="form-control AddAccountInput" placeholder="Text Here"></div></div></div>');
    $('.addAccountInputMob').append('<li class="AddAccountInput' + ($('.addAccountInputMob li').length + 1) + '">Text Here</li>');
});

var gamificationContent = '<div class="AppendContent-items gamification-items notification-items"><div class="form-group mb-2"><input type="text" class="form-control" value="1"></div><div class="form-group select-form-group mb-2"><Select class="form-control select"><option>Option</option><option>1</option><option>2</option></Select></div><p class="mx-2 mb-0">or</p><div class="form-group select-form-group mb-2"><Select class="form-control select"><option>Actors</option><option>Actor 1</option><option>Actor 2</option></Select></div></div>';

$('.add-gamification').on('click', function () {
    $('.gamification-content').append(gamificationContent);
    $('.select').select2({
        minimumResultsForSearch: -1,
        placeholder: function () {
            $(this).data('placeholder');
        }
    });
});

var sendTextNotifi = '<div class="AppendContent-items send-text-notification-items notification-items"><div class="form-group mb-2"><input type="text" class="form-control"></div><p class="mx-2 mb-0">to</p><div class="form-group select-form-group mb-2"><Select class="form-control select"><option>Actors</option><option selected>Actor 1</option><option>Actor 2</option></Select></div></div>';
var sendTextNotifi2 = '<div class="AppendContent-items send-text-notification-items notification-items w-100"><div class="form-group mb-2 w-100"><input type="text" class="form-control"></div><p class="mx-0 mb-2">To</p><div class="form-group select-form-group mb-2"><Select class="form-control select"><option>Actors</option><option selected>Actor 1</option><option>Actor 2</option></Select></div></div>';

$('.send-text-notifi').on('click', function () {
    if($('.send-text-notification-content').hasClass('w-60')){
        $('.send-text-notification-content').append(sendTextNotifi2);
    } else {
        $('.send-text-notification-content').append(sendTextNotifi);
    }
    $('.select').select2({
        minimumResultsForSearch: -1,
        placeholder: function () {
            $(this).data('placeholder');
        }
    });
});

$('.send-inapp-notifi').on('click', function () {
    if($('.send-inapp-notification-content').hasClass('w-60')){
        $('.send-inapp-notification-content').append(sendTextNotifi2);
    } else {
        $('.send-inapp-notification-content').append(sendTextNotifi);
    }
    $('.select').select2({
        minimumResultsForSearch: -1,
        placeholder: function () {
            $(this).data('placeholder');
        }
    });
});

$('.send-email-notifi').on('click', function () {
    if($('.send-email-notification-content').hasClass('w-60')){
        $('.send-email-notification-content').append(sendTextNotifi2);
    } else {
        $('.send-email-notification-content').append(sendTextNotifi);
    }
    $('.select').select2({
        minimumResultsForSearch: -1,
        placeholder: function () {
            $(this).data('placeholder');
        }
    });
});

// Referal Mobile Crousel
$('.owlCarousel').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    responsive: {
        0: {
            items: 1,
            nav: true
        },
        600: {
            items: 1,
            nav: false
        },
        1000: {
            items: 1,
            nav: true,
            loop: false
        }
    }
});

$('.newUserGroup').on('click', function(){
    $('.newUserGroupListContainer').fadeToggle('fast');
    $('.blackOutScreen').fadeToggle('fast');
    $('.newUserGroupCard').toggleClass('active');
});

$('.blackOutScreen').on('click', function(){
    $('.newUserGroupCard').toggleClass('active');
    $(this).fadeToggle('fast');
    $('.newUserGroupListContainer').fadeToggle('fast');
});

$('input[name=UserTable]').on('change', function(){
    var checkBoxes = $("input[name=UserTable1]");

    if($(this).is(':checked')){
        $('.UserListItem').remove();
        checkBoxes.prop("checked", true);
        checkBoxes.each(function(){
            if($(this).is(':checked')){
                $('.ruleList').append('<li class="UserListItem '+ $(this).attr('id') +'">'+ $(this).val() +'</li>')
            } else {
                $('.'+$(this).attr('id')).remove();
            }
        });
        $('.SelectedUserCount').text('('+ $('.ruleList .UserListItem').length +')');
    } else {
        checkBoxes.prop("checked", false);
        $('.SelectedUserCount').text('(0)');
        $('.UserListItem').remove();
    }
});

$('input[name=UserTable1]').on('change', function(){
    if($(this).is(':checked')){
        $('.ruleList').append('<li class="UserListItem '+ $(this).attr('id') +'">'+ $(this).val() +'</li>')
    } else {
        $('.'+$(this).attr('id')).remove();
    }
    $('.SelectedUserCount').text('('+ $('.ruleList .UserListItem').length +')');
});

$('textarea').keypress(function(e) {
    var tval = $(this).val(),
        tlength = tval.length,
        set = 120,
        remain = parseInt(set - tlength);
    if (remain <= 0 && e.which !== 0 && e.charCode !== 0) {
        $(this).val((tval).substring(0, tlength - 1));
        return false;
    }
});

/******************* FEEDBACK / SURVEY FLOW ************************/

var questionDefault = '<li class="questionItem black-text p-0 active mb-2 default position-relative"><div class="d-flex position-relative questionContainer p-2"><span class="d-flex questionContent font10 secondary-color font-light w-100"></span><span class="deleteBtn d-flex justify-content-center align-items-center"><i class="icon icon-delete-sm-blue"></i></span></div><ul class="d-flex flex-column justify-content-start w-100 questionListSub pl-2"></ul></li>';
var followUpDefault = '<li class="d-flex position-relative questionItemSub black-text p-0 active mt-2 default"><div class="d-flex position-relative questionContainerSub p-2 w-100"><span class="d-flex questionContentSub font10 secondary-color font-light w-100"></span></div></li>'

$('.ratingSelect').on('change', function(){
    var elemRating = $(this).find(':selected').data("icon");
    $('.RatingElem').hide();
    if(elemRating == null){
        $('.RatingElem.default').show();
    } else {
        $('.'+elemRating).show();
        $('.questionItem.active').removeClass('default');
        $('li.questionItem.active').attr('data-rating', $(this).find(':selected').val());
        $('li.questionItem.active').attr('data-item', elemRating);
        $('li.questionItem.active').attr('data-clone', elemRating+'Input');
        $('.questionItem.active .questionContent').text($('.'+elemRating).find('.numberingInput').val());
    }
});

$('.FollowSelect').on('change', function(){
    var elemRating = $(this).find(':selected').data("icon");
    $('.FollowElem').hide();
    $('.followQuestionWidget1 .'+elemRating+'Follow').show();
    $('li.questionItemSub.active').attr('data-follow', $(this).find(':selected').val());
    $('li.questionItemSub.active').attr('data-item-follow', elemRating);
    $('li.questionItemSub.active').attr('data-clone-follow', elemRating+'Input');
    $('.questionItemSub.active .questionContentSub').text($('.'+elemRating).find('.numberingInput').val())
})

$('.FollowSelect2').on('change', function(){
    var elemRating = $(this).find(':selected').data("icon");
    $('.FollowElem2').hide();
    $('.followQuestionWidget2 .'+elemRating+'Follow').show();
    $('li.questionItemSub.active').attr('data-follow', $(this).find(':selected').val());
    $('li.questionItemSub.active').attr('data-item-follow', elemRating);
    $('li.questionItemSub.active').attr('data-clone-follow', elemRating+'Input');
    $('.questionItemSub.active .questionContentSub').text($('.'+elemRating).find('.numberingInput').val())
})

$('.addNewQuestion').on('click', function(){
    $('.questionList li').removeClass('active');
    $('.ratingSelect').val(null).trigger("change");
    $('.questionList').append(questionDefault);
    $('.numberingLabel').text($('.questionList li:last-child').index() + 1 + '.');
    $('.RatingElem').hide();
    $('.RatingElem.default').show();
});

$('.addNewFollow1').on('click', function(){
    $('li.questionItem.active .questionListSub').removeClass('active');
    $('li.questionItem.active .questionListSub').append(followUpDefault);
    $('.AddFollow').addClass('d-none').removeClass('d-flex');
    $('.followQuestionWidget1').show();
    $('.AddNewFollowWidget1').removeClass('d-none').addClass('d-flex');
});

$('.AddNewFollowWidget1').on('click', function(){
    $('li.questionItem.active .questionItemSub').removeClass('active');
    $('li.questionItem.active .questionListSub').append(followUpDefault);
    $('.followQuestionWidget2').show();
});

$('.SmileyRadioBtn').on('change', function(){
    if($(this).val() == '2'){
        $('.smileyRating .MobileSmiley').removeClass('enable');
        $('.smileyRating .MobileSmiley:first-child').addClass('enable');
        $('.Smiley4').addClass('active');
        $('.Smiley4 i').removeClass('disable');
        $('.Smiley4 input').attr('disabled', false);
    } else {
        $('.smileyRating .MobileSmiley').addClass('enable');
        $('.smileyRating .MobileSmiley:first-child').removeClass('enable');
        $('.Smiley4').removeClass('active');
        $('.Smiley4 i').addClass('disable');
        $('.Smiley4 input').attr('disabled', true);
    }
});

$('.cloneText').on('keyup', function(){
    $('.'+$(this).data("value")).text($(this).val());
    $('[data-clone = '+$(this).data("value") + '] .questionContent').text($(this).val());
    $('[data-clone-follow = '+$(this).data("value") + '] .questionContentSub').text($(this).val());
});

$('.cloneColor').on('change', function(){
    $('.'+$(this).data("value")).css('background-color', $(this).val());
});

var feedbackGamification = '<div class="AppendContent-items gamification-items notification-items"><div class="form-group mb-2"><input spellcheck="false" type="text" class="form-control" value="1"></div><div class="form-group select-form-group mb-2"><Select class="form-control select"><option>Option</option><option>1</option><option>2</option></Select></div><p class="mx-2 mb-2">to <span class="font-bold">Actor 1</span></p></div>'

$('.add-feedback-gamification').on('click', function(){
    $('.feedback-gamification-content').append(feedbackGamification);
    $('.select').select2({
        minimumResultsForSearch: -1,
        placeholder: function () {
            $(this).data('placeholder');
        }
    });
});

$('.questionList').on('click', '.questionItem', function(){
    var selectedRating = $(this).data("rating");
    $('.questionItem').removeClass('active');
    $(this).addClass('active');
    $('.numberingLabel').text($(this).index() + 1 + '.');
    if($(this).hasClass('default') || selectedRating == null){
        $('.RatingElem').hide();
        $('.RatingElem.default').show();
    } else {
        $('.ratingSelect').val(selectedRating).trigger("change");
    }
});

$('.questionList').on('click', 'li.questionItem .deleteBtn i', function(){
    if($(this).closest('li').hasClass('default')){
    } else {
        if($('ul.questionList li.questionItem').length == 1){
            $('.questionList').append(questionDefault);
            $('ul.questionList').children('li').addClass('default');
            $('.RatingElem').hide();
            $('.RatingElem.default').show();
        }
        setTimeout(function(){
            $('ul.questionList').children('li:last-child').addClass('active');
            $('.ratingSelect').val($('ul.questionList').children('li:last-child').data("rating")).trigger("change");
            $('.numberingLabel').text($('li.questionItem.active').index() + 1 + '.');
        }, 10);
        $(this).closest('li').remove();
    }
});

setTimeout(function(){
    $('.numberingLabel').text($('li.questionItem.active').index() + 1 + '.');
}, 10);

$( function() { 
    $( ".questionList" ).sortable({
        stop: function( event, ui ) {
            $('li.questionItem').removeClass('active');
            $(ui.item).addClass('active');
            $('.numberingLabel').text($('li.questionItem.active').index() + 1 + '.');
        }
    });
    $( ".questionList" ).disableSelection();
});

$('.DynamicCheckboxWidget').on('focus', '.changeBorder', function(){
    $(this).addClass('dottedInput');
});

$('.DynamicCheckboxWidget').on('blur', '.changeBorder', function(){
    $(this).removeClass('dottedInput');
})

var DynamicCheckboxDefault = '<div class="w-50 d-flex justify-content-start align-content-center mb-3 checkboxWidget"><div class="form-group width160px mb-0 position-relative checkInputGroup"><input class="form-control changeBorder checkInput" placeholder="Type New" value=""></div><div class="add-more-widget mb-0 ml-2 width20px addCheckbox"><span class="list-inline-item add-more-new rounded-circle blue-bg m-0"><span class="icon icon-addSmall"></span></span></div><div class="add-more-widget mb-0 ml-2 width20px removeCheckbox"><span class="list-inline-item add-more-new rounded-circle bg-dark text-center m-0"><span class="white-text d-inline-block">-</span></span></div></div>';

$('.DynamicCheckboxWidget').on('click', '.addCheckbox', function(){
    $(this).remove();
    $('.DynamicCheckboxWidget').children('.checkboxWidget').find('.removeCheckbox').show();
    $('.DynamicCheckboxWidget').append(DynamicCheckboxDefault);
    setTimeout(function(){
        $('.DynamicCheckboxWidget').children('.checkboxWidget:last-child').find('.removeCheckbox').hide();
    }, 10);
});

$('.DynamicCheckboxWidget').on('click', '.checkboxWidget .removeCheckbox', function(){
    $(this).closest('.checkboxWidget').remove();
});

$('.removeFollow').on('click', function(){
    $('.followQuestionWidget2').hide();
    $('.questionItemSub.active').remove();
    $('li.questionItemSub:last-child').addClass('active');
});

$('.FollowUpCheck').on('change', function(){
    $(this).closest('.FollowUpQuestionWidget').find('.FollowUpQuestionBody').slideToggle();
})

$('.CompleteSurvey').on('click', function(){
    $(this).parent().find('.SurveyCompletedWidget').removeClass('d-flex').addClass('d-none');
    $('.SurveyCompleted').addClass('d-flex').removeClass('d-none');
})

$('.BackToSurvey').on('click', function(){
    $(this).closest('.SurveyCompleted').removeClass('d-flex').addClass('d-none');
    $('.SurveyCompletedWidget').addClass('d-flex').removeClass('d-none');
})

//************************************************************************************************** ANALYTICS PAGE

$('.IndexContainer').on('click', function(e){
    var cardTitle = $(this).data('title');
    $('.IndexContainer').not($(this)).removeClass('active');
    if($('.FrontModalContent').hasClass('active1')){
        e.preventDefault();
    } else {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
            $('.IndexContent, .FrontModalContent').removeClass('active');
            $('.IndexContent.default').addClass('active');
        } else {
            $(this).addClass('active');
            $('.IndexContent, .FrontModalContent').removeClass('active');
            $('.IndexContent.'+cardTitle).addClass('active');
        }
    }
})

$('.FrontModal .ModalLink').on('click', function(){
    var cardTitle = $(this).closest('.FrontModal').data('title');
    $('.IndexContent').removeClass('active');
    $('.FrontModalContent.'+cardTitle).addClass('active');
})

$('.FrontModalClose').on('click', function(){
    $(this).parents().find('.FrontModalContent').removeClass('active');
    $('.IndexContent, .IndexContainer').removeClass('active');
    $('.IndexContent.default').addClass('active');
})

$('.TimePeriodContainer span').on('click', function(){
    $('.TimePeriodContainer span').removeClass('active');
    $(this).addClass('active');
})

$('.addAppsContainer i, .CloseAddAppsContainer').on('click', function(){
    $('.AppFilterWidget').slideToggle('fast');
})

$('.streams').on('change',function(){
    if($(this).is(':checked')){
        $('.' + $(this).attr('id')).addClass('active');
    } else {
        $('.' + $(this).attr('id')).removeClass('active');
    }
})

$('.closeAppName').on('click', function(){
    $(this).closest('.AppNameContainer').removeClass('active');
})

$('.AppNameContainer').on('click', function(){
    $(this).toggleClass('selected');
})

/********************************************************* DEMO TOUR */
introJs().start();