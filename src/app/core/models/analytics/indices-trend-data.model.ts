export interface IndicesDurationDataModel {
  "sessionDurations": [{
    "daysBefore": number,
    "averageSessionLength": number
  }]
}

export interface IndicesLoyaltyDataModel {
  "loyaltyIndexTrends": [{
    "daysBefore": number,
    "churnPercentage": number
  }]
}

export interface IndicesRetentionDataModel {
  "retentionIndexTrends": [{
    "daysBefore": number,
    "percentageRetention": number
  }]
}

export interface IndicesFeedbackDataModel {
  "averageRatings": [{
    "daysBefore": number,
    "averageRating": number
  }]
}

export interface IndicesScoreDataModel {
  "indicatorModels": [{
    "indexName": string,
    "percentageChange": number
  }]
}