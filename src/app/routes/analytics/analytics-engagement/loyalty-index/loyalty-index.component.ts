import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '../../../../core/service/analytics/analytics.service';
import { LoyaltyIndexEngagementModel } from '../../../../core/models/analytics/loyalty-index-engagement.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-loyalty-index',
  templateUrl: './loyalty-index.component.html',
  styleUrls: ['./loyalty-index.component.scss']
})
export class LoyaltyIndexComponent implements OnInit {
  loyaltyIndexEngagementData: LoyaltyIndexEngagementModel;
  startDate: string = this.analyticsService.getFormattedDate(
    new Date().setDate(new Date().getDate() - 21)
  );
  endDate: string = this.analyticsService.getFormattedDate(new Date());

  // ToDo - Update code once API works properly
  // appId: string = this.analyticsService.getAppId();
  appId: string = '4ed062f1-fc38-4f32-a714-b63dc5399626';
  unit: string = 'week';
  filterChurns: string = '3w';
  activeApp : any;
  subscription: Subscription;

  constructor(private analyticsService: AnalyticsService) {}

  ngOnInit() {

    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();
    this.appId = this.activeApp.appId;

   
    this.getLoyaltyIndex();


    this.subscription = this.analyticsService.getActiveAppInfo().subscribe(activeApp => { 
    
      this.activeApp = activeApp.app;
      this.appId = this.activeApp.appId;
     
    this.getLoyaltyIndex();
      // console.log('retention graph updated',activeApp);
    });
  }

  getLoyaltyIndex() {
    this.analyticsService
      .getAppLoyaltyIndex({
        startDate: this.startDate,
        endDate: this.endDate,
        appId: this.appId,
        unit: this.unit,
        "platforms": [ "Android", "iOS" ]
      })
      .subscribe(data => {
        this.loyaltyIndexEngagementData = data;
      });
  }

  getFormattedDate(date): string {
    let today = new Date(date);
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!

    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = <any>'0' + dd;
    }
    if (mm < 10) {
      mm = <any>'0' + mm;
    }
    return yyyy + '-' + mm + '-' + dd;
  }

  changeParams() {
    this.endDate = this.getFormattedDate(new Date());
    let d = new Date();
    if (this.filterChurns == '3m' || this.filterChurns == '6m') {
      if (d.getMonth() < 3) {
        d.setFullYear(d.getFullYear() - 1);
        d.setMonth(d.getMonth() + 12 - <any>this.filterChurns.substring(0, 1));
      } else {
        d.setMonth(d.getMonth() - <any>this.filterChurns.substring(0, 1));
      }
      this.unit = 'month';
    }
    if (this.filterChurns == '6w') {
      d.setDate(d.getDate() - 42);
      this.unit = 'week';
    } else if (this.filterChurns == '3w') {
      d.setDate(d.getDate() - 21);
      this.unit = 'week';
    }
    this.startDate = this.getFormattedDate(d);
    this.updateGraph();
  }

  updateGraph() {
    this.loyaltyIndexEngagementData = null;
    this.analyticsService
      .getAppLoyaltyIndex({
        startDate: this.startDate,
        endDate: this.endDate,
        appId: this.appId,
        unit: this.unit,
        "platforms": [ "Android", "iOS" ]
      })
      .subscribe(data => {
        this.loyaltyIndexEngagementData = data;
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
