import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { stepData } from "../../referral-details/referral-data";

@Component({
  selector: "app-message",
  templateUrl: "./message.component.html",
  styleUrls: ["./message.component.scss"]
})
export class MessageComponent implements OnInit {
  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;

  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();
  @Output() draftRefData = new EventEmitter();
  @Output() editRefData = new EventEmitter();

  messageData: Object;
  constructor() {}

  ngOnInit() {
    this.messageData = this.referralData["screenConfiguration"]["message"];
  }

  /* change the step status */
  changeBackSetp() {
    this.changeStepStatus.emit(stepData.STEP1_RULE_CONFIGURATION);
  }

  changeNextStep() {
    this.changeStepStatus.emit(stepData.STEP3_HOME);
  }

  saveData() {
    this.saveRefData.emit();
  }
  saveAsDraft() {
    this.draftRefData.emit();
  }

  editReferral() {
    this.editRefData.emit();
  }
}
