import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoyatyIndexChartComponent } from './loyaty-index-chart.component';

describe('LoyatyIndexChartComponent', () => {
  let component: LoyatyIndexChartComponent;
  let fixture: ComponentFixture<LoyatyIndexChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoyatyIndexChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoyatyIndexChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
