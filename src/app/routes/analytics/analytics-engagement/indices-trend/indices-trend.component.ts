import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '../../../../core/service/analytics/analytics.service';
import { IndicesDurationDataModel, IndicesLoyaltyDataModel, IndicesRetentionDataModel, IndicesFeedbackDataModel } from '../../../../core/models/analytics/indices-trend-data.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-indices-trend',
  templateUrl: './indices-trend.component.html',
  styleUrls: ['./indices-trend.component.scss']
})
export class IndicesTrendComponent implements OnInit {

  indicesDurationData: IndicesDurationDataModel;
  indicesLoyaltyData: IndicesLoyaltyDataModel;
  indicesRetentionData: IndicesRetentionDataModel;
  indicesFeedbackData: IndicesFeedbackDataModel;
  appId: string = this.analyticsService.getAppId();
  activeApp : any;
  subscription: Subscription;

  constructor(private analyticsService: AnalyticsService) { }

  ngOnInit() {
    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();
    this.appId = this.activeApp.appId;

    this.updateGraph();
    this.subscription = this.analyticsService.getActiveAppInfo().subscribe(activeApp => { 
      this.activeApp = activeApp.app;
      this.appId = this.activeApp.appId;
      this.updateGraph();
    });
  }

  updateGraph() {
    this.analyticsService.getAppIndicesTrend({
        "appId": this.appId,
        "platforms": [ "Android", "iOS" ]
      }, "duration")
      .subscribe(data => {
        this.indicesDurationData = <IndicesDurationDataModel>data;
        console.log(this.indicesDurationData);
      });
    this.analyticsService.getAppIndicesTrend({
        "appId": this.appId,
        "platforms": [ "Android", "iOS" ]
      }, "loyalty")
      .subscribe(data => {
        this.indicesLoyaltyData = <IndicesLoyaltyDataModel>data;
      });
    this.analyticsService.getAppIndicesTrend({
        "appId": this.appId,
        "platforms": [ "Android", "iOS" ]
      }, "retention" )
      .subscribe(data => {
        this.indicesRetentionData = <IndicesRetentionDataModel>data;
      });
    this.analyticsService.getAppIndicesTrend({
        "appId": this.appId,
        "platforms": [ "Android", "iOS" ]
      }, "feedback" )
      .subscribe(data => {
        this.indicesFeedbackData = <IndicesFeedbackDataModel>data;
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
