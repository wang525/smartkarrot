import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoreTrendChartComponent } from './score-trend-chart.component';

describe('ScoreTrendChartComponent', () => {
  let component: ScoreTrendChartComponent;
  let fixture: ComponentFixture<ScoreTrendChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoreTrendChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoreTrendChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
