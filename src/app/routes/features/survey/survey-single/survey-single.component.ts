import { Component, OnInit, ViewChild } from '@angular/core';
import { SurveyConstants } from '../survey.constants';
import { ConfigQuestionComponent } from '../config-question/config-question.component';
import { ConfigThankyouComponent } from '../config-thankyou/config-thankyou.component';
import { Router, ActivatedRoute } from '@angular/router';
import * as _ from 'lodash';
declare var $;
import { SurveyService } from '../../../../core/service/survey/survey.service';

@Component({
  selector: 'app-survey-single',
  templateUrl: './survey-single.component.html',
  styleUrls: ['./survey-single.component.scss']
})
export class SurveySingleComponent implements OnInit {
  surveyMode: string;
  surveyName: any;
  surveyId: any;
  error: boolean = false;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private surveyService: SurveyService
  ) { }
  tabSelected: any;
  isWebApp: boolean;
  question: Object = {};
  questionConfig: Object = {};
  thankyouConfig: Object = {};
  sideBarList = SurveyConstants.SingleInputSideBar;
  @ViewChild(ConfigQuestionComponent) questionPage: ConfigQuestionComponent;
  @ViewChild(ConfigThankyouComponent) thankyouPage: ConfigThankyouComponent;

  ngOnInit() {
    this.route.params.subscribe(params => {
      if (params && params['id']) {
        this.surveyMode = 'edit';
        this.surveyService.getSurveyDetails(params['id']).subscribe((resp: any) => {
          this.surveyName = resp.survey.surveyName;
          this.surveyId = params['id']
          this.question = resp.survey.questions[0];
          this.editScreenConfig(resp.survey.screenConfig);
          this.tabSelected = 'config-question';
        });
      } else {
        this.initializeScreenConfig(
          {
            question: SurveyConstants.defaultQuestion,
            questionConfig: SurveyConstants.questionConfig,
            thankyou: SurveyConstants.thankyouConfig
          });
        this.tabSelected = 'config-question';
      }
    });
  }

  editScreenConfig(screenConfig) {
    let config: any = {};
    config.questionConfig = _.filter(screenConfig, { "configType": "questionScreen" })[0];
    config.thankyou = _.filter(screenConfig, { "configType": "thankYouScreen" })[0];
    config.question = false;
    this.initializeScreenConfig(config);
  }

  initializeScreenConfig(defaultData) {
    if (defaultData.question)
      this.question = Object.assign({}, defaultData.question);
    this.questionConfig = Object.assign({}, defaultData.questionConfig);
    this.thankyouConfig = Object.assign({}, defaultData.thankyou);
  }

  cancel() {
    $('#surveyCancelModal').modal('show');
  }

  save(draft = false) {
    this.updateScreenConfig();
    if (!this.validateInfo()) {

      if (this.surveyMode == 'edit') {
        this.surveyService.editSurvey(draft, this.isWebApp, this.surveyName, [this.question], [this.questionConfig, this.thankyouConfig], this.surveyId, 'feedback')
          .subscribe(resp => {
            $('#surveySuccessModal').modal('show');
          })
      } else {
        this.surveyService.createSurvey(draft, this.isWebApp, this.surveyName, [this.question], [this.questionConfig, this.thankyouConfig], 'feedback')
          .subscribe(resp => {
            $('#surveySuccessModal').modal('show');
          })
      }
    }
  }

  navigate() {
    this.router.navigate(['/features/survey-list']);
  }

  validateInfo() {
    this.error = !this.question['question'] || (/^ *$/.test(this.question['question'])) ? true : false;
    if (this.error)
      $('#surveyErrorModal').modal('show');
    return this.error;
  }

  onTabChange(event) {
    this.tabSelected = event;
  }

  openQuestion() {
    this.tabSelected = 'config-question';
  }

  updateScreenConfig() {
    if (this.questionPage) {
      this.questionConfig = Object.assign(this.questionConfig, this.questionPage.questionData);
      this.question = Object.assign(this.question, this.questionPage.question);
    }

    if (this.thankyouPage)
      this.thankyouConfig = Object.assign(this.thankyouConfig, this.thankyouPage.thankyouData);

    return [this.questionConfig, this.thankyouConfig];
  }
}

