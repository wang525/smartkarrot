import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingOptionsComponent } from './rating-options.component';

describe('RatingOptionsComponent', () => {
  let component: RatingOptionsComponent;
  let fixture: ComponentFixture<RatingOptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingOptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
