import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngagementHomeComponent } from './engagement-home.component';

describe('EngagementHomeComponent', () => {
  let component: EngagementHomeComponent;
  let fixture: ComponentFixture<EngagementHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngagementHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngagementHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
