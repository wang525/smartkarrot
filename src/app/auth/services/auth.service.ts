import { Injectable } from "@angular/core";

import "rxjs/add/operator/catch";
import "rxjs/add/operator/toPromise";
import {
  HttpClient,
  HttpClientJsonpModule
} from "../../../../node_modules/@angular/common/http";
import { Jsonp } from "@angular/http";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/toPromise";
import { environment } from "../../../environments/environment";
const API = {
  SEARCH: "https://itunes.apple.com/search?"
  // SEARCH: 'https://play.google.com/store/apps/details?id='
};
@Injectable({
  providedIn: "root"
})
export class AuthService {
  url =
    "https://itunes.apple.com/search?limit=5&media=software&term=Faceboo&callback=itunes";
  constructor(private http: HttpClient, private jsonp: HttpClientJsonpModule) {}

  search(searchTerm) {
    return this.http.jsonp(
      `${API.SEARCH}limit=7&media=software&term=${searchTerm}`,
      "callback"
    );
  }
  private handleError(error: any): Promise<any> {
    console.log(error);
    return Promise.reject(error.message || error);
  }
  // public search(critiria) {
  //   console.log(critiria);
  //   return this.http.get(
  //     `http://itunes.apple.com/search?term=yelp&country=us&entity=software`
  //   );
  // }
  signUp(form) {
    return this.http.post(`${environment.baseApiUrlAuth}/signup`, form);
  }

  signIn(form) {
    return this.http.post(`${environment.baseApiUrlAuth}/signin`, form);
  }

  updatePassword(form) {
    return this.http.post(`${environment.baseApiUrlAuth}/updatepassword`, form);
  }

  forgotpassword(form) {
    return this.http.post(`${environment.baseApiUrlAuth}/forgotpassword`, form);
  }
  mockcall() {
    return this.http.get("assets/jsons/users.json");
  }

  submitOnboardApps(onBoardData) {
    console.log(onBoardData);
    return this.http.post(
      `${environment.baseApiUrlAuth}/onboardapps`,
      onBoardData
    );
  }

  signOut(signoutData) {
    console.log(signoutData);
    return this.http.post(`${environment.baseApiUrlAuth}/signout`, signoutData);
  }

  requestInvite(data) {
    console.log(data);
    return this.http.post(`${environment.baseApiUrlAuth}/subscribe`, data);
  }

  fetchProfile(data) {
    console.log(data);
    return this.http.post(`${environment.baseApiUrlAuth}/fetchprofile`, data);
  }

  updateProfile(data) {
    console.log(data);
    return this.http.post(`${environment.baseApiUrlAuth}/updateprofile`, data);
  }

  sendVerifyOtp(data) {
    console.log(data);
    return this.http.post(
      `${environment.baseApiUrlAuth}/sendverifyphoneotp`,
      data
    );
  }

  verifyphone(data) {
    return this.http.post(`${environment.baseApiUrlAuth}/verifyphone`, data);
  }

  fectchAppDetails(appInfo) {
    return this.http.post(
      `${environment.baseApiUrlAuth}/fetchappdetails`,
      appInfo
    );
  }

  saveEditApp(appInfo) {
    return this.http.post(`${environment.baseApiUrlAuth}/editapp`, appInfo);
  }
}
