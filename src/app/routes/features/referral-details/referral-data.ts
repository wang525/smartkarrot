export class stepData {
  /* step status */
  public static STEP0_AUDIENCE = 0;
  public static STEP1_RULE_CONFIGURATION = 1;
  public static STEP2_MESSAGE = 2;
  public static STEP3_HOME = 3;
  public static STEP4_REFERRAL_COUNT = 4;
  public static STEP5_WITHDRAW = 5;
  public static STEP6_ADD_COUNT = 6;
  public static STEP7_WITHDRAWAL = 7;
  public static STEP8_ERROR_SCREEN = 8;
  public static STEP9_TECH_CONFIGURATION = 9;
}
// export interface SurveyModel {
//   id: number;
//   inputType: string;
//   question: string;
//   imageUrl: string;
//   icon: string;
//   value: string;
//   options: Array<string>;
// }
// export class ReferralConstants {
//   public static MultipleInputSideBar = [
//     {
//       id: 1,
//       key: "rule-config",
//       label: "Rule Configuration",
//       hasSub: false,
//       sub: []
//     },
//     {
//       id: 2,
//       key: "screen-config",
//       label: "Screen Configuration",
//       hasSub: true,
//       sub: [
//         { key: "message", label: "Message", isOptional: true },
//         { key: "home", label: "Home", isOptional: false },
//         { key: "referral-count", label: "Referral Count", isOptional: false },
//         { key: "withdraw", label: "Withdraw", isOptional: false },
//         { key: "add-count", label: "Add Account", isOptional: false },
//         { key: "withdrawal", label: "Withdrawal", isOptional: false },
//         { key: "error", label: "Error Screen", isOptional: false }
//       ]
//     },
//     {
//       id: 3,
//       key: "tech-config",
//       label: "Tech Configuration",
//       hasSub: false,
//       sub: []
//     }
//   ];

//   public static FontList = [
//     {
//       key: "NUNITOSANS",
//       value: "Nunito Sans Regular",
//       "font-family": "NunitoSansRegular",
//       "font-weight": "normal",
//       "font-style": "normal"
//     },
//     {
//       key: "NUNITOSANSBOLD",
//       value: "Nunito Sans Bold",
//       "font-family": "NunitoSansBold",
//       "font-weight": "bold",
//       "font-style": "normal"
//     },
//     {
//       key: "NUNITOSANSSEMIBOLD",
//       value: "Nunito Sans Semi Bold",
//       "font-family": "NunitoSansSemiBold",
//       "font-weight": "normal",
//       "font-style": "normal"
//     },
//     {
//       key: "NUNITOSANSITALICS",
//       value: "Nunito Sans Italics",
//       "font-family": "NunitoSansItalic",
//       "font-weight": "normal",
//       "font-style": "italic"
//     },
//     {
//       key: "NUNITOSANSLIGHT",
//       value: "Nunito Sans Light",
//       "font-family": "NunitoSansLight",
//       "font-weight": "normal",
//       "font-style": "normal"
//     }
//   ];

//   //default values

//   // public static defaultQuestion = {
//   //   'inputType': 'radio',
//   //   'question': '',
//   //   'imageUrl': 'https://source.unsplash.com/random',
//   //   'options': [
//   //     'Excellent',
//   //     'Great',
//   //     'Average',
//   //     'Unsatisfactory',
//   //     'Poor'
//   //   ],
//   //   'followUp': []
//   // }
//   // public static welcomeConfig = {
//   //   'configType': 'welcomeScreen',
//   //   'color': '#6b8e23',
//   //   'fontStyle': 'NUNITOSANS',
//   //   'title': 'Take a quick survey to share your experience',
//   //   'body': '',
//   //   'imageUrl': ''
//   // }
//   // public static questionConfig = {
//   //   'configType': 'questionScreen',
//   //   'color': '#6b8e23',
//   //   'fontStyle': 'NUNITOSANS',
//   //   'title': 'Customer Survey',
//   //   'body': '',
//   //   'imageUrl': 'https://source.unsplash.com/random'
//   // }
//   // public static thankyouConfig = {
//   //   'configType': 'thankYouScreen',
//   //   'color': '#6b8e23',
//   //   'fontStyle': 'NUNITOSANS',
//   //   'title': 'Thank you for your valuable time',
//   //   'body': '',
//   //   'imageUrl': ''
//   // }
// }
