import { Component, OnInit, AfterViewInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { SignInModel } from "./sign-in.model";
import { StorageService } from "../services/storage.service";
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";
const emailREGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
declare var $: any;
@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.scss"]
})
export class SignInComponent implements OnInit, AfterViewInit {
  signInModel: SignInModel = {
    emailId: "",
    password: "",
    deviceId: ""
  };
  form: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private authService: AuthService,
    private router: Router
  ) {
    $(window).on("load", function() {
      $("#SignIn").modal("show");
    });
  }
  ngAfterViewInit(): void {
    $("body").click();
  }
  showSignUp() {
    alert("here");
    $("#SignIn").modal("hide");
    $("#SignIn").on("hidden", function() {
      console.log("here");
      $("#SignUp").modal("show");
    });
  }

  ngOnInit() {
    $("#SignIn").modal({ backdrop: "static", keyboard: false });
    $("#SignIn").modal("show");
    this.form = this.formBuilder.group({
      email: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(emailREGEX)
        ])
      ],
      password: [null, Validators.required]
    });
  }
  cancel() {
    this.form.reset();
  }
  submit() {
    try {
      this.signInModel.emailId = this.form.value.email;
      this.signInModel.password = this.form.value.password;
      this.signInModel.deviceId = this.storageService.generateUUID();
      this.authService.signIn(this.signInModel).subscribe(
        data => {
          console.log(data);
          const status = data["status"];
          switch (status) {
            case "INVALID_INPUT":
              alert(status);
              break;
            case "INVALID_PASSWORD":
              alert(status);
              break;
            case "USER_NOT_FOUND":
              alert(status);
              break;
            case "ERROR":
              alert("Something went wrong");
              break;
            case "SUCCESS":
              $("#SignIn").modal("hide");
              this.storageService.setCurrentUser(data);
              const appSize = data["apps"].length;
              // console.log(JSON.stringify(data));
              if (appSize === 0) {
                this.router.navigate(["auth/getting-started"]);
              } else {
                this.router.navigate(["home"]);
              }
              break;
            default:
              alert("Something went wrong");
          }
        },
        error => {
          console.log(error);
        }
      );
    } catch (error) {}
    console.log(this.form.value);
  }
}
