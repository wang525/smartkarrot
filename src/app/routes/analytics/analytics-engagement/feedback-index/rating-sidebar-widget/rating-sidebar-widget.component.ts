import { Component, Input, OnChanges } from '@angular/core';
import { FeedbackIndexModel, FeedbackIndexReviewsModel } from '../../../../../core/models/analytics/feedback-index.model';

@Component({
  selector: 'app-rating-sidebar-widget',
  templateUrl: './rating-sidebar-widget.component.html',
  styleUrls: ['./rating-sidebar-widget.component.scss']
})
export class RatingSidebarWidgetComponent implements OnChanges {



  constructor() { }

  @Input() data: FeedbackIndexModel;
  @Input() sidebar: string;

  all_ratings_data: FeedbackIndexReviewsModel;



  ngOnChanges(): void {
    //this.heroService.getHeroes().subscribe(data => this.data = data);
    if (!this.data) { return; }

    this.createReviesChart();

  }

  public getSum(total, num) {
    return total + num;
  }

  private createReviesChart(): void {
    const data = this.data;
    const sidebar = this.sidebar;





    this.all_ratings_data = new FeedbackIndexReviewsModel();

    if (typeof (data) != 'undefined') {

      let all_d;
      if (sidebar == 'Current' && typeof (data.thisVersion) != 'undefined' && typeof (data.thisVersion.ratingCounts) != 'undefined') {


        all_d = data.thisVersion;

      } else if (sidebar == 'All' && typeof (data.allVersion) != 'undefined' && typeof (data.allVersion.ratingCounts) != 'undefined') {

        all_d = data.allVersion;
      }



      if (typeof (all_d) === null) {
        return;
      }


if(all_d !=null && all_d.ratingCounts !=null){
      let values = Object.values(all_d.ratingCounts);
      let keys = Object.keys(all_d.ratingCounts);


      // this.all_ratings_data.average =  parseFloat(this.all_ratings_data.average.toFixed(2));

      let td = this.all_ratings_data;

      let total_rate = values.reduce(this.getSum);
      td.type = sidebar;
      td.total_rating = total_rate;
      td.average = all_d.average;
      td.ratings = [{ id: 0, percentage: 0, count: 0 }];


      // convert to x and y array
      values.map(function (v: number, i: number) {

        if (i == 0) {
          td.ratings.shift();
        }
        let crcp = (v/td.total_rating)*100;

        
        td.ratings.push(
          {
            id: i,
            percentage: crcp,
            count: v
          }
        );


        return td.ratings;
      });



    
    }
  }
  }
}