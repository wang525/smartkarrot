import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

import { SegmentService } from "../../../core/service/segment/segment.service";
import { CommonService } from "../../../core/service/common/common.service";

@Component({
  selector: "app-user-group-list",
  templateUrl: "./user-group-list.component.html",
  styleUrls: ["./user-group-list.component.scss"]
})
export class UserGroupListComponent implements OnInit {
  headName = [
    { name: "User Segments", url: "segments/list", active: false },
    { name: "User Group", url: "segments/usergrouplist", active: true },
    { name: "Geo Fence", url: "segments/geofencing", active: false }
  ];

  private hideBar = false;

  userGroupList: any;
  group: Array<any> = [];
  private deleteGroup: any;
  private deleteID: any = {};

  constructor(
    public segmentService: SegmentService,
    private router: Router,
    public commonService: CommonService
  ) {}

  ngOnInit() {
    /* set header name to the header bar */
    this.commonService.setHeaderName(this.headName);

    this.hideBar = false;

    /* get list user group from api  */
    this.segmentService.getUserGroupList().then(res => {
      console.log("this.userGroupList", res);
      this.userGroupList = res;
    });
  }

  goToCreateGroup($event) {
    $event.preventDefault();
    this.router.navigate(["/segments/usergroup"]);
  }

  /* delete user Group */
  deleteUserGroup(groupId, appId) {
    let id = {};
    id["userGroupId"] = groupId;
    id["appId"] = appId;
    this.deleteID = id;

    $("#deleteGroup").trigger("click");
  }

  /* confirm to delete Segment */
  deletSelectedGroup($event) {
    $event.preventDefault();

    /* delete selecte segment from the screen list */
    let deletedGroup =
      this.userGroupList &&
      this.userGroupList.find(group => {
        return group.groupId == this.deleteID["userGroupId"];
      });

    if (deletedGroup) {
      this.userGroupList.splice(this.userGroupList.indexOf(deletedGroup), 1);
    }

    this.segmentService.deleteUserGroup(this.deleteID).then(res => {
      this.deleteGroup = res;
    });
  }
  /* Go to user group count */
  gotoUserGroupCount(eachGroup) {
    // let sendData = {
    //   appId: eachGroup.appId,
    //   groupId: eachGroup.groupId
    // };
    this.segmentService.setUserGroupListData(eachGroup);
    this.router.navigate(["/segments/usergroupcount"]);
  }

  editUserGroup(groupId, appId) {
    this.router.navigate(["/segments/usergroup", groupId, appId]);
  }

  duplicateUserGroup(groupId, appId) {
    let data = {
      userGroupId: groupId,
      appId: appId
    };
    this.segmentService.duplicateSingleUserGroup(data).then(res => {
      if (res["status"] == "SUCCESS") {
        this.segmentService.getUserGroupList().then(res => {
          this.userGroupList = res;
        });
      }
    });
  }

  isUserCont(count) {
    if (typeof count === "undefined") {
      return false;
    } else {
      return true;
    }
  }

}
