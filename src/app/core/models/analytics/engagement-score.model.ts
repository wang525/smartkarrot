export interface EngagementScoreModel {
  score: any,
  scoreMaturity: string,
  scorePercentile: number
}
