import { Component, OnChanges, ElementRef, ViewChild, Input } from '@angular/core';
import { DurationIndexSessionModel } from '../../../../../core/models/analytics/duration-index-session.model';
import { DurationIndexFrequencyModel } from '../../../../../core/models/analytics/duration-index-frequency.model';
import * as d3 from 'd3';

@Component({
  selector: 'app-duration-index-chart',
  templateUrl: './duration-index-chart.component.html',
  styleUrls: ['./duration-index-chart.component.scss']
})
export class DurationIndexChartComponent implements OnChanges {

  @ViewChild('chart')
  private chartContainer: ElementRef;

  @Input()
  data: Object

  @Input()
  modelType: string

  margin = {top: 20, right: 20, bottom: 30, left: 40};

  constructor() { }

  ngOnChanges(): void {
    if (!this.data) { return; }

    this.data = this.modelType == 'session' ? <DurationIndexSessionModel>this.data : <DurationIndexFrequencyModel>this.data;
    this.createChart();
  }

  private createChart(): void {
    const element = this.chartContainer.nativeElement;
    let data = this.modelType == 'session' ? this.data['iOS'] : this.data['iOS'];

    if (!data)
      return;

    const xValue = 'date';
    const yValue = this.modelType == 'session' ? 'length' : 'count';

    d3.select(element).select('svg').remove();

    const svg = d3.select(element).append('svg')
        .attr('width', element.offsetWidth)
        .attr('height', element.offsetHeight);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

   
    let x_ax_v = 0;

    let shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

    data.forEach(function(d) {
      d[xValue] = new Date(d[xValue]);
      let daa = +d[yValue];
      d[yValue] = daa;
    });

    const x = d3
      .scaleTime()
      .rangeRound([0, contentWidth])
      .domain(<any>d3.extent(data, function(d) {
        return d[xValue];
      }));

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([0, <any>d3.max(data, d => d[yValue])]);

    const line = d3.line()
        .x((d) => { return x(d[xValue]); })
        .y((d) => { return y(d[yValue]); });
        // .curve(d3.curveMonotoneX);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + contentHeight + ')')
      .call(d3.axisBottom(x)
        .ticks(6)
        .tickFormat(d3.timeFormat("%b %d"))
      );

      console.log("x_ax_v == ", x_ax_v);
    // if(x_ax_v == 0) {
      // var xaa = d3.scaleLinear()
      //   .domain([0,60])
      //   .range([contentHeight,0]);
      //     g.append('g')
      //   .attr('class', 'axis axis--y')
      //   .call(d3.axisLeft(xaa).ticks(5))
      //   .append('text')
      //     .attr('transform', 'rotate(-90)')
      //     .attr('y', 6)
      //     .attr('dy', '0.71em')
      //     .attr('text-anchor', 'end');
    // } else {
      g.append('g')
      .attr('class', 'axis axis--y')
      .call(d3.axisLeft(y).ticks(5))
      .append('text')
        .attr('transform', 'rotate(-90)')
        .attr('y', 6)
        .attr('dy', '0.71em')
        .attr('text-anchor', 'end');
    // }

    // add the X gridlines
    // svg.append("g")
    //   .attr("class", "grid")
    //   .attr("transform", "translate(0," + height + ")")
    //   .call(d3.axisBottom(x).ticks(5)
    //       .tickSize(-height)
    //       .tickFormat(""));

    svg.append("g")      
      .attr("class", "grid")
      .attr("transform", "translate(40, 20)")
      .attr('stroke', '#ccc')
      .call(d3.axisLeft(y)
        .ticks(5)
        .tickSize(-contentWidth)
        .tickFormat(<any>"")
      )

    if (this.data['iOS'] && this.data['android']) {
      for (let i = 2 - 1; i >= 0; i--) {
        let lineData = i == 1 ? this.data['iOS'] : this.data['android'];
        let lineColor = i == 1 ? "#ff8800" : "#5306E0";

        lineData.forEach(function(d) {
          d[xValue] = new Date(d[xValue]);
          let daa = +d[yValue];
          d[yValue] = daa;
        });
        // console.log(lineData);

        g.append("path")
          .attr("class", "path")
          .attr("fill", "none")
          .attr("stroke", lineColor)
          .attr("stroke-width", 1.5)
          .attr("class", "line")
          .attr("d", line(lineData));

        g.selectAll(".dot")
          .data(lineData)
          .enter().append("circle")
          .attr("class", "dot")
          .attr("cx", function(d) { return x(d[xValue]) })
          .attr("cy", function(d) { return y(d[yValue]) })
          .attr("fill", lineColor)
          .attr("r", 5)
          .attr("title", (d) => d[yValue])
          // .on("mouseover", function(a, b, c) {
          //   console.log(a);
          //   // this.attr('class', 'focus');
          // })
          // .on("mouseout", function() {  })

        g.append('g').selectAll("text")
          .data(lineData)
          .enter()
          .append("text")
          .attr("x", function(d, i) {
            return x(d[xValue]);
          })
          .attr("y", function(d, i) {
            let yVal = parseInt(d[yValue]) > 0 ? y(d[yValue]) + 15 : y(d[yValue]) - 10;
            if (lineData[i + 1] && lineData[i + 1][yValue] < d[yValue]) {
              yVal = y(d[yValue]) - 10;
            } else if (lineData[i + 1] && parseInt(lineData[i + 1][yValue]) > parseInt(d[yValue])) {
              yVal = y(d[yValue]) + 15;
            }
            return yVal;
          })
          .attr("fill", lineColor)
          .style("font-size", "12px")
          .text(function(d) { return parseInt(d[yValue]); });
      }
    }
  }
}
