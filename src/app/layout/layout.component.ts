import { Component, OnInit, HostBinding } from '@angular/core';

import { SettingsService } from '../core/settings/settings.service';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    @HostBinding('class.sidebar-hidden') get isFixed() { return this.settings.getLayoutSetting('sidebarHidden'); };
    @HostBinding('class.topbar-hidden') get isTopbarHidden() { return this.settings.getLayoutSetting('topbarHidden'); };

    constructor(public settings: SettingsService) { }

    ngOnInit() {
        
    }

}
