import { Component, OnInit, AfterViewInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ForgotPasswordModel } from "./forgotpassword.model";
import { StorageService } from "../services/storage.service";
import { AuthService } from "../services/auth.service";
import { Router } from "@angular/router";
const emailREGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
declare var $: any;

@Component({
  selector: "app-forgot-password",
  templateUrl: "./forgot-password.component.html",
  styleUrls: ["./forgot-password.component.scss"]
})
export class ForgotPasswordComponent implements OnInit {
  ForgotPasswordModel: ForgotPasswordModel = {
    emailId: "",
    deviceId: ""
  };
  form: FormGroup;
  loadingGif: string = "../assets/images/loading.gif";
  constructor(
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private authService: AuthService,
    private router: Router
  ) {
    $(window).on("load", function() {
      $("#forgotPwd").modal("show");
    });
  }
  ngAfterViewInit(): void {
    $("body").click();
  }

  ngOnInit() {
    $("#forgotPwd").modal({ backdrop: "static", keyboard: false });
    $("#forgotPwd").modal("show");
    this.form = this.formBuilder.group({
      email: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(emailREGEX)
        ])
      ]
    });
  }
  cancel() {
    this.router.navigate(["auth/sign-in"]);
    this.form.reset();
  }
  submit() {
    try {
      this.ForgotPasswordModel.emailId = this.form.value.email;
      this.ForgotPasswordModel.deviceId = this.storageService.generateUUID();
      // $("#loadingicon").modal("show");
      this.authService.forgotpassword(this.ForgotPasswordModel).subscribe(
        data => {
          console.log(data);
          const status = data["status"];
          switch (status) {
            case "INVALID_INPUT":
              alert(status);
              break;
            case "INVALID_PASSWORD":
              alert(status);
              break;
            case "USER_NOT_FOUND":
              alert(status);
              break;
            case "ERROR":
              alert("Something went wrong");
              break;
            case "SUCCESS":
              $("#forgotPwd").modal("hide");
              this.storageService.setCurrentUser(data);
              // $("#loadingicon").modal("hide");
              $("#forgotPwdsent").modal({
                backdrop: "static",
                keyboard: false
              });
              break;
            default:
              alert("Something went wrong");
          }
        },
        error => {
          console.log(error);
        }
      );
    } catch (error) {}
    console.log(this.form.value);
  }

  close() {
    this.router.navigate(["auth/sign-in"]);
  }
}
