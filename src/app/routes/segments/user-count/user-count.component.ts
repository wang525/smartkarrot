import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ɵConsole
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SettingsService } from "../../../core/settings/settings.service";
import { SegmentService } from "../../../core/service/segment/segment.service";
declare var $;

@Component({
  selector: "app-user-count",
  templateUrl: "./user-count.component.html",
  styleUrls: ["./user-count.component.scss"]
})
export class UserCountComponent implements OnInit, OnDestroy {
  userLists: any;
  moreUserList: any;
  nextToken: any;
  userID = "";
  totalUserCount: any;
  isExcluded = "";
  path: any;

  saveFlag = false;

  searchText = "";

  private saveUserList: Array<any> = [];

  constructor(
    public segmentService: SegmentService,
    public settings: SettingsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.settings.setLayoutSetting("sidebarHidden", true);
    this.settings.setLayoutSetting("topbarHidden", true);
  }

  ngOnInit() {
    /* get the segment id of the individual segment */
    this.path = this.route.params.subscribe(params => {
      this.isExcluded = params["id"];
    });

    let data = this.segmentService.getUserListData();

    console.log("data for excluded user ===", data)

    this.saveUserList = [];

    if (this.isExcluded) {
      let editState = this.segmentService.getEditSegmentState();
      this.userLists = this.segmentService.getTmpUserSaveListData();
      if (editState != true) {
        if (this.userLists && this.userLists.length > 0) {
          this.showDatatableContent();
          this.saveUserList = JSON.parse(JSON.stringify(this.userLists));
        }
      }
      else {
        let excludedUserIds = this.segmentService.getExcludedUserEditListData();
        if (excludedUserIds && excludedUserIds.length > 0) {
          data['forExcludedList'] = true;
          console.log("send data for excluded users ===", data)
          this.segmentService.getUserList(data).then(res => {
            this.totalUserCount = res["totalUserCount"];
            this.nextToken = res["nextToken"];
            this.userLists = res["users"];
            console.log("get saved this.userLists==>", this.userLists);
    
            this.showExcludedUser();
            this.showDatatableContent();
          });
        }
      }
    } 
    else {
      /* get user lists */
      this.segmentService.getUserList(data).then(res => {
        this.totalUserCount = res["totalUserCount"];
        this.nextToken = res["nextToken"];
        this.userLists = res["users"];

        this.showExcludedUser();
        this.showDatatableContent();
      });
    }

    $(function() {
      $(".includeState").on("click", function() {
        if ($(this).hasClass("active")) {
          $(this).removeClass("active");
          $(this).text("Include Back");
        } else {
          $(this).addClass("active");
          $(this).text("Exclude");
        }
      });
    });
  }

  showExcludedUser() {
    let editState = this.segmentService.getEditSegmentState();
    let excludedUser = this.segmentService.getTmpUserSaveListData();
    let excludedUserIds = this.segmentService.getExcludedUserEditListData();
    if (editState != true) {
      if (excludedUser && excludedUser.length > 0) {
        if (this.userLists && this.userLists.length > 0) {
          this.saveUserList = [];
          for (var i = 0; i < this.userLists.length; i++) {
            for (var j = 0; j < excludedUser.length; j++) {
              if (this.userLists[i].userId == excludedUser[j].userId) {
                this.userLists[i].isExcluded = true;
                this.saveUserList.push(JSON.parse(JSON.stringify(this.userLists[i])));
                break;
              }
            }
          }
        }
      }
    }
    else {
      let tmpdata = this.segmentService.getTmpUserListData();
      if (JSON.stringify(tmpdata) == '{}') {
        let segInfo = this.segmentService.getUserListData();
        if (segInfo) { 
          let indivEditSegment = this.segmentService.getIndividualSegment( segInfo.segmentId );
          if (indivEditSegment['excludedUserIds'] && indivEditSegment['excludedUserIds'].length > 0) {
            excludedUserIds =  indivEditSegment['excludedUserIds'];
          }
          else {
            excludedUserIds = [];
          }
        }
      }

      if (excludedUserIds && excludedUserIds.length > 0) {
        if (this.userLists && this.userLists.length > 0) {
          this.saveUserList = [];
          for (var i = 0; i < this.userLists.length; i++) {
            for (var j = 0; j < excludedUserIds.length; j++) {
              if (this.userLists[i].userId == excludedUserIds[j]) {
                this.userLists[i].isExcluded = true;
                this.saveUserList.push(JSON.parse(JSON.stringify(this.userLists[i])))
                break;
              }
            }
          }
        }
        if (this.isExcluded) {
          this.userLists = [];
          this.userLists = JSON.parse(JSON.stringify(this.saveUserList));
        }
      }
    }
  }
  
  showDatatableContent() {
    $(function() {
      $("#data-table-content").DataTable({
        paging: false,
        info: false,
        scrollY: "calc(100vh - 170px)",
        scrollCollapse: true,
        searching: false,
        responsive: true
      });
    });
  }

  searchInUsercount(searchText) {
    if (this.saveFlag == false) {
      this.saveFlag = true;
      
      let data = this.segmentService.getUserListData();
      if (searchText != '') {
        data.searchText = searchText;
      }
      else {
        if (data.searchText) {
          delete data.searchText;
        }
      }
      this.segmentService.getUserList(data).then(res => {
        this.saveFlag = false;
        this.totalUserCount = res["totalUserCount"];
        this.nextToken = res["nextToken"];
        this.userLists = res["users"];
      });
    }
  }

  fetchMoreUsersInSegment($event) {
    if (this.saveFlag == false) {
      this.saveFlag = true;
      
      let data = this.segmentService.getUserListData();
      data.nextToken = this.nextToken;
      /* get user lists */
      this.segmentService.getUserList(data).then(res => {
        this.saveFlag = false;
        this.totalUserCount = res["totalUserCount"];
        this.nextToken = res["nextToken"];
        this.moreUserList = res["users"];
        this.userLists = this.userLists.concat(this.moreUserList);
        this.showExcludedUser();
        console.log("Total ", this.userLists.length);
      });
    }
  }

  ngOnDestroy() {
    this.settings.setLayoutSetting("sidebarHidden", false);
    this.settings.setLayoutSetting("topbarHidden", false);
  }

  /* Cancel function */
  setCancelFunc($event) {
    $event.preventDefault();
    
    let tmpdata = this.segmentService.getTmpUserListData();
    if (JSON.stringify(tmpdata) != '{}' && tmpdata["segmentId"]) {
      this.router.navigate(["/segments/edit", tmpdata["segmentId"]]);
    } else if (JSON.stringify(tmpdata) != '{}' && !tmpdata["segmentId"]) {
      this.router.navigate(["/segments/create"]);
    } else {

      this.router.navigate(["/segments/list"]);
    }
  }

  /* Done function */
  setDoneFunc($event) {
    $event.preventDefault();
    if (this.saveFlag == false) {
      this.saveFlag = true;
      let tmpdata = this.segmentService.getTmpUserListData();
      this.segmentService.setTmpUserSaveListData(this.saveUserList);

      this.segmentService.setEditSegmentState(false);
      this.segmentService.setExcludedUserEditListData(null);

      if (JSON.stringify(tmpdata) != '{}' && tmpdata["segmentId"]) {
        this.router.navigate(["/segments/edit", tmpdata["segmentId"]]);
      } 
      else if (JSON.stringify(tmpdata) != '{}' && !tmpdata["segmentId"]) {
        this.router.navigate(["/segments/create"]);
      } 
      else {
        let segInfo = this.segmentService.getUserListData();
        if (segInfo) { 
          let indivEditSegment = this.segmentService.getIndividualSegment( segInfo.segmentId );
          delete indivEditSegment['createdAt'];
          delete indivEditSegment['createdByOrgOfficerId'];
          delete indivEditSegment['str'];
          delete indivEditSegment['updatedAt'];
          delete indivEditSegment['updatedByOrgOfficerId'];

          indivEditSegment['excludedUserIds'] = [];
          if (this.saveUserList && this.saveUserList.length > 0) {
            this.saveUserList.map(saveUserlist => {
              indivEditSegment['excludedUserIds'].push(saveUserlist.userId);
            })
          }
          this.segmentService.updateSegments(indivEditSegment).then(res => {
            this.saveFlag = false;
            if (res == 'SUCCESS') {
              console.log('update data success.');
              this.router.navigate(["/segments/list"]);
              return;
            } else {
              console.log('update data failed.');
              this.router.navigate(["/segments/list"]);
            }
          });
        }
      }
    }
  }

  changeUserStatus(userInfo, status) {
    userInfo.isExcluded = status;

    if (status == true) {
      this.saveUserList.push(userInfo);
    }
    else {
      let resRemoveIdx = this.saveUserList.indexOf(userInfo.userId);
      this.saveUserList.splice(resRemoveIdx, 1);
    }
    console.log("saved user list===>", this.saveUserList);
  }
}
