import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root"
})
export class CommonService {
  public headNames = [];
  constructor() {}

  /* components set header name */
  setHeaderName(name) {
    if (name && name.length > 0) {
      this.headNames = [];
      for (var i = 0; i < name.length; i++) {
        let headName = name[i];
        this.headNames.push(headName);
      }
    }
  }

  /* get header name */
  getHeaderName() {
    return this.headNames;
  }
}
