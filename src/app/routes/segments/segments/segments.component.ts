import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "../../../core/service/common/common.service";

@Component({
  selector: "app-segments",
  templateUrl: "./segments.component.html",
  styleUrls: ["./segments.component.scss"]
})
export class SegmentsComponent implements OnInit {
  headName = [
    { name: "User Group", url: "segments/usergroup" },
    { name: "User Segments", url: "segments/list" },
    { name: "Geo Fencing", url: "segments/geofencing" }
  ];
  constructor(public commonService: CommonService, private router: Router) {}

  ngOnInit() {
    /* set header name to the header bar */
    this.commonService.setHeaderName(this.headName);
  }

  goToCreateSegment($event) {
    $event.preventDefault();

    this.router.navigate(["/segments/new"]);
  }
  goToUserCount($event) {
    $event.preventDefault();
    this.router.navigate(["/segments/usercount"]);
  }
}
