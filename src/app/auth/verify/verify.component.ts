import { Component, OnInit, AfterViewInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { StorageService } from "../services/storage.service";
import { AuthService } from "../services/auth.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { VerifyModel } from "./verify.model";
declare var $: any;
@Component({
  selector: "app-verify",
  templateUrl: "./verify.component.html",
  styleUrls: ["./verify.component.scss"]
})
export class VerifyComponent implements OnInit, AfterViewInit {
  VerifyModel: VerifyModel = {
    password: "",
    deviceId: "",
    token: "",
    userId: "",
    oldPassword: ""
  };
  form: FormGroup;
  errorMessage: string;
  token: string;
  constructor(
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    $(window).on("load", function() {
      $("#Verify").modal("show");
    });
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      console.log("paramsparamsparamsparams  " + JSON.stringify(params));
      this.token = params["token"];
    });
  }
  ngAfterViewInit(): void {
    $("body").click();
  }

  ngOnInit() {
    $("#Verify").modal({ backdrop: "static", keyboard: false });
    $("#Verify").modal("show");
    this.form = this.formBuilder.group({
      password: [null, Validators.required],
      rpwd: [null, Validators.required]
    });
  }
  cancel() {
    this.form.reset();
  }
  submit() {
    this.VerifyModel.password = this.form.value.password;
    // this.VerifyModel.oldPassword = this.form.value.rpwd;
    this.VerifyModel.deviceId = this.storageService.generateUUID();
    this.VerifyModel.token = this.token;
    // this.VerifyModel.userId = localStorage.getItem("currentUser");

    if (this.form.value.password != this.form.value.rpwd) {
      this.errorMessage =
        "Re-enter password didn't Match with Password entered";
    } else {
      console.log("coming here" + JSON.stringify(this.VerifyModel));
      try {
        this.authService.updatePassword(this.VerifyModel).subscribe(
          data => {
            console.log("datadatatad  " + JSON.stringify(data));
            const status = data["status"];
            switch (status) {
              case "INVALID_INPUT":
                alert(status);
                break;
              case "EXPIRED_TOKEN":
                alert(status);
                break;
              case "ERROR":
                alert("Something went wrong");
                break;
              case "SUCCESS":
                $("#Verify").modal("hide");
                this.storageService.setCurrentUser(data);
                this.router.navigate(["auth/sign-in"]);
                break;
              default:
                alert("Something went wrong");
            }
          },
          error => {
            console.log(error);
          }
        );
      } catch (error) {}
    }
    console.log(this.form.value);
  }
}
