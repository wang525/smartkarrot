import { AbstractControl } from '@angular/forms';
import * as googleLibphonenumber from 'google-libphonenumber';
export function phoneNoValidation(control: AbstractControl) {
  if (control.value) {
    try {
      const phoneUtil = googleLibphonenumber.PhoneNumberUtil.getInstance();
      const phoneNumber = phoneUtil.parse(control.value);
      console.log(phoneNumber.values_);
      if (!phoneNumber.values_) {
        return {
          isInValid: true
        };
      }
      return null;
    } catch (error) {
      console.log(error);
      return { isInValid: true };
    }

    //  console.log(isInValid);
  }
}
