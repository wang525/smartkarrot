import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ɵConsole
} from "@angular/core";
import { stepData } from "../../referral-details/referral-data";
import { DomSanitizer } from "@angular/platform-browser";
import { ImageUploadService } from "../../../../core/service/image-upload/image-upload.service";

declare var $;

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;
  @Input() applyAll;
  @Output() applySelected = new EventEmitter();

  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();
  @Output() editRefData = new EventEmitter();

  homeData: Object;
  applyColor: boolean;
  applyText: boolean;
  referralHomeText: any;
  referralHomeScreen: any;

  constructor(
    private imageUploadSrvc: ImageUploadService,
    private _sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.applyColor = true;
    this.applyText = true;
    this.homeData = this.referralData["screenConfiguration"].home;
    this.referralHomeText = this.homeData["referralHomeText"];
    this.referralHomeScreen = this.homeData["referralHomeScreen"];
  }

  /* change the step status */
  changeBackSetp() {
    this.changeStepStatus.emit(stepData.STEP2_MESSAGE);
  }

  changeNextStep() {
    this.changeStepStatus.emit(stepData.STEP4_REFERRAL_COUNT);
  }

  fileChange(event) {
    let file: File = event.target.files[0];
    if (file["type"] == "image/jpeg" || file.type == "image/png")
      this.uploadFile(file);
    else alert("Please upload Jpeg or Png format. ");
  }

  uploadFile(file) {
    this.homeData["referralHomeText"][
      "logoIconURL"
    ] = this._sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));
    this.imageUploadSrvc.getImageUrl().subscribe(async (resp: any) => {
      await this.imageUploadSrvc.uploadImageUrl(resp.url, file);
      this.homeData["referralHomeText"]["logoIconURL"] =
        this.imageUploadSrvc.getImageLoadUrl() + resp.key;
      console.log(this.homeData["referralHomeText"]["logoIconURL"]);
    });
  }

  saveData() {
    this.saveRefData.emit();
  }

  editReferral() {
    this.editRefData.emit();
  }

  applyColorApp() {
    this.applyColor = this.applyColor ? false : true;
  }

  applyTextApp() {
    this.applyText = this.applyText ? false : true;
  }

  onApplySelected(event) {
    this.applySelected.emit(event);
  }
}
