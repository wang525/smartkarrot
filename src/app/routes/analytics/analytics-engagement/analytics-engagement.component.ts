import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from "../../../core/service/analytics/analytics.service";
// import { IndicesScoreDataModel } from "../../../core/models/analytics/indices-trend-data.model"
import { Subscription } from "rxjs";

@Component({
  selector: 'app-analytics-engagement',
  templateUrl: './analytics-engagement.component.html',
  styleUrls: ['./analytics-engagement.component.scss']
})
export class AnalyticsEngagementComponent implements OnInit {
  activeApp: any;
  indicesScore: any;
  durationPercentageChange: number;
  loyaltyPercentageChange: number;
  retentionPercentageChange: number;
  feedbackPercentageChange: number;
  subscription: Subscription;

  constructor(private analyticsService: AnalyticsService) { }

  ngOnInit() {
    if (!this.analyticsService.getActiveAppInfoOnLoad())
      this.analyticsService.updateActiveApp(null);

    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();

    this.subscription = this.analyticsService
      .getActiveAppInfo()
      .subscribe(activeApp => {
        this.activeApp = activeApp.app;
      });

    this.analyticsService.getIndicesScore({
        appId: this.activeApp.appId,
        "platforms": [ "Android", "iOS" ]
      }).subscribe( (data) => {
        this.indicesScore = data.indicatorModels.map((d) => {
              switch (d.indexName) {
                case 'DurationIndex':
                  this.durationPercentageChange = d.percentageChange;
                  // d['indexTitle'] = 'Duration Index';
                  // d['indexDesc'] = 'Measure of Session Length &amp; Frequency';
                  // d['routerLink'] = '/analytics/engagement/duration-index';
                  break;
                case 'LoyaltyIndex':
                  this.loyaltyPercentageChange = d.percentageChange;
                  // d['indexTitle'] = 'Loyalty Index';
                  // d['indexDesc'] = 'Measure of Churn &amp; Trends';
                  break;
                case 'RetentionIndex':
                  this.retentionPercentageChange = d.percentageChange;
                  // d['indexTitle'] = 'Retention Index';
                  // d['indexDesc'] = 'Measure of Active User Cohorts/Passing Duration';
                  break;
                case 'FeedbackIndex':
                  this.feedbackPercentageChange = d.percentageChange;
                  // d['indexTitle'] = 'Feedback Index';
                  // d['indexDesc'] = 'Measure of App Store Ratings &amp; Trends';
                  break;
              }
              return d;
          });
      });
  }

}
