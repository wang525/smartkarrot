import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DurationIndexChartComponent } from './duration-index-chart.component';

describe('DurationIndexChartComponent', () => {
  let component: DurationIndexChartComponent;
  let fixture: ComponentFixture<DurationIndexChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DurationIndexChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DurationIndexChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
