import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { stepData } from "../../referral-details/referral-data";
import { StorageService } from "../../../../auth/services/storage.service";
import { SegmentService } from "../../../../core/service/segment/segment.service";
import { ReferralService } from "../../../../core/service/referral/referral.service";

@Component({
  selector: "app-audience",
  templateUrl: "./audience.component.html",
  styleUrls: ["./audience.component.scss"]
})
export class AudienceComponent implements OnInit {
  /* get the product App Id list */
  private productAppList: any;

  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;

  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();
  @Output() draftRefData = new EventEmitter();
  @Output() attributes = new EventEmitter();
  @Output() editRefData = new EventEmitter();

  private incdecNum = 5;
  private possibleAttrValues = {};
  private id: any;
  isShowSetEvent = false;

  gamificationRuleData = [0];
  textNotificationData = [0];
  inAppNotificaiotnData = [0];

  constructor(
    private storageService: StorageService,
    public segmentService: SegmentService,
    public referralService: ReferralService
  ) {}

  ngOnInit() {
    let organizationId = {
      organizationId: this.storageService.getCurrentUser().user.organizationId
    };
    this.segmentService.getProductAppIdList(organizationId).then(res => {
      this.productAppList = res;
    });
  }

  changeNextStep() {
    this.changeStepStatus.emit(stepData.STEP1_RULE_CONFIGURATION);
  }

  saveAsDraft() {
    this.draftRefData.emit();
  }

  editReferral() {
    this.editRefData.emit();
  }

  saveData() {
    this.saveRefData.emit();
  }

  attributedata() {
    console.log("coming here");
    this.attributes.emit();
  }
}
