import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DurationIndexComponent } from './duration-index.component';

describe('DurationIndexComponent', () => {
  let component: DurationIndexComponent;
  let fixture: ComponentFixture<DurationIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DurationIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DurationIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
