import { Component, OnInit, AfterViewInit, ViewChild } from "@angular/core";
import { AuthService } from "../services/auth.service";
import { GettingStarted } from "./getting-started-model";
import { StorageService } from "../services/storage.service";
import { Router } from "../../../../node_modules/@angular/router";
import { FormGroup, Validators, FormControl } from "@angular/forms";

declare var $: any;
@Component({
  selector: "app-getting-started",
  templateUrl: "./getting-started.component.html",
  styleUrls: ["./getting-started.component.scss"]
})
export class GettingStartedComponent implements AfterViewInit, OnInit {
  generalForm: FormGroup;
  accessToken: string;
  userId: string;
  verifiedIcon: String = "assets/images/verified (1).png";
  verifyIcon: String = "assets/images/warningTran.png";
  androidIcon: String = "assets/images/android.png";
  appleIcon: String = "assets/images/apple.png";
  appRating: number;
  base64binaryString: any;
  // platformConfigured: string;
  platformConfiguredIOS: string;
  platformConfiguredAndroid: string;
  platformConfiguredWeb: string;
  SmartEnagagementScore: string =
    "Configuring below options will help us to get loyalty and feedback metrics which are used to calculate your app engagement score.";
  AppStoreID: string =
    "Helps us to get your app's user feedback and app ratings from store.";
  PushNotification: string =
    "Helps us to track app uninstalls using silent push notifications.  ";
  FeedBack: string =
    "Helps us to get your app's user feedback and app ratings from store. ";
  constructor(
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router
  ) {
    let currentUser = this.storageService.getCurrentUser();
    let user = currentUser.user;
    this.userId = user.userId;
    this.accessToken = currentUser.accessToken;
    // this.organizationId = user.organizationId;
  }
  inputImageLogo = "assets/images/App1-small.png";
  signedUpResponse = {};
  // gettingStarted: GettingStarted = {
  //   platform: "",
  //   appName: "",
  //   appRating: 0,
  //   bundleIdentitfier: "",
  //   appStoreId: "",
  //   isVerified: false,
  //   appUserCount: 0,
  //   apikey: "",
  //   clientId: "",
  //   clientSecret: "",
  //   refreshToken: "",
  //   filePassword: "",
  //   domainName: ""
  // };

  @ViewChild("f") form: any;
  searchResults = [];
  artistId = 0;
  showAndroidField: boolean;
  selectedArtist: string;
  ngAfterViewInit() {
    $(function() {
      $("#scroll").slimScroll({
        height: "auto",
        color: "#00f",
        size: "2px",
        alwaysVisible: true
      });
    });
  }

  reset() {
    this.generalForm.reset();
  }

  // platformChanged(platform) {
  //   this.gettingStarted.bundleIdentitfier = "";
  //   this.gettingStarted.appName = "";
  //   this.gettingStarted.appStoreId = "";
  //   try {
  //     if (platform === "Android") {
  //       this.showAndroidField = true;
  //     } else {
  //       this.showAndroidField = false;
  //       // this.form.reset();
  //     }
  //   } catch (error) {}
  // }

  // submit() {
  //   try {
  //     const formData = {
  //       apps: this.signedUpResponse["apps"],
  //       userId: this.signedUpResponse["user"].userId,
  //       accessToken: this.signedUpResponse["accessToken"]
  //     };
  //     this.authService.submitOnboardApps(formData).subscribe(
  //       data => {
  //         console.log(data);
  //         const status = data["status"];
  //         switch (status) {
  //           case "INVALID_INPUT":
  //             alert(status);
  //             break;
  //           case "SUCCESS":
  //             this.router.navigate(["home"]);
  //             break;
  //           default:
  //             break;
  //         }
  //       },
  //       error => {
  //         console.log(error);
  //       }
  //     );
  //   } catch (error) {}
  // }

  add(gettingStarted) {
    console.log("6874548");
    console.log(this.form.value);
    console.log(gettingStarted);
    const index = this.signedUpResponse["apps"].findIndex(
      x => x.bundleIdentitfier === this.form.value.bundleIdentitfier
    );
    if (index === -1) {
      this.signedUpResponse["apps"].push(this.form.value);
    } else {
      alert("already added");
    }
  }

  search() {
    console.log("HELLO");
    let searchTerm = (<HTMLInputElement>document.getElementById("appName"))
      .value;
    console.log(searchTerm);
    let iOSSelected = document
      .getElementById("ios-tab")
      .getAttribute("aria-selected");
    let androidSelected = document
      .getElementById("android-tab")
      .getAttribute("aria-selected");
    if (iOSSelected) {
      this.authService.search(searchTerm).subscribe(response => {
        console.log(response);
        this.searchResults = response["results"];
        console.log(this.searchResults);
        //  $('#appNameInput').autocomplete as any({ source: this.searchResults });
      });
    }
  }

  // onInputChanged(name) {
  //   this.searchResults.forEach(element => {
  //     console.log(element.trackCensoredName, name);
  //     if (element.trackCensoredName === name) {
  //       this.gettingStarted.bundleIdentitfier = element.bundleId;
  //       this.gettingStarted.appStoreId = element.trackId;
  //     } else {
  //     }
  //   });
  // }

  deleteApp(index) {
    console.log(index);
    this.signedUpResponse["apps"].splice(index, 1);
    console.log(this.signedUpResponse["apps"]);
  }
  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.generalForm = new FormGroup({
      appName: new FormControl(""),
      bundleIdentitfier: new FormControl(""),
      ApiKey: new FormControl(""),
      clientId: new FormControl(""),
      clientSecretKey: new FormControl(""),
      RefreshToken: new FormControl(""),
      appStoreId: new FormControl(""),
      domainName: new FormControl(""),
      file: new FormControl(""),
      password: new FormControl(""),
      packageName: new FormControl(""),
      iosSandboxAPNSCertificateKey: new FormControl(""),
      iosSandboxAPNSCertificateKeyPassword: new FormControl("")
    });
  }

  getBase64() {
    let file = $("#file")[0].files[0];
    console.log("file" + file);
    if (file) {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
      });
    } else {
      return new Promise((resolve, reject) => {
        console.log("file is not added");
      });
    }
  }

  onSubmit() {
    console.log("this.form.value" + JSON.stringify(this.generalForm.value));
    // if (
    //   (this.generalForm.value &&
    //     this.generalForm.value.bundleIdentitfier != null) ||
    //   this.generalForm.value.appStoreId != null ||
    //   this.base64binaryString != null ||
    //   this.generalForm.value.password != null
    // ) {
    //   this.platformConfigured = "iOS";
    // } else if (
    //   (this.generalForm.value && this.generalForm.value.packageName) ||
    //   this.generalForm.value.ApiKey != null ||
    //   this.generalForm.value.clientId != null ||
    //   this.generalForm.value.clientSecretKey != null ||
    //   this.generalForm.value.RefreshToken != null
    // ) {
    //   this.platformConfigured = "Android";
    // } else if (this.generalForm.value.domainName != null) {
    //   this.platformConfigured = "Web";
    // } else {
    //   this.platformConfigured = "";
    // }
    if (
      this.generalForm.value.bundleIdentitfier &&
      this.generalForm.value.packageName &&
      this.generalForm.value.domainName
    ) {
      this.platformConfiguredIOS = "iOS";
      this.platformConfiguredAndroid = "Android";
      this.platformConfiguredWeb = "Web";
    }
    try {
      this.getBase64().then(data => (this.base64binaryString = data));
      let data = {
        apps: [
          {
            appName: this.generalForm.value.appName,
            platforms: [
              {
                platform: this.platformConfiguredIOS,
                bundleIdOrPackageName: this.generalForm.value.bundleIdentitfier,
                appStoreId: this.generalForm.value.appStoreId,
                iosAPNSCertificateKey: this.base64binaryString,
                iosAPNSCertificateKeyPassword: this.generalForm.value.password,
                iosSandboxAPNSCertificateKey: "",
                iosSandboxAPNSCertificateKeyPassword: ""
              },
              {
                platform: this.platformConfiguredAndroid,
                bundleIdOrPackageName: this.generalForm.value.packageName,
                androidFCMAPIKey: this.generalForm.value.ApiKey,
                playstoreClientId: this.generalForm.value.clientId,
                playstoreClientSecret: this.generalForm.value.clientSecretKey,
                playstoreRefreshToken: this.generalForm.value.RefreshToken
              },
              {
                platform: this.platformConfiguredWeb,
                domainName: this.generalForm.value.domainName
              }
            ]
          }
        ],
        userId: this.userId,
        accessToken: this.accessToken
      };
      this.authService.submitOnboardApps(data).subscribe(
        data => {
          console.log(data);
          const status = data["status"];
          switch (status) {
            case "INVALID_INPUT":
              alert(status);
              break;
            case "SUCCESS":
              this.router.navigate(["home"]);
              break;
            case "ERROR":
              alert("Something Went wrong");
              break;
            case "INVALID_CERTIFICATE":
              alert(status);
              break;
            case "INVALID_PLAYSTORE_CREDENTIALS":
              alert(status);
              break;
            default:
              break;
          }
        },
        error => {
          console.log(error);
        }
      );
    } catch (e) {
      console.log(e);
    }
  }
}
