import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { SettingsComponent } from "./settings.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { AppsComponent } from "./apps/apps.component";
import { MyAccountComponent } from "./my-account/my-account.component";
import { ProfileComponent } from "./my-account/profile/profile.component";
import { ChangePasswordComponent } from "./my-account/change-password/change-password.component";
import { CheckPasswordDirective } from "./my-account/change-password/check-password.directive";
import { RolesAndPermissionComponent } from "./roles-and-permission/roles-and-permission.component";
import { UsersComponent } from "./users/users.component";
import { BillingAndPaymentComponent } from "./billing-and-payment/billing-and-payment.component";
import { AlertAndNotificationsComponent } from "./alert-and-notifications/alert-and-notifications.component";

const routes: Routes = [
  {
    path: "",
    component: SettingsComponent,
    children: [
      {
        path: "",
        redirectTo: "my-account",
        pathMatch: "full",
        component: MyAccountComponent
      },
      { path: "my-account", component: MyAccountComponent },
      { path: "apps", component: AppsComponent },
      // { path: "apps/edit", component: AppEditComponent },
      { path: "roles", component: RolesAndPermissionComponent },
      { path: "users", component: UsersComponent },
      { path: "billingAndPayment", component: BillingAndPaymentComponent },
      {
        path: "alertAndNotifications",
        component: AlertAndNotificationsComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    NgbModule,
    ReactiveFormsModule
  ],

  declarations: [
    SettingsComponent,
    AppsComponent,
    MyAccountComponent,
    ProfileComponent,
    ChangePasswordComponent,
    RolesAndPermissionComponent,
    UsersComponent,
    BillingAndPaymentComponent,
    AlertAndNotificationsComponent
  ],
  exports: [RouterModule]
})
export class SettingsModule {}
