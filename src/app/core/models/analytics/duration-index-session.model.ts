export interface DurationIndexSessionModel {
  "unit": string,
  "averageOverTime": string,
  "percentageChange": any,
  "averageSessionLength": [
    {
      "date": string,
      "length": number
    }
  ]
}
