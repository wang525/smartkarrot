import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserGroupCountComponent } from './user-group-count.component';

describe('UserGroupCountComponent', () => {
  let component: UserGroupCountComponent;
  let fixture: ComponentFixture<UserGroupCountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserGroupCountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserGroupCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
