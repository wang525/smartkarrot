import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {
  constructor(private http: HttpClient) {}

  getApiUrl(subPath) {
    return environment.baseApiUrlSurvey + 'surveyAndFeedback/' + subPath;
  }

  getSurveyList() {
    return this.http.post(this.getApiUrl('list/'), {
      appId: '4ed062f1-fc38-4f32-a714-b63dc5399626'
    });
  }

  getSurveyDetails(surveyId) {
    let params = {
      appId: '4ed062f1-fc38-4f32-a714-b63dc5399626',
      surveyId: surveyId
    };
    return this.http.post(this.getApiUrl('get/'), params);
  }

  createSurvey(
    isDraft,
    isWebApp,
    surveyName,
    questions,
    screenConfig,
    type = 'survey'
  ) {
    let params = this.getSurveyParams(
      isDraft,
      isWebApp,
      surveyName,
      questions,
      screenConfig,
      type
    );
    return this.http.post(this.getApiUrl('create/'), params);
  }

  editSurvey(
    isDraft,
    isWebApp,
    surveyName,
    questions,
    screenConfig,
    surveyId,
    type = 'survey'
  ) {
    let params = this.getSurveyParams(
      isDraft,
      isWebApp,
      surveyName,
      questions,
      screenConfig,
      type
    );
    params['surveyId'] = surveyId;
    return this.http.post(this.getApiUrl('edit/'), params);
  }

  duplicateSurvey(surveyId) {
    let params = {
      appId: '4ed062f1-fc38-4f32-a714-b63dc5399626',
      surveyId: surveyId
    };
    return this.http.post(this.getApiUrl('duplicate/'), params);
  }

  deleteSurvey(surveyId) {
    let params = {
      appId: '4ed062f1-fc38-4f32-a714-b63dc5399626',
      surveyId: surveyId
    };
    return this.http.post(this.getApiUrl('delete/'), params);
  }

  getSurveyParams(
    isDraft,
    isWebApp,
    surveyName,
    questions,
    screenConfig,
    type
  ) {
    return {
      appId: '4ed062f1-fc38-4f32-a714-b63dc5399626',
      draft: isDraft,
      survey: {
        isForWebAppOnly: isWebApp || false,
        surveyType: type,
        surveyName: surveyName || '',
        questions: questions,
        startDate: new Date(),
        endDate: new Date(),
        screenConfig: screenConfig
      }
    };
  }
}
