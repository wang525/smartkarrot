import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { Observable,Subject } from "rxjs";
import { EngagementScoreModel } from '../../models/analytics/engagement-score.model';
import { EngagementScoreTrendsModel } from '../../models/analytics/engagement-score-trends.model';
import { EngagementScoreGroupsModel } from '../../models/analytics/engagement-score-groups.model';
import { RealTimeEngagementModel, RealTimeEngagementDetailsModel } from '../../models/analytics/real-time-engagement.model';
import { LoyaltyIndexEngagementModel } from '../../models/analytics/loyalty-index-engagement.model';
import { RetentionIndexModel } from '../../models/analytics/retention-index.model';
import { DurationIndexSessionModel } from '../../models/analytics/duration-index-session.model';
import { DurationIndexFrequencyModel } from '../../models/analytics/duration-index-frequency.model';
import { FeedbackIndexModel,FeedbackIndexTrendingModel } from '../../models/analytics/feedback-index.model';
import { AppDataModel } from '../../models/analytics/app-data.model';
import { IndicesDurationDataModel, IndicesLoyaltyDataModel, IndicesRetentionDataModel,
  IndicesFeedbackDataModel, IndicesScoreDataModel } from '../../models/analytics/indices-trend-data.model';
import { CommonService } from "../common/common.service";
import { environment } from "../../../../environments/environment";
import { StorageService } from '../../../auth/services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  private apps: AppDataModel[];
  public selectedApp: AppDataModel;
  public selectedPlatforms: string[];
  public activeApp: any;
  private subject = new Subject<any>();
  public realTimeChartData: RealTimeEngagementModel;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
    ) {
    this.getAppInfo();
  }

  getAppInfo(): void {
    this.http.post(this.getApiUrl('organization-apps'), {
        "organizationId": this.storageService.getCurrentUser().user.organizationId
      }).subscribe(res => {
        this.apps = res['apps'];
        this.selectedApp = this.apps[0];
      });
  }

  getAppList(): any {
    if (!this.apps) return false;
    return this.apps;
  }

  setSelectedApp(appId): void {
    this.selectedApp = <any>this.apps.filter(app => {
      return app.appId == appId;
    });
  }

  getSelectedPlatforms(): any {
    return this.selectedPlatforms;
  }

  setSelectedPlatforms(platforms): void {
    this.selectedPlatforms = platforms;
  }



  // ToDo - Remove code maybe
  getAppId(): string {
    return this.activeApp.appId;
  }

  getSavedActiveApp() {
    if (localStorage.getItem('activeApp') !== null) {
      let app = JSON.parse(localStorage.getItem('activeApp'));
      this.subject.next({ app: app });
      // console.log('called getSavedActiveApp');
      return app;
    }
  }

  updateActiveApp(app){
    if(app==null){
      app = this.storageService.getCurrentUser().apps[0];
    }
    localStorage.setItem('activeApp', JSON.stringify(app));

    this.subject.next({ app: app });
  }

  getActiveAppInfo (): Observable<any> {
   
    // console.log('called getActiveAppInfo');
    // return this.getSavedActiveApp();
    return this.subject.asObservable();
    
    // this.updateActiveApp(this.activeApp);
    // return this.activeApp;
  }

  getActiveAppInfoOnLoad () {
   
    return this.getSavedActiveApp();
  }

  updateAppFilter(filterApp) {
    // let self = this;
    // this.appData.forEach(function (val, i) {
    //   if (val.platform == filterApp)
    //     self.selectedAppData = val;
    // });
  }

  removeAppFilter(app){
    this.activeApp = null;
    // this.selectedAppData = null;
  }

  /* API */
  getBaseApiUrl() {
    return environment.baseApiUrlAnalytics;
  }

  getApiUrl(subPath) {
    return environment.baseApiUrlAnalytics + subPath;
  }

  getApiKey() {
    return environment.apiKey;
  }

  getApiValue() {
    return environment.apiValue;
  }

  getFormattedDate(date): string {
    let today = new Date(date);
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!

    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = <any>'0' + dd;
    } 
    if (mm < 10) {
      mm = <any>'0' + mm;
    } 
    return yyyy + '-' + mm + '-' + dd;
  }

  getAppEngagementScore(data) {
    return this.http
        .post<EngagementScoreModel>(this.getApiUrl("engagement-score"), data);
  }

  getAppEngagementScoreTrends(data) {
    return this.http
        .post<EngagementScoreTrendsModel>(this.getApiUrl("engagement-score/trends"), data);
  }

  getAppEngagementScoreGroups(data) {
    return this.http
        .post<EngagementScoreGroupsModel>(this.getApiUrl("engagement-score/groups"), data);
  }

  // getEnggScorePercentile(data) {
  //   return this.http
  //       .post<EngagementScoreGroupsModel>(this.getApiUrl("engagement-score/percentile"), data);
  // }

  getAppRealTimeEngagement(data) {
    return this.http
        .post<RealTimeEngagementModel>(this.getApiUrl("realtime-engagement"), data);
  }

  getAppRealTimeEngagementDetails(data) {
    return this.http
        .post<RealTimeEngagementDetailsModel>(this.getApiUrl("realtime-engagement/details"), data);
  }

  getAppLoyaltyIndex(data) {
    return this.http
        .post<LoyaltyIndexEngagementModel>(this.getApiUrl("loyalty-index"), data);
  }

  getAppRetentionIndex(data) {
    return this.http
        .post<RetentionIndexModel>(this.getApiUrl("retention-index"), data);
  }

  getAppDurationIndexSession(data) {
    return this.http
        .post<DurationIndexSessionModel>(this.getApiUrl("session/length"), data);
  }

  getAppDurationIndexFrequency(data) {
    return this.http
        .post<DurationIndexFrequencyModel>(this.getApiUrl("session/frequency"), data);
  }

  getAppFeedbackIndexFrequency(data) {
    return this.http
        .post<FeedbackIndexModel>(this.getApiUrl("feedback-index"), data);
  }
  
  getAppFeedbackIndexTrending(data) {
    return this.http
        .post<FeedbackIndexTrendingModel>(this.getApiUrl("feedback-index/trends"), data);
  }

  getIndicesScore(data) {
    return this.http
          .post<IndicesScoreDataModel>(this.getApiUrl("indices-trend/changes"), data);
  }

  getAppIndicesTrend(data, modelType: string) {
    let url;
    switch (modelType) {
      case "duration":
        url = this.http
          .post<IndicesDurationDataModel>(this.getApiUrl("indices-trend/session-length"), data);
        break;
      case "loyalty":
        url = this.http
          .post<IndicesLoyaltyDataModel>(this.getApiUrl("indices-trend/loyalty-index"), data);
        break;
      case "retention":
        url = this.http
          .post<IndicesRetentionDataModel>(this.getApiUrl("indices-trend/retention-index"), data);
        break;
      default:
        url = this.http
          .post<IndicesFeedbackDataModel>(this.getApiUrl("indices-trend/feedback-index"), data);
        break;
    }
    return url;
  }

  getAllApps(data) {
    return this.http
        .post<FeedbackIndexTrendingModel>(this.getApiUrl("feedback-index/trends"), data);
  }
}
