import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EngScoreRelativeComponent } from './eng-score-relative.component';

describe('EngScoreRelativeComponent', () => {
  let component: EngScoreRelativeComponent;
  let fixture: ComponentFixture<EngScoreRelativeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EngScoreRelativeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EngScoreRelativeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
