import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Tab, FilterData } from '../../../core/service/segment/data';

@Component({
  selector: 'segment-filter-tabs',
  templateUrl: './filter-tabs.component.html',
  styleUrls: ['./filter-tabs.component.scss']
})
export class FilterTabsComponent implements OnInit {
  @Output() tabChanged = new EventEmitter();
  selected_tab = FilterData.TAB_USER_ATTR;   /* Current Selected Tab id */
  
  public tabs = [
    new Tab(FilterData.TAB_USER_ATTR, "User Attribute"),
    new Tab(FilterData.TAB_DEVICE_ATTR, "Device Attribute"),
    new Tab(FilterData.TAB_APP_EVENT_ATTR, "App Events")
  ];

  constructor() { }

  ngOnInit() {
  }

  onChnagedTab(tab_id) {
    this.selected_tab = tab_id;
    this.tabChanged.emit(this.selected_tab);
  }
}
