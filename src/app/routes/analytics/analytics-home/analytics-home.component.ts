import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AnalyticsHeaderComponent } from '../analytics-header/analytics-header.component';
import { AnalyticsService } from '../../../core/service/analytics/analytics.service';
import { CommonService } from '../../../core/service/common/common.service';
import { StorageService } from '../../../auth/services/storage.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics-home.component.html',
  styleUrls: ['./analytics-home.component.scss']
})
export class AnalyticsComponent implements OnInit {

  headName = [{
    "name" : "Dashboard",
    "url" : "analytics/engagement",
    "active": true
  }];

  constructor(
    public analyticsService: AnalyticsService,
    public commonService: CommonService,
    private storageService: StorageService,
    private router: Router) {}

  ngOnInit() {
    /* set header name to head bar */
    this.commonService.setHeaderName(this.headName);

    // this.analyticsService.getAppInfo({
    //   "organizationId": this.storageService.getCurrentUser().user.organizationId
    // });

    if (!document.querySelector('#gMaps-script')) { 
      var script = document.createElement('script');
      script.type = 'text/javascript';
      script.id = 'gMaps-script';
      script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyASH-7nEgy_6Jz7qU2elN8IG097swIIquk';
      document.body.appendChild(script);
    }
  }

  // ngOnDestroy() {
  //   // ...
  // }
}
