import { Component, OnInit } from '@angular/core';
import { CommonService } from '../../../../core/service/common/common.service';
import { SurveyService } from '../../../../core/service/survey/survey.service';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-survey-list',
  templateUrl: './survey-list.component.html',
  styleUrls: ['./survey-list.component.scss']
})
export class SurveyListComponent implements OnInit {

  headName: Array<string> = ["Survey / Feedback"];
  surveyList: Array<object> = [];
  constructor(
    private commonService: CommonService,
    private surveyService: SurveyService,
    private router: Router
  ) { }

  ngOnInit() {
    this.commonService.setHeaderName(this.headName);

    this.getSurveyList();
  }

  getSurveyList() {
    this.surveyService.getSurveyList().subscribe(response => {
      this.surveyList = response['surveys'];
    });
  }

  customTrackBy(index: number, obj: any): any {
    return index;
  }

  editSurvey(survey) {
    if (survey.surveyType == 'survey')
      this.router.navigate(['/features/survey-multiple', survey.surveyId]);
    else if (survey.surveyType == 'feedback')
      this.router.navigate(['/features/survey-single', survey.surveyId]);
  }

  duplicateSurvey(survey) {
    this.surveyService.duplicateSurvey(survey.surveyId).subscribe(resp => {
      this.getSurveyList();
    })
  }

  deleteSurvey(survey) {
    this.surveyService.deleteSurvey(survey.surveyId).subscribe(resp => {
      this.getSurveyList();
    })
  }

}
