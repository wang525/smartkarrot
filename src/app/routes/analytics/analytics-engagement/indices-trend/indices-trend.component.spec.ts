import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicesTrendComponent } from './indices-trend.component';

describe('IndicesTrendComponent', () => {
  let component: IndicesTrendComponent;
  let fixture: ComponentFixture<IndicesTrendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicesTrendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicesTrendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
