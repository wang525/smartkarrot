import { Component, OnChanges, ElementRef, ViewChild, Input } from '@angular/core';
import { EngagementScoreTrendsModel } from '../../../../../core/models/analytics/engagement-score-trends.model';
import * as d3 from 'd3';

@Component({
  selector: 'app-score-trend-chart',
  templateUrl: './score-trend-chart.component.html',
  styleUrls: ['./score-trend-chart.component.scss']
})
export class ScoreTrendChartComponent implements OnChanges {

  @ViewChild('chart')
  private chartContainer: ElementRef;

  @Input()
  data: EngagementScoreTrendsModel;

  margin = {top: 20, right: 20, bottom: 30, left: 40};

  constructor() { }

  ngOnChanges(): void {
    if (!this.data) { return; }

    this.createChart();
  }

  private createChart(): void {
    const element = this.chartContainer.nativeElement;
    let data = this.data.scores;
    data.forEach(function (d) {
      d.daysBeforeToday = d.daysBeforeToday == 0 ? "Current" : d.daysBeforeToday + " days";
      d.score = d.score;
    });

    d3.select(element).select('svg').remove();

    const svg = d3.select(element).append('svg')
        .attr('width', element.offsetWidth)
        .attr('height', element.offsetHeight);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    const x = d3
      .scaleBand()
      .rangeRound([0, contentWidth])
      .padding(0.5)
      .domain(data.map(d => {
        // let result = d.daysBeforeToday == 0 ? "Current" : d.daysBeforeToday + " days";
        return d.daysBeforeToday;
      }));

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([0, (d3.max(data, d => d.score) * 1.3)]);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + contentHeight + ')')
      .call(d3.axisBottom(x));

    const valueline = d3.line()
      .x((d) => {
            let result = d['daysBeforeToday'];
            // return (result == "Current" ? x(result) + 20 : x(result) - 10);
            return x(result) + 15;
          })
      .y((d) => { return y(d['score']) - 20 })
      .curve(d3.curveMonotoneX);

    g.selectAll('.bar')
      .data(data)
      .enter().append('rect')
        .attr('class', 'bar')
        .attr('style', 'fill: #9b9b9b')
        .attr('x', d => {
              let result = d.daysBeforeToday;
              return x(result);
            })
        .attr('y', d => y(<any>d.score))
        .attr('width', x.bandwidth())
        .attr('height', d => contentHeight - y(d.score));

    g.append('path')
      .attr("class", "path")
      .attr("fill", "none")
      .attr("stroke", "#5306E0")
      .attr("stroke-width", 1.5)
      .attr("d", valueline(<any>data));

    g.selectAll(".dot")
      .data(data)
      .enter().append("circle") // Uses the enter().append() method
      .attr("class", "dot") // Assign a class for styling
      .attr('cx', d => {
            let result = d.daysBeforeToday;
            return x(result) + 15;
          })
      .attr('cy', d => y(<any>d.score) - 20)
      .attr("r", 3)
      .attr("fill", "#5306E0")
      .on("mouseover", function(a, b, c) {
        console.log(a);
        // this.attr('class', 'focus');
      })
      .on("mouseout", function() {  });

    g.append('g').selectAll("text")
      .data(data)
      .enter()
      .append("text")
      .attr("x", function(d) {
        let result = d['daysBeforeToday'];
        // return (result == "Current" ? x(result) + 10 : x(result) + 10);
        return x(result) + 7;
      })
      .attr("y", function(d) { return y(d.score) - 27 })
      .attr("fill", "#5306E0")
      .style("font-size", "12px")
      .text(function(d) { return d.score.toFixed(1) });
  }
}
