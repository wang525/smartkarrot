import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { stepData } from "../../referral-details/referral-data";

@Component({
  selector: "app-add-count",
  templateUrl: "./add-count.component.html",
  styleUrls: ["./add-count.component.scss"]
})
export class AddCountComponent implements OnInit {
  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;

  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();
  @Output() draftRefData = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  /* change the step status */
  changeBackSetp() {
    this.changeStepStatus.emit(stepData.STEP5_WITHDRAW);
  }

  changeNextStep() {
    this.changeStepStatus.emit(stepData.STEP7_WITHDRAWAL);
  }

  saveData() {
    this.saveRefData.emit();
  }
  saveAsDraft() {
    this.draftRefData.emit();
  }
}
