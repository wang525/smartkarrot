import { Component, OnInit, OnDestroy } from "@angular/core";
import { AnalyticsService } from "../../../core/service/analytics/analytics.service";
import { StorageService } from "../../../auth/services/storage.service";
import { Subscription } from "rxjs";

@Component({
  selector: "app-analytics-header",
  templateUrl: "./analytics-header.component.html",
  styleUrls: ["./analytics-header.component.scss"]
})
export class AnalyticsHeaderComponent implements OnInit, OnDestroy {
  hideFilterOptions = true;
  appFilterWidget = {
    ios: false,
    android: false
  };
  activeApp: any;
  selectedActiveApp: any;
  subscription: Subscription;
  allApps: [{}];
  appId: any;
  activeAppId: any;

  constructor(
    private analyticsService: AnalyticsService,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    if (!this.analyticsService.getActiveAppInfoOnLoad())
      this.analyticsService.updateActiveApp(null);

    this.allApps = this.storageService.getCurrentUser().apps;
    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();
    this.appId = this.activeApp.appId;

    // to make selected
    this.activeAppId = this.activeApp.appId;

    this.subscription = this.analyticsService
      .getActiveAppInfo()
      .subscribe(activeApp => {
        this.activeApp = activeApp.app;
        this.appId = this.activeApp.appId;
        // console.log('header updated',activeApp);
      });
  }

  applyAppFilter() {
    this.activeApp = this.selectedActiveApp;
    this.analyticsService.activeApp = this.activeApp;

    this.updateActiveApp(this.activeApp);
    this.hideFilterOptions = true;
  }
  updateAppFilter(app) {
    this.selectedActiveApp = app;
    // if (type == 'ios') {
    //   this.appFilterWidget.ios = !this.appFilterWidget.ios;
    //   this.analyticsService.updateAppFilter('iOS');
    // } else {
    //   this.appFilterWidget.android = !this.appFilterWidget.android;
    //   this.analyticsService.updateAppFilter('Android');
    // }
  }

  updateActiveApp(app) {
    // this.analyticsService.activeApp = app;
    this.activeApp = app;
    this.analyticsService.updateActiveApp(app);
  }

  removeAppFilter(app) {
    this.analyticsService.activeApp = null;
    this.activeApp = null;
  }

  toggleFilterWindow($event) {
    // addAppsContainer
    $event.preventDefault();
    this.hideFilterOptions = !this.hideFilterOptions;
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
