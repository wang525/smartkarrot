import { Component, OnInit } from "@angular/core";
import { CommonService } from "../../../core/service/common/common.service";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  hideBar: boolean;
  headName = [{ name: "Get Started", url: "", active: true }];
  constructor(public commonService: CommonService) {}

  ngOnInit() {
    /* set header name to the header bar */
    this.commonService.setHeaderName(this.headName);
  }

  open() {
    window.open("https://docs.smartkarrot.com/#referral-endpoints");
  }
}
