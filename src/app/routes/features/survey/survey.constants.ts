export interface SurveyModel {
  id: number,
  inputType: string,
  question: string,
  imageUrl: string,
  icon: string,
  value: string,
  options: Array<string>
}
export class SurveyConstants {

  public static surveyInputs: Array<SurveyModel> = [
    {
      id: 1, inputType: 'radio', question: '', imageUrl: 'https://source.unsplash.com/random', icon: 'radioRating', value: 'Radio button',
      options: [
        'Excellent',
        'Great',
        'Average',
        'Unsatisfactory',
        'Poor'
      ]
    },
    {
      id: 2, inputType: 'checkBox', question: '', imageUrl: 'https://source.unsplash.com/random', icon: 'checkRating', value: 'Check box',
      options: [
        'Excellent',
        'Great',
        'Average',
        'Unsatisfactory',
        'Poor'
      ]
    },
    {
      id: 3, inputType: 'dropdown', question: '', imageUrl: 'https://source.unsplash.com/random', icon: 'dropdownRating', value: 'Dropdown',
      options: [
        'Excellent',
        'Great',
        'Average',
        'Unsatisfactory',
        'Poor'
      ]
    },
    {
      id: 4, inputType: 'nps', question: '', imageUrl: 'https://source.unsplash.com/random',
      icon: 'NPSRating', value: 'NPS',
      options: [
        'Likely',
        'Not Likely'
      ]
    },
    {
      id: 5, inputType: 'thumbsRating', question: '', imageUrl: 'https://source.unsplash.com/random', icon: 'thumbsRating',
      value: 'Thumbs Rating',
      options: [
        'Very Likely',
        'Unlikely'
      ]
    },
    {
      id: 6, inputType: 'starRating', question: '', imageUrl: 'https://source.unsplash.com/random', icon: 'starRating',
      value: 'Star Rating',
      options: [
        '1 Star',
        '2 Star',
        '3 Star',
        '4 Star',
        '5 Star'
      ]
    },
    {
      id: 7, inputType: 'smileyRating', question: '', imageUrl: 'https://source.unsplash.com/random', icon: 'smileyRating',
      value: 'Smiley Rating',
      options: [
        'Could be better',
        'Neutral',
        'Satisfied',
        'Very Satisfied',
      ]
    },
    {
      id: 8, inputType: 'textbox', question: '', imageUrl: 'https://source.unsplash.com/random', icon: 'textRating',
      value: 'Text Box',
      options: []
    },
    {
      id: 9, inputType: 'imageArea', question: '', imageUrl: 'https://source.unsplash.com/random', icon: 'imageRating',
      value: 'Image area',
      options: []
    },
  ];

  public static MultipleInputSideBar = [
    { id: 1, key: 'questions', label: 'Questions', hasSub: false, sub: [] },
    {
      id: 2, key: 'screen-config', label: 'Screen Configuration', hasSub: true, sub: [
        { key: 'config-welcome', label: 'Welcome', isOptional: true },
        { key: 'config-question', label: 'Question', isOptional: false },
        { key: 'config-thankyou', label: 'Thank You', isOptional: false }
      ]
    },
    { id: 3, key: 'action-block', label: 'Action Block', hasSub: false, sub: [] }
  ]

  public static SingleInputSideBar = [
    {
      id: 1, key: 'screen-config', label: 'Screen Configuration', hasSub: true, sub: [
        { key: 'config-question', label: 'Question', isOptional: false },
        { key: 'config-thankyou', label: 'Thank You', isOptional: false }
      ]
    },
    { id: 2, key: 'action-block', label: 'Action Block', hasSub: false, sub: [] }
  ];

  public static FontList = [
    { key: 'NUNITOSANS', value: 'Nunito Sans Regular', 'font-family': 'NunitoSansRegular', 'font-weight': 'normal', 'font-style': 'normal' },
    { key: 'NUNITOSANSBOLD', value: 'Nunito Sans Bold', 'font-family': 'NunitoSansBold', 'font-weight': 'bold', 'font-style': 'normal' },
    { key: 'NUNITOSANSSEMIBOLD', value: 'Nunito Sans Semi Bold', 'font-family': 'NunitoSansSemiBold', 'font-weight': 'normal', 'font-style': 'normal' },
    { key: 'NUNITOSANSITALICS', value: 'Nunito Sans Italics', 'font-family': 'NunitoSansItalic', 'font-weight': 'normal', 'font-style': 'italic' },
    { key: 'NUNITOSANSLIGHT', value: 'Nunito Sans Light', 'font-family': 'NunitoSansLight', 'font-weight': 'normal', 'font-style': 'normal' }
  ];

  //default values

  public static defaultQuestion = {
    'inputType': 'radio',
    'question': '',
    'imageUrl': 'https://source.unsplash.com/random',
    'options': [
      'Excellent',
      'Great',
      'Average',
      'Unsatisfactory',
      'Poor'
    ],
    'followUp': []
  }
  public static welcomeConfig = {
    'configType': 'welcomeScreen',
    'color': '#6b8e23',
    'fontStyle': 'NUNITOSANS',
    'title': 'Take a quick survey to share your experience',
    'body': '',
    'imageUrl': ''
  }
  public static questionConfig = {
    'configType': 'questionScreen',
    'color': '#6b8e23',
    'fontStyle': 'NUNITOSANS',
    'title': 'Customer Survey',
    'body': '',
    'imageUrl': 'https://source.unsplash.com/random'
  }
  public static thankyouConfig = {
    'configType': 'thankYouScreen',
    'color': '#6b8e23',
    'fontStyle': 'NUNITOSANS',
    'title': 'Thank you for your valuable time',
    'body': '',
    'imageUrl': ''
  }
}