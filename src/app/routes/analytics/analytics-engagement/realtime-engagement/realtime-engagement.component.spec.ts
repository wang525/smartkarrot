import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealtimeEngagementComponent } from './realtime-engagement.component';

describe('RealtimeEngagementComponent', () => {
  let component: RealtimeEngagementComponent;
  let fixture: ComponentFixture<RealtimeEngagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealtimeEngagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealtimeEngagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
