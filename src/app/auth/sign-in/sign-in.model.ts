export interface SignInModel {
  emailId: string;
  password: string;
  deviceId: string;
}
