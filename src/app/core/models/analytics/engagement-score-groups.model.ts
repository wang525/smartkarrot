export interface EngagementScoreGroupsModel {
  "status": number,
  "engagementGroups": [
      {
          "category": string,
          "score": number
      }
  ],
  "allAppsEnagementScorePercentile": number,
  "categoryAppsEngagementScorePercentile": number
}
