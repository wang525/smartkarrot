import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { NotificationsComponent } from "./notifications.component";
import { FormsModule } from "@angular/forms";

const routes: Routes = [{ path: "", component: NotificationsComponent }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes), FormsModule],

  declarations: [NotificationsComponent],
  exports: [RouterModule]
})
export class NotificationsModule {}
