import { Component, OnInit, AfterViewInit, ViewChild } from "@angular/core";
import { StorageService } from "../../../auth/services/storage.service";
import { AuthService } from "../../../auth/services/auth.service";
import { GettingStarted } from "../../../auth/getting-started/getting-started-model";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { Router } from "../../../../../node_modules/@angular/router";

declare var $: any;
@Component({
  selector: "app-apps",
  templateUrl: "./apps.component.html",
  styleUrls: ["./apps.component.scss"]
})
export class AppsComponent implements OnInit {
  generalForm: FormGroup;
  accessToken: string;
  userId: string;
  appDetails: [{}];
  editAppDetails: {};
  verifiedIcon: String = "assets/images/verified (1).png";
  verifyIcon: String = "assets/images/warningTran.png";
  androidIcon: String = "assets/images/android.png";
  appleIcon: String = "assets/images/apple.png";
  rating: number;
  starItems: number = 0;
  base64binaryString: any;
  platformConfiguredIOS: string;
  platformConfiguredAndroid: string;
  platformConfiguredWeb: string;
  appId: string;
  SmartEnagagementScore: string =
    "Configuring below options will help us to get loyalty and feedback metrics which are used to calculate your app engagement score.";
  AppStoreID: string =
    "Helps us to get your app's user feedback and app ratings from store.";
  PushNotification: string =
    "Helps us to track app uninstalls using silent push notifications.  ";
  FeedBack: string =
    "Helps us to get your app's user feedback and app ratings from store. ";
  constructor(
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router
  ) {
    let currentUser = this.storageService.getCurrentUser();
    let user = currentUser.user;
    this.userId = user.userId;
    this.accessToken = currentUser.accessToken;
    // this.organizationId = user.organizationId;
  }

  gettingStarted: GettingStarted = {
    platform: "",
    appName: "",
    appRating: 0,
    bundleIdentitfier: "",
    appStoreId: "",
    isVerified: false,
    appUserCount: 0,
    apikey: "",
    clientId: "",
    clientSecret: "",
    refreshToken: "",
    filePassword: "",
    domainName: ""
  };

  @ViewChild("f") form: any;
  searchResults = [];
  artistId = 0;
  showAndroidField: boolean;
  selectedArtist: string;
  ngAfterViewInit() {
    $(function() {
      $("#scroll").slimScroll({
        height: "auto",
        color: "#00f",
        size: "2px",
        alwaysVisible: true
      });
    });
  }

  ngOnInit() {
    this.buildForm();
    let data = {
      userId: this.userId,
      accessToken: this.accessToken
    };
    this.authService.fetchProfile(data).subscribe(
      res => {
        // console.log("res" + JSON.stringify(res));
        if (res) {
          this.appDetails = res["apps"];
          this.starItems = this.appDetails["rating"] / 10;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  buildForm() {
    this.generalForm = new FormGroup({
      appName: new FormControl(null),
      bundleIdentitfier: new FormControl(null, Validators.required),
      ApiKey: new FormControl(null),
      clientId: new FormControl(null),
      clientSecretKey: new FormControl(null),
      RefreshToken: new FormControl(null),
      appStoreId: new FormControl(null),
      domainName: new FormControl(null, Validators.required),
      file: new FormControl(null),
      password: new FormControl(null),
      packageName: new FormControl(null, Validators.required),
      iosSandboxAPNSCertificateKey: new FormControl(null),
      iosSandboxAPNSCertificateKeyPassword: new FormControl(null)
    });
  }

  verify() {
    let data = {
      userId: this.userId,
      deviceId: this.storageService.generateUUID()
    };
    this.authService.sendVerifyOtp(data).subscribe(response => {
      if (response) {
        if (response["status"] == "SUCCESS") {
          $("#VerifyContact").modal("show");
        } else {
          console.log("no status");
        }
      } else {
        console.log("no response");
      }
    });
  }

  verifyMobile(otp) {
    console.log("otp" + otp);
    let data = {
      userId: this.userId,
      deviceId: this.storageService.generateUUID(),
      otp: otp
    };
    this.authService.verifyphone(data).subscribe(res => {
      if (res) {
        if (res["status"] == "SUCCESS") {
        } else {
          console.log("No status");
        }
      } else {
        console.log("No response");
      }
    });
  }

  appsOnboard() {
    $("#appsOnboard").modal("show");
  }

  search() {
    console.log("HELLO");
    let searchTerm = (<HTMLInputElement>document.getElementById("appName"))
      .value;
    console.log(searchTerm);
    let iOSSelected = document
      .getElementById("ios-tab")
      .getAttribute("aria-selected");
    let androidSelected = document
      .getElementById("android-tab")
      .getAttribute("aria-selected");
    if (iOSSelected) {
      this.authService.search(searchTerm).subscribe(response => {
        console.log(response);
        this.searchResults = response["results"];
        console.log(this.searchResults);
        //  $('#appNameInput').autocomplete as any({ source: this.searchResults });
      });
    }
  }

  onInputChanged(name) {
    console.log(name);
    this.searchResults.forEach(element => {
      console.log(element.trackCensoredName, name);
      if (element.trackCensoredName === name) {
        this.gettingStarted.bundleIdentitfier = element.bundleId;
        this.gettingStarted.appStoreId = element.trackId;
      } else {
      }
    });
  }

  getBase64() {
    let file = $("#file")[0].files[0];
    console.log("file" + file);
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });
  }

  reset() {
    this.generalForm.reset();
  }

  onSubmit() {
    console.log("this.form.value" + JSON.stringify(this.generalForm.value));
    if (
      this.generalForm.value.bundleIdentitfier &&
      this.generalForm.value.packageName &&
      this.generalForm.value.domainName
    ) {
      this.platformConfiguredIOS = "iOS";
      this.platformConfiguredAndroid = "Android";
      this.platformConfiguredWeb = "Web";
    }
    // } else if (
    //   this.generalForm.value.bundleIdentitfier == null &&
    //   this.generalForm.value.packageName &&
    //   this.generalForm.value.domainName
    // ) {
    //   this.platformConfiguredIOS = "";
    //   this.platformConfiguredAndroid = "Android";
    //   this.platformConfiguredWeb = "Web";
    // } else if (
    //   this.generalForm.value.bundleIdentitfier == null &&
    //   this.generalForm.value.packageName == null &&
    //   this.generalForm.value.domainName
    // ) {
    //   this.platformConfiguredIOS = "";
    //   this.platformConfiguredAndroid = "";
    //   this.platformConfiguredWeb = "Web";
    // } else if (
    //   this.generalForm.value.bundleIdentitfier == null &&
    //   this.generalForm.value.packageName == null &&
    //   this.generalForm.value.domainName == null
    // ) {
    //   this.platformConfiguredIOS = "";
    //   this.platformConfiguredAndroid = "";
    //   this.platformConfiguredWeb = "";
    // } else if (
    //   this.generalForm.value.bundleIdentitfier == null &&
    //   this.generalForm.value.packageName &&
    //   this.generalForm.value.domainName == null
    // ) {
    //   this.platformConfiguredIOS = "";
    //   this.platformConfiguredAndroid = "Android";
    //   this.platformConfiguredWeb = "";
    // } else if (
    //   this.generalForm.value.bundleIdentitfier &&
    //   this.generalForm.value.packageName == null &&
    //   this.generalForm.value.domainName == null
    // ) {
    //   this.platformConfiguredIOS = "iOS";
    //   this.platformConfiguredAndroid = "";
    //   this.platformConfiguredWeb = "";
    // } else if (
    //   this.generalForm.value.bundleIdentitfier &&
    //   this.generalForm.value.packageName &&
    //   this.generalForm.value.domainName == null
    // ) {
    //   this.platformConfiguredIOS = "iOS";
    //   this.platformConfiguredAndroid = "Android";
    //   this.platformConfiguredWeb = "";
    // } else if (
    //   this.generalForm.value.bundleIdentitfier &&
    //   this.generalForm.value.packageName == null &&
    //   this.generalForm.value.domainName
    // ) {
    //   this.platformConfiguredIOS = "iOS";
    //   this.platformConfiguredAndroid = "";
    //   this.platformConfiguredWeb = "Web";
    // }

    try {
      this.getBase64().then(data => (this.base64binaryString = data));
      let data = {
        apps: [
          {
            appName: this.generalForm.value.appName,
            platforms: [
              {
                platform: this.platformConfiguredIOS,
                bundleIdOrPackageName: this.generalForm.value.bundleIdentitfier,
                appStoreId: this.generalForm.value.appStoreId,
                iosAPNSCertificateKey: this.base64binaryString,
                iosAPNSCertificateKeyPassword: this.generalForm.value.password,
                iosSandboxAPNSCertificateKey: this.generalForm.value
                  .iosSandboxAPNSCertificateKey,
                iosSandboxAPNSCertificateKeyPassword: this.generalForm.value
                  .iosSandboxAPNSCertificateKeyPassword
              },
              {
                platform: this.platformConfiguredAndroid,
                bundleIdOrPackageName: this.generalForm.value.packageName,
                androidFCMAPIKey: this.generalForm.value.ApiKey,
                playstoreClientId: this.generalForm.value.clientId,
                playstoreClientSecret: this.generalForm.value.clientSecretKey,
                playstoreRefreshToken: this.generalForm.value.RefreshToken
              },
              {
                platform: this.platformConfiguredWeb,
                domainName: this.generalForm.value.domainName
              }
            ]
          }
        ],
        userId: this.userId,
        accessToken: this.accessToken
      };

      console.log(data);
      this.authService.submitOnboardApps(data).subscribe(
        data => {
          console.log(data);
          const status = data["status"];
          switch (status) {
            case "INVALID_INPUT":
              alert(status);
              break;
            case "SUCCESS":
              alert("App is added successfully");
              let data = {
                userId: this.userId,
                accessToken: this.accessToken
              };
              this.authService.fetchProfile(data).subscribe(
                res => {
                  if (res) {
                    this.appDetails = res["apps"];
                    this.starItems = this.appDetails["rating"] / 10;
                  }
                },
                error => {
                  console.log(error);
                }
              );
              $("#appsOnboard").modal("hide");
              this.reset();
              break;
            default:
              break;
          }
        },
        error => {
          console.log(error);
        }
      );
    } catch (e) {
      console.log(e);
    }
  }

  editApp(appId) {
    this.appId = appId;
    try {
      let appInfo = {
        accessToken: this.accessToken,
        userId: this.userId,
        appId: appId
      };

      this.authService.fectchAppDetails(appInfo).subscribe(response => {
        console.log(JSON.stringify(response));
        if (response) {
          const status = response["status"];
          switch (status) {
            case "SUCCESS":
              this.editAppDetails = response["apps"];
              $("#editApps").modal("show");
          }
        }
      });
    } catch (e) {
      console.log(e);
    }
  }

  saveEditedApp() {
    console.log("coming here" + this.appId);
    console.log("this.form.value" + JSON.stringify(this.generalForm.value));
    if (
      (this.generalForm.value.bundleIdentitfier &&
        this.generalForm.value.packageName) ||
      this.generalForm.value.domainName
    ) {
      this.platformConfiguredIOS = "iOS";
      this.platformConfiguredAndroid = "Android";
      this.platformConfiguredWeb = "Web";
    }

    console.log(
      this.platformConfiguredIOS +
        this.platformConfiguredAndroid +
        this.platformConfiguredWeb
    );
    try {
      this.getBase64().then(data => (this.base64binaryString = data));
      console.log(this.base64binaryString);
      let data = {
        app: {
          appId: this.appId,
          appName: this.generalForm.value.appName,
          platforms: [
            {
              platform: this.platformConfiguredIOS,
              bundleIdOrPackageName: this.generalForm.value.bundleIdentitfier,
              appStoreId: this.generalForm.value.appStoreId,
              iosAPNSCertificateKey: this.base64binaryString,
              iosAPNSCertificateKeyPassword: this.generalForm.value.password,
              iosSandboxAPNSCertificateKey: this.generalForm.value
                .iosSandboxAPNSCertificateKey,
              iosSandboxAPNSCertificateKeyPassword: this.generalForm.value
                .iosSandboxAPNSCertificateKeyPassword
            },
            {
              platform: this.platformConfiguredAndroid,
              bundleIdOrPackageName: this.generalForm.value.packageName,
              androidFCMAPIKey: this.generalForm.value.ApiKey,
              playstoreClientId: this.generalForm.value.clientId,
              playstoreClientSecret: this.generalForm.value.clientSecretKey,
              playstoreRefreshToken: this.generalForm.value.RefreshToken
            },
            {
              platform: this.platformConfiguredWeb,
              domainName: this.generalForm.value.domainName
            }
          ]
        },
        userId: this.userId,
        accessToken: this.accessToken
      };

      console.log(data);

      this.authService.saveEditApp(data).subscribe(
        data => {
          console.log(data);
          const status = data["status"];
          switch (status) {
            case "INVALID_INPUT":
              alert(status);
              break;
            case "SUCCESS":
              alert("App saved successfully");
              let data = {
                userId: this.userId,
                accessToken: this.accessToken
              };
              this.authService.fetchProfile(data).subscribe(
                res => {
                  if (res) {
                    this.appDetails = res["apps"];
                  }
                },
                error => {
                  console.log(error);
                }
              );
              $("#editApps").modal("hide");
              this.reset();
              break;
            default:
              break;
          }
        },
        error => {
          console.log(error);
        }
      );
    } catch (e) {
      console.log(e);
    }
  }

  copyToClipboard(element) {
    console.log("cominfgjfjg" + element);
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element)).select();
    document.execCommand("copy");
    $temp.remove();
  }
}
