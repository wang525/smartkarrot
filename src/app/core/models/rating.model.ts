export interface RatingModel {
  inputType: string,
  question: string,
  imageUrl: string,
  options: Array<string>
}