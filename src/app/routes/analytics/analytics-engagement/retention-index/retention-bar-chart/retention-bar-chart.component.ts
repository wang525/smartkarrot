import { Component, OnChanges, ElementRef, ViewChild, Input } from '@angular/core';
import { RetentionIndexModel } from '../../../../../core/models/analytics/retention-index.model';
import * as d3 from 'd3';

@Component({
  selector: 'app-retention-bar-chart',
  templateUrl: './retention-bar-chart.component.html',
  styleUrls: ['./retention-bar-chart.component.scss']
})
export class RetentionBarChartComponent implements OnChanges {

  @ViewChild('chart')
  private chartContainer: ElementRef;

  @Input()
  data: RetentionIndexModel;
  @Input()
  unit: string;

  @Input()
  chartStyle: string = 'normal';

  margin = {top: 20, right: 20, bottom: 30, left: 40};
  weeks = [{avg: 0},{avg: 0},{avg: 0},{avg: 0},{avg: 0},{avg: 0},{avg: 0}];
  barWidth = 150;
  barHeight = 42;

  constructor() { }

  ngOnChanges(): void {
    if (!this.data) { return; }

    // this.updateWeekData();
    if (this.chartStyle == 'lineCurve') {
      this.createLineChart();
    } else {
      this.createChart();
    }
  }

  // private updateWeekData(): void {
  //   let i = 0;
  //   for (let j=0;j<=this.data.cohortAnalysisModels[0].retentions.length;j++) {
  //     for (let i=0;i<=this.data.cohortAnalysisModels.length;i++) {
  //       if (this.data.cohortAnalysisModels[i].retentions[j])
  //         this.weeks[i].avg = this.weeks[i].avg + this.data.cohortAnalysisModels[i].retentions[j];
  //     }
  //     this.weeks[i].avg = this.weeks[i].avg / 7;
  //   }
  // }

  private aver(a, b): number {
    return a + b;
  }
  public getColumnAverage(data:any,index:number): number {
    let total = 0;
    let total_i = 0;
    let average =0;
    data.forEach( function (o:any, ia:number) {
      if(o.retentions[index]){
        total += o.retentions[index];
        total_i++;
      }

    });
    average = (total / total_i);
    if(isNaN(average)){
      average = 0;
    }
    // average = (average * 100) / 100;
    // average = Math.round(average);
    average = parseFloat(average.toFixed(2));
    
    return average;
  }

  private formatMonthName(str): string {
    return str.replace(/(\d+\s[\w]{3})[\w]+\s-\s([\d]+\s[\w]{3})[\w]+/, '$1 - $2');
  }

  private scaleColor = d3.scaleSequential(d3.interpolate("#5306e0", "#A882EF"))
      .domain([0, 100]);

  private createChart(): void {
    const element = this.chartContainer.nativeElement;
    let data = this.data.cohortAnalysisModels;

    

    d3.select(element).select('svg').remove();

    const svg = d3.select(element).append('svg')
        .attr('width', this.barWidth*data.length+this.barWidth)
        .attr('height', this.barHeight*data.length+50);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;
    let self = this;

    // let parent_g = svg.append('g');
    // parent_g.append('text')
    //   .html('<tspan x="100" style="text-align: center;" dy="1.2em">All Users</tspan>')
    //   .attr('fill', '#ccc')
    //   .attr('font-size', 12)
    //   .attr('transform', function(d) {
    //     return "translate(0, 5)";
    //   });

    // let barHeight = element.offsetHeight / 8;
    let barHeight = this.barHeight;
    // let barWidth = (element.offsetWidth - 100) / 7;
    let barWidth = this.barWidth;

    

    
    // let sequentialScales:Array<any> = [];
     
var all_indices:Array<number> =[];
    data.forEach( function (oa, iaa) {
      let reta = oa.retentions;
      let ret2a = reta.slice(1);
      all_indices = all_indices.concat(ret2a);
    });
    var new_range_all = d3.extent(all_indices);
    let sequentialScale = d3.scaleSequential(d3.interpolate("#cedef9", "#1b39a9"))
    .domain(new_range_all);

    data.forEach( function (o, ia) {

      let ret = o.retentions;
      let ret2 = ret.slice(1);
      

      let average = self.getColumnAverage(data,ia);


    //   var new_range = d3.extent(ret2);
    //   sequentialScales[ia] = d3.scaleSequential(d3.interpolate("#cedef9", "#1b39a9"))
    // .domain(new_range);

      let g = svg.append('g');

      g.append('text')
        .html('<tspan x="100" style="fill: #aaa; text-transform:capitalize;" dy="1.2em">'+self.unit+' '+ia+'</tspan>')
        .attr('fill', '#5306e0')
        .attr('font-size', 12)
        .attr('transform', function(d) {
          return "translate("+((ia*barWidth)+(barWidth/2)-15)+", -5)";
        });

        

        g.append('text')
        .html(function(d,i){
          if(ia==0){
            return '<tspan x="-60" y="30" dy="1.2em" fill="#aaa" style="font-weight:bold;">All Users</tspan><tspan x="110" y="25" dy="1.2em">'+average+'%</tspan>';
          }else{
            return '<tspan x="110" y="25" dy="1.2em">'+average+'%</tspan>'
          }
          
        })
        .attr('fill', '#5306e0')
        .attr('font-size', 12)
        .attr('transform', function(d) {
          return "translate("+((ia*barWidth)+(barWidth/2)-15)+", -5)";
        });

      let monthname = o.dateRange;
      let monthname_new = monthname.replace(/(\d+\s[\w]{3})[\w]+\s-\s([\d]+\s[\w]{3})[\w]+/, '$1 - $2');

      // Month Names
      g.append('text')
        .html('<tspan y="53" dy="1.2em"  style="font-weight:bold;">'+monthname_new+'</tspan>')
        .attr('fill','#000')
        .attr('font-size',12)
        // .attr('text-anchor','middle')
        .attr('transform', function(d) {
          return "translate(0,"+(ia*barHeight)+")";
        });

      // User Count
      g.append('text')
        .html('<tspan y="60" dy="1.2em"  style="color: #aaa;">'+o.totalUsers+' users</tspan>')
        .attr('fill','#000')
        .attr('font-size',12)
        // .attr('text-anchor','middle')
        .attr('transform', function(d) {
          return "translate(0,"+(ia*barHeight+7)+")";
        });

      // All Grids
      let ga = g.selectAll('g')
        .data(o.retentions)
        .enter()
        .append('g')
        .attr('transform',function(){
          return "translate(100,50)";
        });

    
      // ga.append('rect')
      // .attr('height', function(d) {  return barHeight-1; })
      // .attr('y', function(d,i) {  return (barHeight*i); })
      // .attr('x', function(d,i) {  return 0; })
      // .attr('width', barWidth - 1)
      // .attr('fill', function(d,i) { 
      //   if(i==0){
      //       return sequentialScale(parseInt(<any>d))
      //   }else{
      //     return sequentialScale(parseInt(<any>d))
      //   }
      //   })
      // .attr('transform', function(d, i) {
      //   return "translate(" + ia * barWidth + ",0)";
      // });

      ga.append('rect')
        .attr('height', function(d) {  return barHeight-1; })
        .attr('x', function(d,i) {  return (barWidth*i); })
        .attr('y', function(d,i) {  return 0; })
        .attr('width', barWidth - 1)
        .attr('fill', function(d,i) { 
          if(i==0){
             return '#cccccc'
          }else{

            return sequentialScale(parseInt(<any>d))
          }
          })
        .attr('transform', function(d, i) {
          return "translate(0," + ia * barHeight + ")";
        });

      // ga.append('text')
      // .text(function(d){return d.toFixed(2)})
      // .attr('fill','#fff')
      // .attr('font-size',12)
      // .attr('y', function(d,i) {  return (barHeight*i)+25; })
      // .attr('x', function(d,i) {  return barWidth/2-15; })
      // .attr('transform', function(d, i) {
      //   return "translate(" + ((ia * barWidth)+0) + ",0)";
      // });

      ga.append('text')
        .text(function(d){return d.toFixed(2)+'%'})
        .attr('fill',function(d,i){
          if(i==0){
            return '#000';
          }else{
            // return '#fff';
            return '#000';
          }
        })
        .attr('font-size',12)
        .attr('x', function(d,i) {  
          if(i==0){
          return (barWidth*i)+60;
          }else{

            return (barWidth*i)+70;
          } })
        .attr('y', function(d,i) {  return barHeight/2+5; })
        .attr('transform', function(d, i) {
          return "translate(0," + ((ia * barHeight)+0) + ")";
        });

    });
  }

  private createLineChart(): void {
    const element = this.chartContainer.nativeElement;
    let data = this.data.cohortAnalysisModels;

    d3.select(element).select('svg').remove();

    const svg = d3.select(element).append('svg')
        .attr('width', element.offsetWidth)
        .attr('height', element.offsetHeight);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    const x = d3
      .scaleBand()
      .rangeRound([0, contentWidth])
      .padding(0.6)
      .domain(data.map(d => <any>d.dateRange));

    data.forEach(function (d, i) {
      let ret = d.retentions;
      let total_sum = ret.reduce(this.aver);

      let average = (ret.length / total_sum) * 100;
      d['avg'] = <any>average.toFixed(2);  
    });

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([0, d3.max(data, d => d['avg'])]);

    const line = d3.line()
        .x((d) => { return x(d['dateRange']); })
        .y((d) => { return y(d['avg']); })
        .curve(d3.curveMonotoneX);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + contentHeight + ')')
      .call(d3.axisBottom(x));

    g.append("path")
      .attr("class", "path")
      .attr("fill", "none")
      .attr("stroke", "#5306E0")
      .attr("stroke-width", 1.5)
      .attr("class", "line")
      .attr("d", line(<any>data));

    g.selectAll(".dot")
      .data(data)
      .enter().append("circle")
      .attr("class", "dot")
      .attr("cx", function(d) { return x(d['dateRange']) })
      .attr("cy", function(d) { return y(d['avg']) })
      .attr("fill", "#5306E0")
      .attr("r", 5)
        .on("mouseover", function(a, b, c) { 
          console.log(a);
          // this.attr('class', 'focus');
        })
        .on("mouseout", function() {  })
  }

  
}
