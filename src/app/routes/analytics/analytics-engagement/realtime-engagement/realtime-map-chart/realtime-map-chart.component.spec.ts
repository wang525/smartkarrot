import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RealtimeMapChartComponent } from './realtime-map-chart.component';

describe('RealtimeMapChartComponent', () => {
  let component: RealtimeMapChartComponent;
  let fixture: ComponentFixture<RealtimeMapChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RealtimeMapChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RealtimeMapChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
