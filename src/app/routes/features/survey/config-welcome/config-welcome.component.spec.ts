import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigWelcomeComponent } from './config-welcome.component';

describe('ConfigWelcomeComponent', () => {
  let component: ConfigWelcomeComponent;
  let fixture: ComponentFixture<ConfigWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigWelcomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
