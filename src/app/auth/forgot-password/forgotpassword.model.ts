export interface ForgotPasswordModel {
  emailId: string;
  deviceId: string;
}
