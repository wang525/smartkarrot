import { Injectable } from "@angular/core";
import { Router } from "../../../../node_modules/@angular/router";

@Injectable({
  providedIn: "root"
})
export class StorageService {
  constructor(private router: Router) {}

  setCurrentUser(userInfo) {
    console.log(JSON.stringify(userInfo));
    localStorage.setItem("currentUser", JSON.stringify(userInfo));
  }

  updateUserInfo(newAppsArray) {
    const currentUser = JSON.parse(localStorage.getItem("currentUser"));
    console.log(currentUser);
    currentUser["apps"] = newAppsArray.slice(0);
    localStorage.setItem("currentUser", JSON.stringify(currentUser));
    //  console.log(JSON.parse(sessionStorage.getItem('currentUser')));
  }

  getCurrentUser() {
    if (localStorage.getItem("currentUser") !== null) {
      return JSON.parse(localStorage.getItem("currentUser"));
    } else {
      // alert('Please log In');
      this.router.navigate(["/auth/sign-in"]);
    }
  }

  generateUUID() {
    if (localStorage.getItem("deviceId") !== null) {
      return localStorage.getItem("deviceId");
    } else {
      let deviceId = (([1e7] as any) + -1e3 + -4e3 + -8e3 + -1e11).replace(
        /[018]/g,
        c =>
          (
            c ^
            (crypto.getRandomValues(new Uint8Array(1))[0] & (15 >> (c / 4)))
          ).toString(16)
      );
      localStorage.setItem("deviceId", deviceId);
      return deviceId;
    }
  }

  clearAll() {
    localStorage.clear();
  }
}
