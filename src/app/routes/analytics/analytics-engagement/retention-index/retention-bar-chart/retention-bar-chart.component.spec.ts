import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetentionBarChartComponent } from './retention-bar-chart.component';

describe('RetentionBarChartComponent', () => {
  let component: RetentionBarChartComponent;
  let fixture: ComponentFixture<RetentionBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetentionBarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
