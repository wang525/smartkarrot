import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CommonService } from '../../../core/service/common/common.service';
import { SegmentService } from '../../../core/service/segment/segment.service';
import { StorageService } from '../../../auth/services/storage.service';

declare var $;

@Component({
  selector: 'app-user-group',
  templateUrl: './user-group.component.html',
  styleUrls: ['./user-group.component.scss']
})
export class UserGroupComponent implements OnInit {
  headName = [{ name: 'User List', url: '', active: true }];

  productAppList: any;
  userListInGroup: any;
  ruleList: Array<any> = [];
  excludedUserList: Array<any> = [];
  userGroupName: String;
  moreUserList: any;
  searchText = "";

  private path: any;
  private groupId: any;
  private appId: any;

  private singleGroupData: any;

  private selectedAppId: any;

  /* check if save is finished */
  private saveFlag = false;

  constructor(
    public segmentService: SegmentService,
    public storageService: StorageService,
    private router: Router,
    public commonService: CommonService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    /* set header name to the header bar */
    this.commonService.setHeaderName(this.headName);

    this.selectedAppId = '';
    this.userGroupName = 'ACCOUNT NAME_USERGROUP_01';

    /* get the product App List */
    let organizationId = {
      organizationId: this.storageService.getCurrentUser().user.organizationId
    };
    this.segmentService.getProductAppIdList(organizationId).then(res => {
      this.productAppList = res;
    });
    this.userListInGroup = {};
    this.userListInGroup['allChecked'] = false;

    /* get the segment id of the individual segment */
    this.path = this.route.params.subscribe(params => {
      this.groupId = params['groupId']; //
      this.appId = params['appId']; //
    });

    if (this.groupId && this.appId) {
      let data = {
        userGroupId: this.groupId,
        appId: this.appId
      };
      this.userGroupName = '';
      this.segmentService.fetchSingleUserGroup(data).then(res => {
        this.singleGroupData = res;
        this.userGroupName = this.singleGroupData['userGroupDetail']['userGroupName'];
        if (
          this.singleGroupData &&
          this.singleGroupData['userGroupDetail'] &&
          this.singleGroupData['userGroupDetail']['appId']
        ) {
          this.selectedAppId = this.singleGroupData['userGroupDetail']['appId'];
          this.showUserList(this.selectedAppId);
        }
      });
    }
  }

  showUserList(AppId) {
    this.ruleList = [];
    this.userListInGroup = {};
    let data = {
      "appId": AppId
    }

    /* get list user group from api  */
    this.segmentService.getUserListInGroup(data).then(res => {
      this.userListInGroup = res;
      this.userListInGroup['allChecked'] = false;
      if (
        this.userListInGroup &&
        this.userListInGroup.users &&
        this.userListInGroup.users.length > 0
      ) {
        for (var i = 0; i < this.userListInGroup.users.length; i++) {
          this.userListInGroup.users[i]['checked'] = false;
        }
      }

      $(function() {
        $('#user-table-content').DataTable({
          paging: false,
          info: false,
          scrollY: 'calc(100vh - 270px)',
          scrollCollapse: true,
          searching: false,
          order: [[1, 'desc']],
          columnDefs: [
            { orderable: false, targets: 0 },
            { orderable: false, targets: 7 }
          ],
          destroy: true,
          retrieve: true
        });
      });

      if ( this.userListInGroup && this.singleGroupData && this.singleGroupData['userGroupDetail']['userIds'] ) {
        this.ruleList = [];
        for ( var i = 0; i < this.singleGroupData['userGroupDetail']['userIds'].length; i++ ) {
          for (var j = 0; j < this.userListInGroup.users.length; j++) {
            if ( this.singleGroupData['userGroupDetail']['userIds'][i] == this.userListInGroup.users[j]['userId'] ) {
              this.userListInGroup.users[j]['checked'] = true;
              this.ruleList.push(this.userListInGroup.users[j]);
            }
          }
        }
      }
      console.log("ruleList ", this.ruleList.length);

      if ( this.userListInGroup && this.singleGroupData && this.singleGroupData['userGroupDetail']['excludedUserIds'] ) {
        this.excludedUserList = [];
        for ( var i = 0; i < this.singleGroupData['userGroupDetail']['excludedUserIds'].length; i++ ) {
          for (var j = 0; j < this.userListInGroup.users.length; j++) {
            if ( this.singleGroupData['userGroupDetail']['excludedUserIds'][i] == this.userListInGroup.users[j]['userId'] ) {
              this.excludedUserList.push(this.userListInGroup.users[j]['userId']);
            }
          }
        }
      }
      console.log("excludedUserList ", this.excludedUserList.length);
    });
  }

  searchInUsercount(searchText) {
    if (this.saveFlag == false) {
      this.saveFlag = true;
      let data: any;
      if (searchText != '') {
        data = {
          "appId" : this.userListInGroup["appId"],
          "searchText" : searchText
        };
      }
      else  {
        data = {
          "appId" : this.userListInGroup["appId"]
        };
      }

      this.segmentService.getUserListInGroup(data).then(res => {
        this.saveFlag = false;
        this.userListInGroup = res;
      });
    }
  }

  fetchMoreUsersInSegment($event) {
    if (this.saveFlag == false) {
      this.saveFlag = true;
      let data = {
        "appId" : this.userListInGroup["appId"],
        "nextToken" : this.userListInGroup['nextToken']
      };
      /* get user lists */
      this.segmentService.getUserListInGroup(data).then(res => {
        this.saveFlag = false;
        this.userListInGroup["nextToken"] = res["nextToken"];
        this.userListInGroup["totalUserCount"] = res["totalUserCount"];
        this.moreUserList = res["users"];
        this.userListInGroup.users = this.userListInGroup.users.concat(this.moreUserList);

        if ( this.userListInGroup && this.singleGroupData && this.singleGroupData['userGroupDetail']['userIds'] ) {
          this.ruleList = [];
          for ( var i = 0; i < this.singleGroupData['userGroupDetail']['userIds'].length; i++ ) {
            for (var j = 0; j < this.userListInGroup.users.length; j++) {
              if ( this.singleGroupData['userGroupDetail']['userIds'][i] == this.userListInGroup.users[j]['userId'] ) {
                this.userListInGroup.users[j]['checked'] = true;
                this.ruleList.push(this.userListInGroup.users[j]);
              }
            }
          }
        }
        console.log("Total ", this.userListInGroup.users.length);

        if ( this.userListInGroup && this.singleGroupData && this.singleGroupData['userGroupDetail']['excludedUserIds'] ) {
          this.excludedUserList = [];
          for ( var i = 0; i < this.singleGroupData['userGroupDetail']['excludedUserIds'].length; i++ ) {
            for (var j = 0; j < this.userListInGroup.users.length; j++) {
              if ( this.singleGroupData['userGroupDetail']['excludedUserIds'][i] == this.userListInGroup.users[j]['userId'] ) {
                this.excludedUserList.push(this.userListInGroup.users[j]['userId']);
              }
            }
          }
        }
        console.log("excludedUserList ", this.excludedUserList.length);
      });
    }
  }

  inputCheckedUser(idx) {
    let selectedChecked = this.userListInGroup.users[idx]['checked'];
    selectedChecked = selectedChecked == true ? false : true;
    this.userListInGroup.users[idx]['checked'] = selectedChecked;
    if (selectedChecked) {
      this.ruleList.push(this.userListInGroup.users[idx]);
    } else {
      let res = this.ruleList.indexOf(this.userListInGroup.users[idx]);
      if (res != -1) {
        this.ruleList.splice(res, 1);
      }
    }
  }

  inputCheckedRuleList(rule) {
    let idx = this.ruleList.indexOf(rule);
    if (idx != -1) {
      let idxTotal = this.userListInGroup.users.indexOf(this.ruleList[idx]);
      if (idxTotal != -1) {
        this.userListInGroup.users[idxTotal].checked = false;
      }
      this.ruleList.splice(idx, 1);
    }
  }

  inputCheckedAllUser() {
    if (this.userListInGroup) {
      let allChecked = this.userListInGroup['allChecked'];
      if (allChecked == true) {
        this.ruleList = [];
        this.userListInGroup['allChecked'] = false;
        if (
          this.userListInGroup &&
          this.userListInGroup.users &&
          this.userListInGroup.users.length > 0
        ) {
          for (var i = 0; i < this.userListInGroup.users.length; i++) {
            this.userListInGroup.users[i]['checked'] = false;
          }
        }
      } else {
        this.ruleList = [];
        this.userListInGroup['allChecked'] = true;
        if (
          this.userListInGroup &&
          this.userListInGroup.users &&
          this.userListInGroup.users.length > 0
        ) {
          for (var i = 0; i < this.userListInGroup.users.length; i++) {
            let rule = this.userListInGroup.users[i];
            this.userListInGroup.users[i]['checked'] = true;
            this.ruleList.push(rule);
          }
        }
      }
    }
  }

  saveUserGroupCheck() {
    if (this.appId && this.groupId) {
      $('#saveFilters').trigger('click');
    } else {
      this.saveUserGroup();
    }
  }

  saveUserGroup() {
    if (this.saveFlag == false) {
      this.saveFlag = true;
      let saveData = {};

      saveData['appId'] = this.userListInGroup['appId'];
      saveData['userGroupName'] = this.userGroupName;
      saveData['source'] = 'jan_users.xls';
      saveData['isDraft'] = false;
      saveData['excludedUserIds'] = [];

      if (this.appId && this.groupId) {
        saveData['userGroupId'] = this.groupId;
      } else {
        saveData[
          'orgUserId'
        ] = this.storageService.getCurrentUser().user.userId;
      }

      if (this.ruleList && this.ruleList.length > 0) {
        let userIds = [];
        for (var i = 0; i < this.ruleList.length; i++) {
          let userList = this.ruleList[i]['userId'];
          userIds.push(userList);
        }
        saveData['userIds'] = userIds;

        if (this.appId && this.groupId) {
          saveData['excludedUserIds'] = this.excludedUserList;
          console.log('save data content==', saveData);
          this.segmentService.updateUserRule(saveData).then(res => {
            this.saveFlag = false;
            this.router.navigate(['/segments/usergrouplist']);
          });
        } else {
          console.log('save data content==', saveData);
          this.segmentService.saveUserRule(saveData).then(res => {
            this.saveFlag = false;
            if (res['status'] == 'SUCCESS') {
              this.router.navigate(['/segments/usergrouplist']);
            }
          });
        }
      } else {
        $('#emptyUserList').trigger('click');
      }
    }
  }

  cancelSaveUserGroup() {
    if (this.ruleList && this.ruleList.length > 0) {
      $('#cancelUserGroupList').trigger('click');
    } else {
      this.router.navigate(['/segments/usergrouplist']);
    }
  }

  gotoSaveAsDraft() {
    if (this.saveFlag == false) {
      this.saveFlag = true;
      let saveData = {};

      saveData['appId'] = this.userListInGroup['appId'];
      saveData['userGroupName'] = this.userGroupName;
      saveData['source'] = 'jan_users.xls';
      saveData['isDraft'] = true;
      saveData['excludedUserIds'] = [];

      if (this.appId && this.groupId) {
        saveData['userGroupId'] = this.groupId;
      } else {
        saveData[
          'orgUserId'
        ] = this.storageService.getCurrentUser().user.userId;
      }

      if (this.ruleList && this.ruleList.length > 0) {
        let userIds = [];
        for (var i = 0; i < this.ruleList.length; i++) {
          let userList = this.ruleList[i]['userId'];
          userIds.push(userList);
        }
        saveData['userIds'] = userIds;

        if (this.appId && this.groupId) {
          saveData['excludedUserIds'] = this.excludedUserList;
          console.log('save data content==', saveData);
          this.segmentService.updateUserRule(saveData).then(res => {
            this.saveFlag = false;
            this.router.navigate(['/segments/usergrouplist']);
          });
        } else {
          console.log('save data content==', saveData);
          this.segmentService.saveUserRule(saveData).then(res => {
            this.saveFlag = false;
            if (res['status'] == 'SUCCESS') {
              this.router.navigate(['/segments/usergrouplist']);
            }
          });
        }
      } else {
        $('#emptyUserList').trigger('click');
      }
    }
  }
}
