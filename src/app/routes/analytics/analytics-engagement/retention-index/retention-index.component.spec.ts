import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetentionIndexComponent } from './retention-index.component';

describe('RetentionIndexComponent', () => {
  let component: RetentionIndexComponent;
  let fixture: ComponentFixture<RetentionIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetentionIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetentionIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
