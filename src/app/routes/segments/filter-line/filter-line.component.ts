import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SegmentService } from '../../../core/service/segment/segment.service';
import { FormControl, FormGroup } from '@angular/forms';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead';
import { mergeMap } from 'rxjs/operators';
import {
  OperatorData,
  FilterData,
  TypeData
} from '../../../core/service/segment/data';
import * as $ from 'jquery';

@Component({
  selector: 'segment-filter-line',
  templateUrl: './filter-line.component.html',
  styleUrls: ['./filter-line.component.scss']
})
export class FilterLineComponent implements OnInit {
  @Input() AppId: string;
  @Input() TabName: string;
  @Input() attrs: Object;
  @Input() filter: FilterData;

  @Output() filterRemoved = new EventEmitter();
  @Output() filterAdded = new EventEmitter();

  private html: any;
  private maxDate: Date;

  // TYPEAHEAD PROPS
  public stateCtrl: FormControl = new FormControl();

  public myForm: FormGroup = new FormGroup({
    state: this.stateCtrl
  });

  public states: Array<any> = [];

  private autocompleteValues: any;

  constructor(public segmentService: SegmentService) {}

  ngOnInit() {
    this.html = `<span style="width:500px; background:red">An event like "Purchased Product" could have additional attributes like
                    "Product Id", "Delivered to PIN Code", "Discount Applied". You can refine the
                    app event filter further by choosing additional attributes.</span>`;
    var date = new Date();
    this.maxDate = new Date(
      date.getFullYear(),
      date.getMonth(),
      date.getDate(),
      12,
      30
    );

    this.getAutoCompleteContentForUserDevice();
    this.getAutoCompleteContent();
  }

  /* City state Gender check */
  isNotAllowNumber(value) {
    if (value == 'City' || value == 'Gender' || value == 'State') {
      return true;
    } else {
      return false;
    }
  }

  /* only integer confirm */
  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  isUserAttr() {
    return this.filter.attr_kind === FilterData.TAB_USER_ATTR;
  }
  isDeviceAttr() {
    return this.filter.attr_kind === FilterData.TAB_DEVICE_ATTR;
  }
  isAppEvent() {
    return this.filter.attr_kind === FilterData.TAB_APP_EVENT_ATTR;
  }

  hasCondition(): boolean {
    //return this.filter.attr_cond != '';
    if (this.filter.attr_name) {
      return this.isAppEvent();
    }
    return false;
  }

  hasOperator(): boolean {
    return this.filter.attr_type != '';
  }

  hasAttribute(): boolean {
    let res = false;
    if (this.filter.event_attr_name || this.filter.has_attr) {
      res = true;
    }
    return res;
  }

  getTypeAheadValue() {
    if (this.autocompleteValues) {
      return this.autocompleteValues;
    }
    return [];
  }

  getOperator(): OperatorData {
    return this.segmentService.getOperator(
      this.filter.attr_type,
      this.filter.operator
    );
  }

  getType(): TypeData {
    return this.segmentService.getType(this.filter.attr_type);
  }

  isOperatorInputable(): boolean {
    let type = this.getType();
    if (type) {
      return !type.selectable;
    }
    return false;
  }

  isOperatorSelectable(): boolean {
    let type = this.getType();
    if (type) {
      return type.selectable;
    }
    return false;
  }

  getConditions(): Array<any> {
    return this.segmentService.getConditions();
  }

  getDurations(): Array<any> {
    let conditionForDurations = [];
    for (
      var i = 0;
      i < this.attrs['app_events']['conditionForDuration'].length;
      i++
    ) {
      let conditionForDuration = {};
      conditionForDuration['name'] = this.attrs['app_events'][
        'conditionForDuration'
      ][i];
      conditionForDurations.push(conditionForDuration);
    }
    return conditionForDurations;
  }

  getAttributes(): Array<any> {
    let eventAttributes = [];
    let eventName = this.attrs['app_events']['events'];
    for (var i = 0; i < eventName.length; i++) {
      if (this.filter.attr_cond == eventName[i]['name']) {
        if (eventName[i]['attributes']) {
          for (var j = 0; j < eventName[i]['attributes'].length; j++) {
            eventAttributes[j] = eventName[i]['attributes'][j];
          }
          return eventAttributes;
        }
      }
    }
    return [];
  }

  getOperators(): Array<any> {
    return this.segmentService.getOperators(this.filter.attr_type);
  }

  /* this is the temporary data */
  getAppEventOperator(): Array<any> {
    let res = [{ operator: 'At Least' }, { operator: 'At Most' }];
    return res;
  }

  getAttrOperators(): Array<any> {
    let attrOperator;
    // let eventName = this.attrs['app_events']['events'];
    // for (var i = 0; i < eventName.length; i++) {
    //   if (this.filter.attr_cond == eventName[i]['name']) {
    //     for (var j = 0; j < eventName[i]['attributes'].length; j++) {
    //       if (this.filter.event_attr_name == eventName[i]['attributes'][j]['attrName']) {
    //         attrOperator = eventName[i]['attributes'][j]['type'];
    //         return this.segmentService.getOperators(attrOperator);
    //       }
    //     }
    //   }
    // }
    this.filter.attr_type = 'Boolean';
    return this.segmentService.getOperators(this.filter.attr_type);
  }

  getOptions() {
    let type = this.getType();
    if (type) {
      return !type.options;
    }
    return [];
  }

  removeFilter() {
    this.filterRemoved.emit(this.filter);
  }

  addFilter($event) {
    $event.preventDefault();
    let res = true;

    this.filter.used_attr = true;
    this.filter.logic_status = FilterData.LOGIC_AND;
    if (this.filter.has_attr == false) {
      this.filter.event_attr_name = '';
      this.filter.event_attr_type = '';
      this.filter.event_attr_value = '';
    }

    /* user attribute confirm to fill the empty item */
    if (this.isUserAttr()) {
      if (this.filter.attr_name == '' || this.filter.operator == '') {
        res = false;
      }
      for (var i = 0; i < this.filter.params.length; i++) {
        if (this.filter.params[i].value == '') {
          res = false;
        }
      }
    }

    /* device attribute confirm to fill the empty item */
    if (this.isDeviceAttr()) {
      if (this.filter.attr_name == '' || this.filter.operator == '') {
        res = false;
      }
      for (var i = 0; i < this.filter.params.length; i++) {
        if (this.filter.params[i].value == '') {
          res = false;
        }
      }
    }

    /* app event confirm to fill the empty item */
    if (this.isAppEvent()) {
      if (
        this.filter.attr_name == '' ||
        this.filter.operator == '' ||
        this.filter.attr_cond == '' ||
        this.filter.cnt_times == '' ||
        this.filter.cnt_days == '' ||
        this.filter.attr_dura == ''
      ) {
        res = false;
      }
      if (
        this.filter.has_attr == true &&
        (this.filter.event_attr_name == '' ||
          this.filter.event_attr_type == '' ||
          this.filter.event_attr_value == '')
      ) {
        res = false;
      }
    }

    if (res == false) {
      $('#fillEmptyItem').trigger('click');
      this.filter.logic_status = FilterData.LOGIC_NONE;
      return;
    } else {
      this.filterAdded.emit(this.filter);
    }
    console.log('filter===', this.filter);
  }

  isSelectableStatus() {
    return this.filter.logic_status === FilterData.LOGIC_SELECTABLE;
  }

  isAndStatus() {
    return this.filter.logic_status === FilterData.LOGIC_AND;
  }

  isNotStatus() {
    return this.filter.logic_status === FilterData.LOGIC_NOT;
  }

  isNoneStatus() {
    return this.filter.logic_status === FilterData.LOGIC_NONE;
  }

  setAndStatus() {
    this.filter.logic_status = FilterData.LOGIC_AND;
  }

  setNotStatus() {
    this.filter.logic_status = FilterData.LOGIC_NOT;
  }

  getUserAttrs(): Array<any> {
    if (this.attrs && this.attrs['user_attrs']) {
      return Object.values(this.attrs['user_attrs']);
    }
    return [];
  }
  getDeviceAttrs(): Array<any> {
    if (this.attrs && this.attrs['device_attrs']) {
      return Object.values(this.attrs['device_attrs']);
    }
    return [];
  }

  getAppEventCond(): Array<any> {
    if (this.attrs && this.attrs['app_events']) {
      return Object.values(this.attrs['app_events']['events']);
    }
    return [];
  }

  getAppEvents(): Array<any> {
    if (this.attrs && this.attrs['app_events']) {
      let conditionForEvents = [];
      for (
        var i = 0;
        i < this.attrs['app_events']['conditionForEvent'].length;
        i++
      ) {
        let conditionForEvent = {};
        conditionForEvent['name'] = this.attrs['app_events'][
          'conditionForEvent'
        ][i];
        conditionForEvents.push(conditionForEvent);
      }
      return conditionForEvents;
    }
    return [];
  }

  getAttrCond(): Array<any> {
    if (this.isAppEvent()) {
      return this.getAppEventCond();
    }
    return [];
  }

  getAttrs(): Array<any> {
    if (this.isUserAttr()) {
      return this.getUserAttrs();
    } else if (this.isDeviceAttr()) {
      return this.getDeviceAttrs();
    } else if (this.isAppEvent()) {
      return this.getAppEvents();
    }

    return [];
  }

  addEventAttribute() {
    this.filter.has_attr = true;
    if (this.filter.attr_cond && this.getAttributes().length > 0) {
      this.filter.has_attr = true;
    } else {
      this.filter.has_attr = false;
    }
  }

  onChangedAttrDuration($event) {}

  onChangedAttrCond($event) {
    let attrs = this.getAttrCond();
    for (let i = 0; i < attrs.length; i++) {
      let attr = attrs[i];
      if (attr.name == $event.target.value) {
        if (attr.type) {
          this.filter.attr_type = attr.type;
          break;
        } else {
          this.filter.attr_type = '';
          break;
        }
      }
    }
    this.getAutoCompleteContent();
  }

  onChangedAttrName($event) {
    $event.preventDefault();
    // this.filter = new FilterData(FilterData.TAB_USER_ATTR, '', '', '', '', false, false, '18', '7', '', 'is', '42', '', [], FilterData.LOGIC_NONE);
    if (this.filter.params && this.filter.params.length > 0) {
      for (var k = 0; k < this.filter.params.length; k++) {
        this.filter.params[k].value = '';
      }
    }
    let attrs = this.getAttrs();
    for (let i = 0; i < attrs.length; i++) {
      let attr = attrs[i];
      if (attr.name == $event.target.value) {
        if (attr.type) {
          this.filter.attr_type = attr.type;
          break;
        } else {
          //this.filter.attr_cond = attr.name;
          break;
        }
      }
    }

    this.getAutoCompleteContentForUserDevice();
  }

  /* show the input box according to the selection of attribute type */
  isStringType() {
    let res = false;
    if (this.filter.attr_type == 'String') {
      res = true;
    }
    return res;
  }

  isNumberType() {
    let res = false;
    if (this.filter.attr_type == 'Number') {
      res = true;
    }
    return res;
  }

  isDateType() {
    let res = false;
    if (this.filter.attr_type == 'Date') {
      res = true;
    }
    return res;
  }

  isBooleanType() {
    let res = false;
    if (this.filter.attr_type == 'Boolean') {
      res = true;
    }
    return res;
  }

  onChangedOperator($event) {
    this.filter.operator = $event.target.value;
    let op = this.getOperator();

    if (op) {
      if (this.filter.attr_type == 'Number') {
        this.filter.params = [];
        for (let i = 0; i < op['countOfValues']; i++) {
          this.filter.params.push({ value: 0 });
        }
      } else if (this.filter.attr_type == 'String') {
        this.filter.params = [];
        for (let i = 0; i < op['countOfValues']; i++) {
          this.filter.params.push({ value: '' });
        }
      } else if (this.filter.attr_type == 'Date') {
        this.filter.params = [];
        for (let i = 0; i < op['countOfValues']; i++) {
          this.filter.params.push({ value: '' });
        }
      } else if (this.filter.attr_type == 'Boolean') {
        this.filter.params = [];
        for (let i = 0; i < op['countOfValues']; i++) {
          this.filter.params.push({ value: false });
        }
      }
    }
    this.getAutoCompleteContentForUserDevice();
  }

  getAutoCompleteContentForUserDevice() {
    if (this.TabName != 'Event' && this.filter.attr_name) {
      let data = {
        appId: this.AppId,
        attributeType: this.TabName,
        attributeName: this.filter.attr_name
      };
      this.segmentService.getAutoCompleteAttributeValues(data).then(res => {
        this.autocompleteValues = res['autocompleteValues'];
      });
    }
  }

  getAutoCompleteContent() {
    if (
      this.filter.has_attr &&
      this.filter.attr_cond &&
      this.filter.event_attr_name
    ) {
      let data = {
        appId: this.AppId,
        attributeType: this.TabName
      };
      data['eventName'] = this.filter.attr_cond;
      data['attributeName'] = this.filter.event_attr_name;
      this.segmentService.getAutoCompleteAttributeValues(data).then(res => {
        this.autocompleteValues = res['autocompleteValues'];
      });
    }
  }
}
