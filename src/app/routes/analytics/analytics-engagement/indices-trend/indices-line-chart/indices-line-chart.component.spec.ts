import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndicesLineChartComponent } from './indices-line-chart.component';

describe('IndicesLineChartComponent', () => {
  let component: IndicesLineChartComponent;
  let fixture: ComponentFixture<IndicesLineChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndicesLineChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndicesLineChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
