import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '../../../../core/service/analytics/analytics.service';
import { DurationIndexSessionModel } from '../../../../core/models/analytics/duration-index-session.model';
import { DurationIndexFrequencyModel } from '../../../../core/models/analytics/duration-index-frequency.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-duration-index',
  templateUrl: './duration-index.component.html',
  styleUrls: ['./duration-index.component.scss']
})
export class DurationIndexComponent implements OnInit {
  durationIndexSessionData: DurationIndexSessionModel;
  durationIndexFrequencyData: DurationIndexFrequencyModel;
  startDate1: string = this.analyticsService.getFormattedDate(
    new Date().setDate(new Date().getDate() - 15)
  );
  startDate2: string = this.analyticsService.getFormattedDate(
    new Date().setDate(new Date().getDate() - 15)
  );
  endDate1: string = this.analyticsService.getFormattedDate(new Date());
  endDate2: string = this.analyticsService.getFormattedDate(new Date());

  // ToDo - Update code once API works properly
  // appId: string = this.analyticsService.getAppId();
  appId: string = '4ed062f1-fc38-4f32-a714-b63dc5399626';
  filterDate1: string = 'Last 0.5 Month';
  filterDate2: string = 'Last 0.5 Month';
  activeApp : any;
  subscription: Subscription;

  constructor(private analyticsService: AnalyticsService) {}

  ngOnInit() {
    
    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();
    this.appId = this.activeApp.appId;

    this.updateGraph2();
    this.updateGraph1();

    this.subscription = this.analyticsService.getActiveAppInfo().subscribe(activeApp => { 
    
      this.activeApp = activeApp.app;
      this.appId = this.activeApp.appId;
      this.updateGraph2();
      this.updateGraph1();
      // console.log('retention graph updated',activeApp);
    });
  }

  getLastDate(days) {
    var date = new Date();
    var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));

    return this.analyticsService.getFormattedDate(last);
}


  changeParams(select_field,start_date,end_date,callback){
    this[end_date] = this.analyticsService.getFormattedDate(new Date());
    let d = new Date();

    let multiplier:number = 30;

    let date_string = this[select_field];

    

    let selectedDay:number = parseFloat(date_string.match(/[\d.]+/)[0]);
    let numberOfDays:number = 1;

    numberOfDays = selectedDay*multiplier;

    let last_date = this.getLastDate(numberOfDays);
    this[start_date] = last_date;
    this[callback]();
  }

  changeDateGraph1(){
    this.changeParams('filterDate1','startDate1','endDate1','updateGraph1');
  }
  changeDateGraph2(){
    this.changeParams('filterDate2','startDate2','endDate2','updateGraph2');
  }

  updateGraph1() {
    this.durationIndexSessionData = null;
    this.analyticsService
      .getAppDurationIndexSession({
        startDate: this.startDate1,
        endDate: this.endDate1,
        appId: this.appId,
        "platforms": [ "Android", "iOS" ]
      })
      .subscribe(data => {
        this.durationIndexSessionData = data;
      });
  }

  updateGraph2() {
    this.durationIndexFrequencyData = null;
    this.analyticsService
      .getAppDurationIndexFrequency({
        startDate: this.startDate2,
        endDate: this.endDate2,
        appId: this.appId,
        "platforms": [ "Android", "iOS" ]
      })
      .subscribe(data => {
        this.durationIndexFrequencyData = data;
      });
  }

  changeDate1() {
    this.endDate1 = this.analyticsService.getFormattedDate(new Date());
    let d = new Date();
    if (this.filterDate1 == '3m' || this.filterDate1 == '6m') {
      if (d.getMonth() < 3) {
        d.setFullYear(d.getFullYear() - 1);
        d.setMonth(d.getMonth() + 12 - <any>this.filterDate1.substring(0, 1));
      } else {
        d.setMonth(d.getMonth() - <any>this.filterDate1.substring(0, 1));
      }
    }
    if (this.filterDate1 == '6w') {
      d.setDate(d.getDate() - 42);
    }
    this.startDate1 = this.analyticsService.getFormattedDate(d);

    this.updateGraph1();
  }

  changeDate2() {
    this.endDate2 = this.analyticsService.getFormattedDate(new Date());
    let d = new Date();
    if (this.filterDate2 == '3m' || this.filterDate2 == '6m') {
      if (d.getMonth() < 3) {
        d.setFullYear(d.getFullYear() - 1);
        d.setMonth(d.getMonth() + 12 - <any>this.filterDate2.substring(0, 1));
      } else {
        d.setMonth(d.getMonth() - <any>this.filterDate2.substring(0, 1));
      }
    }
    if (this.filterDate2 == '6w') {
      d.setDate(d.getDate() - 42);
    }
    if (this.filterDate2 == '15d') {
      d.setDate(d.getDate() - 15);
    }
    this.startDate2 = this.analyticsService.getFormattedDate(d);

    this.updateGraph2();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}