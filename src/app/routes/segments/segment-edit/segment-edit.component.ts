import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SegmentService } from "../../../core/service/segment/segment.service";
import { CommonService } from "../../../core/service/common/common.service";
import { Observable } from "rxjs";
import { UserAttr, FilterData } from "../../../core/service/segment/data";
import * as $ from "jquery";
import { send } from "q";
import { StorageService } from "../../../auth/services/storage.service";

/* Constants */
@Component({
  selector: "app-segment-edit",
  templateUrl: "./segment-edit.component.html",
  styleUrls: ["./segment-edit.component.scss"]
})

/* Component Class */
export class SegmentEditComponent implements OnInit {
  headName = [{ name: "User Segments Create/Edit", url: "", active: true }];

  private possible_attrs = {}; /* possible attributes data from api service */
  private countUsers = {}; /* counter of users in segment from api service */
  private individualSegment = {}; /* individual segment data from api service */

  private filters = {
    productApp: "",
    users: [] /*Array of FilterData*/,
    devices: [] /*Array of FilterData*/,
    app_events: [] /*Array of FilterData*/
  };
  private cur_filter: FilterData = new FilterData(
    FilterData.TAB_USER_ATTR,
    "",
    "",
    "",
    "",
    false,
    false,
    "18",
    "7",
    "",
    "is",
    "42",
    "",
    [],
    FilterData.LOGIC_NONE
  );

  private method_show = 0;
  private LOGIC_AND = FilterData.LOGIC_AND;
  private LOGIC_NOT = FilterData.LOGIC_NOT;

  /* send data */
  private sendData: any = {}; /* send save segment data */
  private saveSegName = "ACCOUNT NAME_SEGMENT_01"; /* save segment name */

  /* get path and segment id */
  private path: any;
  private id: any;

  /* check if show first select box */
  private showCombine = true;

  /* check if save is finished */
  private saveFlag = false;

  /* get the product App Id list */
  private productAppList: any;

  /* get data for getting user count */
  private dataForUserCount: any;

  /* save temporary data */
  private tmpSaveData: any;

  constructor(
    public segmentService: SegmentService,
    public commonService: CommonService,
    private storageService: StorageService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    /* get the segment id of the individual segment */
    this.path = this.route.params.subscribe(params => {
      this.id = params["id"]; //
    });

    /* set header name to the header bar */
    this.commonService.setHeaderName(this.headName);

    /* get the product App List */
    let organizationId = {
      organizationId: this.storageService.getCurrentUser().user.organizationId
    };

    this.tmpSaveData = this.segmentService.getTmpUserListData();

    this.segmentService.getProductAppIdList(organizationId).then(res => {
      this.productAppList = res;
      this.individualSegment = this.tmpSaveData;

      /* get the individual segment data */
      if (this.id || JSON.stringify(this.tmpSaveData) != "{}") {
        if (!this.id) {
          this.individualSegment = this.tmpSaveData;
        } else {
          this.individualSegment = this.segmentService.getIndividualSegment(
            this.id
          );
        }
        // console.log("individual segment data", this.individualSegment);
        /* possible attr in segments */
        if (this.individualSegment && this.individualSegment["appId"]) {
          let appId = this.individualSegment["appId"];
          this.segmentService.getPossibleAttrs(appId).then(res => {
            this.possible_attrs = res;

            /* show the data of individual segment data to the screen */
            if (/*this.id && */ this.individualSegment != null) {
              this.addIndividualSegmentData();

              let sendCountData = this.sendFilterData();
              let sendData = {};
              sendData["segment"] = sendCountData["filters"];
              sendData["appId"] = appId;
              this.dataForUserCount = sendData;
              this.segmentService
                .getCountsUserSegments(this.dataForUserCount)
                .then(res => {
                  this.countUsers = res;
                  this.countUsers["segment"]["countOfExclud"] = 0;
                  let excludeUser;
                  let editState = this.segmentService.getEditSegmentState();
                  if (editState == true) {
                    if (
                      this.individualSegment["excludedUserIds"] &&
                      this.individualSegment["excludedUserIds"].length > 0
                    ) {
                      excludeUser = this.individualSegment["excludedUserIds"];
                    }
                  } else {
                    excludeUser = this.segmentService.getTmpUserSaveListData();
                  }

                  if (excludeUser && excludeUser.length > 0) {
                    let includeUserCount = parseInt(
                      this.countUsers["segment"]["countOfUsers"]
                    );
                    let excludeUserCount = excludeUser.length;
                    this.countUsers["segment"]["countOfUsers"] =
                      includeUserCount - excludeUserCount;
                    this.countUsers["segment"][
                      "countOfExclud"
                    ] = excludeUserCount;
                  }
                });
            } else if (this.id) {
              $("#emptySegment").trigger("click");
            }
          });
        } else if (this.id) {
          $("#emptySegment").trigger("click");
        }
      }
    });

    if (this.id) {
      this.saveSegName = "";
      this.headName[0]["name"] = "User Segments Edit";
    } else {
      if (
        JSON.stringify(this.tmpSaveData) != "{}" &&
        (this.tmpSaveData && this.tmpSaveData["segmentName"])
      ) {
        // console.log("save data", this.tmpSaveData);
        this.saveSegName = this.tmpSaveData["segmentName"];
      } else {
        this.saveSegName = "ACCOUNT NAME_SEGMENT_01";
      }
      this.headName[0]["name"] = "User Segments Create";
    }
  }

  /* show possible attributes */
  showPossibleAttr(selectedAppId) {
    this.segmentService.getPossibleAttrs(selectedAppId).then(res => {
      this.possible_attrs = res;

      /* get the individual segment data */
      this.individualSegment = this.segmentService.getIndividualSegment(
        this.id
      );

      /* show the data of individual segment data to the screen */
      if (this.id && this.individualSegment != null) {
        this.addIndividualSegmentData();
      } else if (this.id) {
        // if (this.id != "cancel") {
        $("#emptySegment").trigger("click");
        // }
      }
    });

    /* count of users in segments */
    this.dataForUserCount = { appId: selectedAppId };
    this.segmentService
      .getCountsUserSegments(this.dataForUserCount)
      .then(res => {
        this.countUsers = res;
        this.countUsers["segment"]["countOfExclud"] = 0;
        let includeUserCount = parseInt(
          this.countUsers["segment"]["countOfUsers"]
        );
        this.countUsers["segment"]["countOfUsers"] = includeUserCount;
        // let excludeUser = this.segmentService.getTmpUserSaveListData();
        // if (excludeUser && excludeUser.length > 0) {
        //   let includeUserCount = parseInt(this.countUsers['segment']['countOfUsers']);
        //   let excludeUserCount = excludeUser.length;
        //   this.countUsers['segment']['countOfUsers'] = includeUserCount - excludeUserCount;
        //   this.countUsers['segment']['countOfExclud'] = excludeUserCount;
        // }
      });
  }

  /* If there is no individual segment data go to the list segments */
  gotoListSegments() {
    this.router.navigate(["/segments/list"]);
  }

  /* get attr type */
  getAttributeType(attrKind, attrName) {
    let possibleVal = this.possible_attrs;

    if (attrKind == FilterData.TAB_USER_ATTR) {
      let userAttr = possibleVal["user_attrs"];
      for (var i = 0; i < userAttr.length; i++) {
        if (attrName == userAttr[i]["name"]) {
          return userAttr[i]["type"];
        }
      }
    }
    if (attrKind == FilterData.TAB_DEVICE_ATTR) {
      let deviceAttr = possibleVal["device_attrs"];
      for (var i = 0; i < deviceAttr.length; i++) {
        if (attrName == deviceAttr[i]["name"]) {
          return deviceAttr[i]["type"];
        }
      }
    }
    if (attrKind == FilterData.TAB_APP_EVENT_ATTR) {
      let appEvent = possibleVal["app_events"]["events"];
      for (var i = 0; i < appEvent.length; i++) {
        if (attrName == appEvent[i]["name"]) {
          return appEvent[i]["type"];
        }
      }
    }
    return;
  }

  /* delete selected product App */
  deleteSelecteProduct(selectedProductApp) {
    this.filters.productApp = "";
    this.filters = {
      productApp: "",
      users: [] /*Array of FilterData*/,
      devices: [] /*Array of FilterData*/,
      app_events: [] /*Array of FilterData*/
    };
    this.countUsers = {};
    if (this.id) {
      this.segmentService.setIndividualSegment(this.id);
    }
    this.cur_filter = new FilterData(
      FilterData.TAB_USER_ATTR,
      "",
      "",
      "",
      "",
      false,
      false,
      "18",
      "7",
      "",
      "is",
      "42",
      "",
      [],
      FilterData.LOGIC_NONE
    );
  }

  getRealDate(date) {
    var realDate = new Date(date);
    return (
      realDate.getFullYear() +
      "-" +
      (realDate.getMonth() + 1) +
      "-" +
      realDate.getDate()
    );
  }

  showDateValue(value) {
    var valDate = new Date(value);

    return (
      valDate.getFullYear() +
      "-" +
      (valDate.getMonth() + 1) +
      "-" +
      valDate.getDate()
    );
  }

  showAppName(appId) {
    let res = this.productAppList.find(elem => {
      return elem.appId == appId;
    });
    return res.name;
  }

  /* init the individual segment data */
  addIndividualSegmentData() {
    this.saveSegName = this.individualSegment["segment"].segmentName;

    if (this.individualSegment["segment"].filters) {
      let indivSegFilters = this.individualSegment["segment"].filters;
      this.filters.productApp = this.individualSegment["appId"];

      /*Compose user attributes elements */
      let userAttributes = indivSegFilters["userAttributes"];
      for (var i = 0; i < userAttributes.length; i++) {
        let filterUserAttribute = {};
        filterUserAttribute["attr_kind"] = FilterData.TAB_USER_ATTR;
        filterUserAttribute["attr_name"] = userAttributes[i]["attribute"];
        filterUserAttribute["attr_type"] = this.getAttributeType(
          FilterData.TAB_USER_ATTR,
          userAttributes[i]["attribute"]
        );
        filterUserAttribute["operator"] = userAttributes[i]["condition"];
        filterUserAttribute["attr_cond"] = "";
        filterUserAttribute["attr_dura"] = "";
        filterUserAttribute["cnt_days"] = "";
        filterUserAttribute["cnt_times"] = "";
        filterUserAttribute["event_attr_name"] = "";
        filterUserAttribute["event_attr_type"] = "";
        filterUserAttribute["event_attr_value"] = "";
        filterUserAttribute["has_attr"] = false;
        filterUserAttribute["used_attr"] = true;
        filterUserAttribute["logic_status"] = 2;
        let userAttributeParams = userAttributes[i]["values"];
        let filterUserAttributeParam = {};
        let filterUserAttributeParams = [];
        for (var j = 0; j < userAttributeParams.length; j++) {
          filterUserAttributeParam["value"] = userAttributeParams[j];
          filterUserAttributeParams.push(filterUserAttributeParam);
        }
        filterUserAttribute["params"] = filterUserAttributeParams;
        this.filters.users.push(filterUserAttribute);
      }

      let deviceAttributes = indivSegFilters["deviceAttributes"];
      for (var i = 0; i < deviceAttributes.length; i++) {
        let filterDeviceAttribute = {};
        filterDeviceAttribute["attr_kind"] = FilterData.TAB_DEVICE_ATTR;
        filterDeviceAttribute["attr_name"] = deviceAttributes[i]["attribute"];
        filterDeviceAttribute["attr_type"] = this.getAttributeType(
          FilterData.TAB_DEVICE_ATTR,
          deviceAttributes[i]["attribute"]
        );
        filterDeviceAttribute["operator"] = deviceAttributes[i]["condition"];
        filterDeviceAttribute["attr_cond"] = "";
        filterDeviceAttribute["attr_dura"] = "";
        filterDeviceAttribute["cnt_days"] = "";
        filterDeviceAttribute["cnt_times"] = "";
        filterDeviceAttribute["event_attr_name"] = "";
        filterDeviceAttribute["event_attr_type"] = "";
        filterDeviceAttribute["event_attr_value"] = "";
        filterDeviceAttribute["has_attr"] = false;
        filterDeviceAttribute["used_attr"] = true;
        filterDeviceAttribute["logic_status"] = 2;
        let deviceAttributeParams = deviceAttributes[i]["values"];
        let filterDeviceAttributeParam = {};
        let filterDeviceAttributeParams = [];
        for (var j = 0; j < deviceAttributeParams.length; j++) {
          filterDeviceAttributeParam["value"] = deviceAttributeParams[j];
          filterDeviceAttributeParams.push(filterDeviceAttributeParam);
        }
        filterDeviceAttribute["params"] = filterDeviceAttributeParams;
        this.filters.devices.push(filterDeviceAttribute);
      }

      /*Compose app events elements */
      let appEvents = indivSegFilters["appEvents"];
      for (var i = 0; i < appEvents.length; i++) {
        let filterAppEvent = {};
        let appEvent = {};
        let appEventAttributeObj = {};
        let appEventAttribute = appEvents[i]["attributes"];

        filterAppEvent["attr_kind"] = FilterData.TAB_APP_EVENT_ATTR;
        filterAppEvent["attr_name"] = appEvents[i]["conditionForEvent"];
        filterAppEvent["operator"] = appEvents[i]["conditionForCount"];
        filterAppEvent["attr_cond"] = appEvents[i]["event"];
        filterAppEvent["attr_dura"] = appEvents[i]["conditionForDuration"];
        filterAppEvent["attr_type"] = "";
        filterAppEvent["cnt_days"] = appEvents[i]["countOfDays"];
        filterAppEvent["cnt_times"] = appEvents[i]["countOfTimes"];
        filterAppEvent["event_attr_name"] = appEventAttribute[0]["attribute"];
        filterAppEvent["event_attr_type"] = appEventAttribute[0]["condition"];
        filterAppEvent["event_attr_value"] = appEventAttribute[0]["value"];
        filterAppEvent["has_attr"] = false;
        filterAppEvent["used_attr"] = true;
        filterAppEvent["logic_status"] = 2;
        this.filters.app_events.push(filterAppEvent);
      }
    }
    // console.log("filters is ", this.filters);
  }

  /* show the method of rule */
  gotoMethodNewAttribute($event) {
    $event.preventDefault();
    if (this.method_show == 1) {
      this.method_show = 0;
    } else {
      this.method_show = 1;
    }
  }

  /* Find a duplicated attr named attr_name from filter_attrs */
  findDuplicatedAttr(filter_attrs, attr_name) {
    return filter_attrs.find(attr => {
      return attr.attr_name == attr_name;
    });
  }

  /* Remove a attr from filter_attrs */
  removeDuplicatedAttr(filter_attrs, duplicated_attr) {
    if (duplicated_attr) {
      let duplicated_idx = filter_attrs.indexOf(duplicated_attr);
      filter_attrs.splice(duplicated_idx, 1);
    }
  }

  /* check show first select option */
  showCombineFunc() {
    let res = true;
    for (var i = 0; i < this.filters.users.length; i++) {
      if (
        this.filters.users[i]["logic_status"] == FilterData.LOGIC_SELECTABLE
      ) {
        res = false;
      }
    }
    for (var i = 0; i < this.filters.devices.length; i++) {
      if (
        this.filters.devices[i]["logic_status"] == FilterData.LOGIC_SELECTABLE
      ) {
        res = false;
      }
    }
    for (var i = 0; i < this.filters.app_events.length; i++) {
      if (
        this.filters.app_events[i]["logic_status"] ==
        FilterData.LOGIC_SELECTABLE
      ) {
        res = false;
      }
    }
    if (
      this.filters.users.length == 0 &&
      this.filters.devices.length == 0 &&
      this.filters.app_events.length == 0
    ) {
      res = true;
    }
    return res;
  }

  onAddedFilter(filter) {
    let sendCountData = {};
    let tmpTab = filter.attr_kind;
    this.segmentService.setTmpUserSaveListData(null);

    if (filter.attr_kind == FilterData.TAB_USER_ATTR) {
      /* Find Duplicated Attribute And Remove it */
      let duplicated_attr = this.findDuplicatedAttr(
        this.filters.users,
        filter.attr_name
      );
      this.removeDuplicatedAttr(this.filters.users, duplicated_attr);

      this.filters.users.push(filter);
    } else if (filter.attr_kind == FilterData.TAB_DEVICE_ATTR) {
      let duplicated_attr = this.findDuplicatedAttr(
        this.filters.devices,
        filter.attr_name
      );
      this.removeDuplicatedAttr(this.filters.devices, duplicated_attr);

      this.filters.devices.push(filter);
    } else if (filter.attr_kind == FilterData.TAB_APP_EVENT_ATTR) {
      let duplicated_attr = this.findDuplicatedAttr(
        this.filters.app_events,
        filter.attr_name
      );
      this.removeDuplicatedAttr(this.filters.app_events, duplicated_attr);

      this.filters.app_events.push(filter);
    }

    /* get user count from API */
    sendCountData = this.sendFilterData();
    let sendData = {};
    sendData["segment"] = sendCountData["filters"];
    sendData["appId"] = this.filters.productApp;
    sendData["segmentName"] = this.saveSegName;
    this.dataForUserCount = sendData;
    this.segmentService
      .getCountsUserSegments(this.dataForUserCount)
      .then(res => {
        this.countUsers = res;
        this.countUsers["segment"]["countOfExclud"] = 0;
        let includeUserCount = parseInt(
          this.countUsers["segment"]["countOfUsers"]
        );
        this.countUsers["segment"]["countOfUsers"] = includeUserCount;
      });

    this.cur_filter = new FilterData(
      tmpTab,
      "",
      "",
      "",
      "",
      false,
      false,
      "18",
      "7",
      "",
      "is",
      "42",
      "",
      [],
      FilterData.LOGIC_NONE
    );
  }

  onRemovedFilter(filter) {
    let sendCountData = {};
    if (filter.attr_kind == FilterData.TAB_USER_ATTR) {
      let idx = this.filters.users.indexOf(filter);
      if (idx != -1) {
        this.filters.users.splice(idx, 1);
      }
    } else if (filter.attr_kind == FilterData.TAB_DEVICE_ATTR) {
      let idx = this.filters.devices.indexOf(filter);
      if (idx != -1) {
        this.filters.devices.splice(idx, 1);
      }
    } else if (filter.attr_kind == FilterData.TAB_APP_EVENT_ATTR) {
      let idx = this.filters.app_events.indexOf(filter);
      if (idx != -1) {
        this.filters.app_events.splice(idx, 1);
      }
    }

    this.segmentService.setTmpUserSaveListData(null);

    /* get user count from API */
    sendCountData = this.sendFilterData();
    let sendData = {};
    sendData["segment"] = sendCountData["filters"];
    // sendData["appId"] = this.filters.productApp;
    sendData["segmentName"] = this.saveSegName;
    // console.log("56888888888" + sendData);
    this.dataForUserCount = sendData;
    this.segmentService
      .getCountsUserSegments(this.dataForUserCount)
      .then(res => {
        this.countUsers = res;
        this.countUsers["segment"]["countOfExclud"] = 0;
        let includeUserCount = parseInt(
          this.countUsers["segment"]["countOfUsers"]
        );
        this.countUsers["segment"]["countOfUsers"] = includeUserCount;
      });
  }

  /* $event is selected Tab id */
  onChangedTab($event) {
    this.cur_filter = new FilterData(
      $event,
      "",
      "",
      "",
      "",
      false,
      false,
      "18",
      "7",
      "",
      "is",
      "42",
      "",
      [],
      FilterData.LOGIC_NONE
    );
  }

  getTabName() {
    if (this.cur_filter) {
      if (this.cur_filter.attr_kind == 0) {
        return "User";
      } else if (this.cur_filter.attr_kind == 1) {
        return "Device";
      } else if (this.cur_filter.attr_kind == 2) {
        return "Event";
      }
    }
    return "";
  }
  /* send filters data to segment for save */
  sendFilterData() {
    /* add the data to the sendData */
    let segment = {};
    segment["segmentName"] = this.saveSegName;
    segment["isDraft"] = false;
    segment["filters"] = {};

    /*Compose user attributes elements */
    let userAttributes = [];
    for (var i = 0; i < this.filters.users.length; i++) {
      let userAttribute = {};
      userAttribute["attribute"] = this.filters.users[i].attr_name;
      userAttribute["condition"] = this.filters.users[i].operator;
      let userAttributeParams = [];
      for (var j = 0; j < this.filters.users[i].params.length; j++) {
        userAttributeParams[j] = this.filters.users[i].params[j].value;
      }
      userAttribute["values"] = userAttributeParams;
      userAttributes.push(userAttribute);
    }
    segment["filters"]["userAttributes"] = userAttributes;

    /*Compose device attributes elements */
    let deviceAttributes = [];
    for (var i = 0; i < this.filters.devices.length; i++) {
      let deviceAttribute = {};
      let deviceAttributeParams = [];
      deviceAttribute["attribute"] = this.filters.devices[i].attr_name;
      deviceAttribute["condition"] = this.filters.devices[i].operator;

      for (var j = 0; j < this.filters.devices[i].params.length; j++) {
        deviceAttributeParams[j] = this.filters.devices[i].params[j].value;
      }
      deviceAttribute["values"] = deviceAttributeParams;
      deviceAttributes.push(deviceAttribute);
    }
    segment["filters"]["deviceAttributes"] = deviceAttributes;

    /*Compose app events elements */
    let appEvents = [];
    for (var i = 0; i < this.filters.app_events.length; i++) {
      let appEvent = {};
      let appEventAttributeObj = {};
      let appEventAttribute = [];

      appEvent["conditionForEvent"] = this.filters.app_events[i].attr_name;
      appEvent["conditionForCount"] = this.filters.app_events[i].operator;
      appEvent["event"] = this.filters.app_events[i].attr_cond;
      appEvent["countOfTimes"] = parseInt(this.filters.app_events[i].cnt_times);
      appEvent["conditionForDuration"] = this.filters.app_events[i].attr_dura;
      appEvent["countOfDays"] = parseInt(this.filters.app_events[i].cnt_days);
      appEventAttributeObj["attribute"] = this.filters.app_events[
        i
      ].event_attr_name;
      appEventAttributeObj["condition"] = this.filters.app_events[
        i
      ].event_attr_type;
      appEventAttributeObj["value"] = this.filters.app_events[
        i
      ].event_attr_value;
      appEventAttribute[0] = appEventAttributeObj;
      if (this.filters.app_events[i].has_attr) {
        appEvent["attributes"] = appEventAttribute;
      } else {
        appEvent["attributes"] = [];
      }
      appEvents.push(appEvent);
    }
    segment["filters"]["appEvents"] = appEvents;

    /* excluded user IDs */
    // let excludID = [
    //   '9275cadd-3376-4d48-818e-3c6b8efa593b',
    //   'f168ea9d-ec1a-47fb-b9eb-dbd375ff618a',
    //   'f168ea9d-ec1a-47fb-b9eb-dbd375ff618a'
    // ];

    let excludIDs: any;
    let editState = this.segmentService.getEditSegmentState();
    if (editState == true) {
      if (
        this.individualSegment &&
        this.individualSegment["excludedUserIds"] &&
        this.individualSegment["excludedUserIds"].length > 0
      ) {
        excludIDs = [];
        this.individualSegment["excludedUserIds"].map(indSegment => {
          let eachExcludeID = { userId: indSegment };
          excludIDs.push(eachExcludeID);
        });
      }
    } else {
      excludIDs = this.segmentService.getTmpUserSaveListData();
    }

    let excludedUserIds = [];
    if (excludIDs) {
      for (var i = 0; i < excludIDs.length; i++) {
        excludedUserIds.push(excludIDs[i].userId);
      }
    }
    segment["excludedUserIds"] = excludedUserIds;
    segment["appId"] = this.filters.productApp;
    return segment;
  }

  /* save segments */
  saveSegments() {
    if (this.saveFlag == false) {
      this.saveFlag = true;

      let segment: any;
      segment = this.sendFilterData();

      /* send data */
      if (this.id) {
        /* update the data */
        this.sendData = segment;
        this.sendData["segmentId"] = this.id;
        // console.log("sendData", this.sendData);
        this.segmentService.updateSegments(this.sendData).then(res => {
          this.saveFlag = false;
          if (res == "SUCCESS") {
            console.log("update data success.");
            this.router.navigate(["/segments/list"]);
            return;
          } else {
            console.log("update data failed.");
            $("#failSaveSeg").trigger("click");
          }
        });
      } else {
        /* create new data */
        this.sendData["segment"] = segment;
        this.sendData["appId"] = this.filters.productApp;
        this.sendData[
          "userId"
        ] = this.storageService.getCurrentUser().user.userId;
        this.sendData[
          "orgId"
        ] = this.storageService.getCurrentUser().user.organizationId;
        // console.log(this.sendData);

        this.segmentService.sendSaveSegments(this.sendData).then(res => {
          this.saveFlag = false;
          if (res == "SUCCESS") {
            console.log("save data success.");
            this.router.navigate(["/segments/list"]);
            return;
          } else {
            console.log("save data failed.");
            $("#failSaveSeg").trigger("click");
          }
        });
      }
    }
  }

  confirmFilterContent() {
    let res = true;
    if (this.filters) {
      /* user attribute confirm to fill the empty item */
      if (this.filters.users.length > 0) {
        for (var i = 0; i < this.filters.users.length; i++) {
          if (
            this.filters.users[i].attr_name == "" ||
            this.filters.users[i].operator == ""
          ) {
            res = false;
            return res;
          }
          for (var k = 0; k < this.filters.users[i].params.length; k++) {
            if (this.filters.users[i].params[k].value == "") {
              res = false;
              return res;
            }
          }
        }
      }

      /* device attribute confirm to fill the empty item */
      if (this.filters.devices.length > 0) {
        for (var i = 0; i < this.filters.devices.length; i++) {
          if (
            this.filters.devices[i].attr_name == "" ||
            this.filters.devices[i].operator == ""
          ) {
            res = false;
            return res;
          }
          for (var k = 0; k < this.filters.devices[i].params.length; k++) {
            if (this.filters.devices[i].params[k].value == "") {
              res = false;
              return res;
            }
          }
        }
      }

      /* app event confirm to fill the empty item */
      if (this.filters.app_events.length > 0) {
        for (var i = 0; i < this.filters.app_events.length; i++) {
          if (
            this.filters.app_events[i].attr_name == "" ||
            this.filters.app_events[i].operator == "" ||
            this.filters.app_events[i].attr_cond == "" ||
            this.filters.app_events[i].cnt_times == "" ||
            this.filters.app_events[i].cnt_days == "" ||
            this.filters.app_events[i].attr_dura == ""
          ) {
            res = false;
            return res;
          }
          if (
            this.filters.app_events[i].has_attr == true &&
            (this.filters.app_events[i].event_attr_name == "" ||
              this.filters.app_events[i].event_attr_type == "" ||
              this.filters.app_events[i].event_attr_value == "")
          ) {
            res = false;
            return res;
          }
        }
      }
    }
    return res;
  }

  /* check the segment data */
  saveSegmentCheck() {
    if (
      (this.filters.users.length == 0 &&
        this.filters.devices.length == 0 &&
        this.filters.app_events.length == 0) ||
      this.saveSegName == "" ||
      !this.confirmFilterContent()
    ) {
      $("#emptyFilters").trigger("click");
      return;
    } else {
      if (this.id) {
        $("#saveFilters").trigger("click");
      } else {
        this.saveSegments();
      }
    }
  }

  /* go to the user list */
  goToUserCount($event) {
    $event.preventDefault();

    //this.segmentService.setTmpUserSaveListData(null);
    if (this.id) {
      this.tmpSaveData = this.individualSegment;
    }
    let filterData = this.sendFilterData();
    if (filterData) {
      if (!this.tmpSaveData) {
        this.tmpSaveData = {};
      }
      this.tmpSaveData["filters"] = filterData["filters"];
      this.tmpSaveData["segmentName"] = this.saveSegName;
      this.tmpSaveData["appId"] = filterData["appId"];
    } else {
      if (!this.tmpSaveData) {
        this.tmpSaveData = {};
      }
      this.tmpSaveData["segmentName"] = this.saveSegName;
    }

    this.segmentService.setTmpUserListData(this.tmpSaveData);
    this.segmentService.setUserListData(this.dataForUserCount);
    let editState = this.segmentService.getEditSegmentState();
    if (editState == true) {
      if (
        this.individualSegment &&
        this.individualSegment["excludedUserIds"] &&
        this.individualSegment["excludedUserIds"].length > 0
      ) {
        this.segmentService.setExcludedUserEditListData(
          this.individualSegment["excludedUserIds"]
        );
      }
    }

    this.router.navigate(["/segments/usercount"]);
  }

  /* go to the excluded user list */
  gotoExcludedUser($event) {
    let excludedUser = "isExcluded";
    $event.preventDefault();
    if (this.id) {
      this.tmpSaveData = this.individualSegment;
    }
    let filterData = this.sendFilterData();
    if (filterData) {
      if (!this.tmpSaveData) {
        this.tmpSaveData = {};
      }
      this.tmpSaveData["filters"] = filterData["filters"];
      this.tmpSaveData["segmentName"] = this.saveSegName;
      this.tmpSaveData["appId"] = filterData["appId"];
    } else {
      if (!this.tmpSaveData) {
        this.tmpSaveData = {};
      }
      this.tmpSaveData["segmentName"] = this.saveSegName;
    }

    this.segmentService.setTmpUserListData(this.tmpSaveData);
    this.segmentService.setUserListData(this.dataForUserCount);
    let editState = this.segmentService.getEditSegmentState();
    if (editState == true) {
      if (
        this.individualSegment &&
        this.individualSegment["excludedUserIds"] &&
        this.individualSegment["excludedUserIds"].length > 0
      ) {
        this.segmentService.setExcludedUserEditListData(
          this.individualSegment["excludedUserIds"]
        );
      }
    }

    this.router.navigate(["/segments/usercount", excludedUser]);
  }

  /* Save as Draft when click cancel button */
  gotoSaveAsDraft($event) {
    if (this.saveFlag == false) {
      this.saveFlag = true;

      let segment: any;
      segment = this.sendFilterData();

      /* send data */
      if (this.id) {
        /* update the data as draft */
        this.sendData = segment;
        this.sendData["segmentId"] = this.id;
        this.sendData["isDraft"] = true;

        this.segmentService.updateSegments(this.sendData).then(res => {
          this.saveFlag = false;
          if (res == "SUCCESS") {
            console.log("update data success.");
            this.router.navigate(["/segments/list"]);
            return;
          } else {
            console.log("update data failed.");
            $("#failSaveSeg").trigger("click");
          }
        });
      } else {
        /* create new data as draft*/
        this.sendData["segment"] = segment;
        this.sendData["segment"]["isDraft"] = true;
        this.segmentService.sendSaveSegments(this.sendData).then(res => {
          this.saveFlag = false;
          if (res == "SUCCESS") {
            console.log("save data success.");
            this.router.navigate(["/segments/list"]);
            return;
          } else {
            console.log("save data failed.");
            $("#failSaveSeg").trigger("click");
          }
        });
      }
    }
  }
}
