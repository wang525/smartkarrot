import { Component, OnInit, Input, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { SurveyConstants } from '../survey.constants';
import * as _ from 'lodash';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  constructor() { }
  @Input() question: any = {};
  @Output() onQuestionChange = new EventEmitter();
  enableFollowup: boolean = false;

  ngOnInit() {
    //console.log(this.question)
    this.enableFollowup = (this.question.followUp && this.question.followUp.length) ? true : false;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.question && !changes.question.isFirstChange()) {
      this.enableFollowup = (this.question.followUp && this.question.followUp.length) ? true : false;
      //console.log(this.question)
    }
  }


  onCheckFollow(event) {
    if (event && (!this.question.followUp || !this.question.followUp.length)) {
      if (!this.question.followUp) this.question.followUp = [];
      this.addFollowup();
    }
  }

  onChange(event) {
    this.question = _.cloneDeep(event);
    this.onQuestionChange.emit(this.question);
  }

  onFollowChange(event) {
    this.question.followUp = this.question.followUp.map(each => {
      if (each.qid == event.qid) {
        each = _.cloneDeep(event);
      }
      return each;
    })

    this.onQuestionChange.emit(this.question);
  }

  addFollowup() {
    let quesObj = {
      ...SurveyConstants.defaultQuestion,
      ...{ onResponse: this.question['options'][0], qid: this.question.followUp.length + 101 },
    };
    delete quesObj['followUp'];
    this.question.followUp.push(quesObj);

    this.onQuestionChange.emit(this.question);
  }

  removeFollowup(index) {
    this.question.followUp.splice(index, 1);
    if (this.question.followUp.length == 0)
      this.enableFollowup = false;
  }

  customTrackBy(index: number, obj: any): any {
    return index;
  }
}
