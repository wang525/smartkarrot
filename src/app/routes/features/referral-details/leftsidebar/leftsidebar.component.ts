import { Component, OnInit, Input, Output, EventEmitter  } from '@angular/core';
import { stepData } from '../../referral-details/referral-data';


@Component({
  selector: 'app-leftsidebar',
  templateUrl: './leftsidebar.component.html',
  styleUrls: ['./leftsidebar.component.scss']
})
export class LeftsidebarComponent implements OnInit {

  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;
  
  @Output() changeStepStatus = new EventEmitter();

  private stepData; 

  constructor() { }

  ngOnInit() {
    this.stepData = stepData;
  }

  changeCurrentStep(currentStep) {
    this.changeStepStatus.emit(currentStep);
  }
}
