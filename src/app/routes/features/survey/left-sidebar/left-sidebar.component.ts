import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import * as _ from 'lodash';

@Component({
  selector: 'app-left-sidebar',
  templateUrl: './left-sidebar.component.html',
  styleUrls: ['./left-sidebar.component.scss']
})
export class LeftSidebarComponent implements OnInit, OnChanges {

  constructor() { }

  tabSelected: { parent, sub } = { parent: false, sub: false };

  currentTab: any;
  currentSubTab: any;
  @Input() tabList: Array<object> = [];
  @Input() tab;
  @Input() webAppOnly;
  @Input() isWelcomeSelected;

  @Output() onTabChange = new EventEmitter();
  @Output() webAppOnlyChange = new EventEmitter();
  @Output() isWelcomeSelectedChange = new EventEmitter();

  ngOnInit() {
    this.updateCurrentTab(this.tab);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.tab && !changes.tab.isFirstChange() && (changes.tab.currentValue != this.tabSelected.sub && changes.tab.currentValue != this.tabSelected.parent)) {
      this.updateCurrentTab(this.tab);
    }
  }

  updateCurrentTab(selectedTab) {
    let tabSelected: any = _.filter(this.tabList, { key: selectedTab });

    if (tabSelected.length && !tabSelected[0].hasSub) {
      this.setTab(tabSelected[0].key, false);
      this.currentTab = this.tabList.indexOf(tabSelected[0]);
      this.currentSubTab = -1;
    } else {
      this.tabList.map((parentTab: any) => {
        if (parentTab.hasSub) {
          tabSelected = _.filter(parentTab.sub, { key: selectedTab });
          if (tabSelected.length) {
            this.currentTab = this.tabList.indexOf(parentTab);
            this.setTab(parentTab.key, tabSelected[0].key);
            this.currentSubTab = parentTab.sub.indexOf(tabSelected[0]);
          }
        }
      });
    }
  }

  setTab(parent, sub) {
    this.tabSelected.parent = parent;
    this.tabSelected.sub = sub;
    this.onTabChange.emit(sub ? sub : parent);
  }

  selectCheckbox(event) {
    this.isWelcomeSelectedChange.emit(event);
  }

  toggleWebApp(event) {
    this.webAppOnlyChange.emit(event)
  }

  customTrackBy(index: number, obj: any): any {
    return index;
  }
}
