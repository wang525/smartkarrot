
const Settings = {
    text: 'Settings',
    link: '/settings',
    icon: 'icon-settings',
    submenu: [
        {
            text: 'App Users',
            link: '/settings/app.Users'
        },
        {
            text: 'Roles & Permissions',
            link: '/settings/roles.permissions'
        },
        {
            text: 'My User & Roles',
            link: '/settings/myuser.roles'
        },
        {
            text: 'Finance',
            link: '/settings/finance'
        },
        {
            text: 'Branding',
            link: '/settings/branding'
        },
        {
            text: 'Cloud',
            link: '/settings/cloud'
        },
        {
            text: 'Integration',
            link: '/settings/integration'
        }
    ]
};

const Segments = {
    text: 'Segments',
    link: '/segments',
    icon: 'icon-target'
};

const Features = {
    text: 'Features',
    link: '/features',
    icon: 'icon-wrench',
    submenu: [
        {
            text: 'Wallet',
            link: '/features/wallet'
        },
        {
            text: 'Refferal',
            link: '/features/referral-list'
        },
        {
            text: 'Survey',
            link: '/features/survey-list'
        },
        {
            text: 'Donation',
            link: '/features/donation'
        },
        {
            text: 'Gamification',
            link: '/features/gamification'
        }
    ]
};

const Journey = {
    text: 'Journey',
    link: '/journey',
    icon: 'icon-plane'
};

const Analytics = {
    text: 'Analytics',
    link: '/analytics',
    icon: 'icon-chart'
};

const Billing = {
    text: 'Billing',
    link: '/billing',
    icon: 'icon-credit-card'
};

const Sandbox = {
    text: 'Sandbox',
    link: '/sandbox',
    icon: 'icon-cursor'
};

export const menu = [
    Settings,
    Segments,
    Features,
    Journey,
    Analytics,
    Billing,
    Sandbox
];
