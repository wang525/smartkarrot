import { Component, OnInit } from "@angular/core";
import "../../../settings/setting-module.scss";
import { FormGroup, Validators, FormControl } from "@angular/forms";
import { AuthService } from "../../../../auth/services/auth.service";
import { StorageService } from "../../../../auth/services/storage.service";
import * as googleLibphonenumber from "google-libphonenumber";
import { phoneNoValidation } from "../../../../auth/sign-up/custom-validator-phone";

declare var $: any;

export class User {
  name: string;
  emailId: string;
  phoneNumber: string;
  callingCode: string;
  organizationName: string;
  organizationId: string;
  deviceId: string;

  public constructor(init?: Partial<User>) {
    Object.assign(this, init);
  }
}
@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"]
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  accessToken: string;
  userId: string;
  userDetails: any;
  verifiedIcon: String = "assets/images/verified (1).png";
  verifyIcon: String = "assets/images/warningSet.png";
  errorMsg = "";
  form: FormGroup;
  countryPhoneNumber: any;
  user: any;
  organizationId: string;
  constructor(
    private authService: AuthService,
    private storageService: StorageService
  ) {
    let currentUser = this.storageService.getCurrentUser();
    let user = currentUser.user;
    this.userId = user.userId;
    this.accessToken = currentUser.accessToken;
    this.organizationId = user.organizationId;
  }

  ngOnInit() {
    this.buildForm();
    console.log(this.accessToken);
    let data = {
      userId: this.userId,
      accessToken: this.accessToken
    };
    this.authService.fetchProfile(data).subscribe(
      res => {
        if (res) {
          this.userDetails = res["user"];
          console.log(this.userDetails);
          if (this.userDetails.callingCode && this.userDetails.phoneNumber) {
            this.countryPhoneNumber =
              this.userDetails.callingCode + this.userDetails.phoneNumber;
          } else {
            this.countryPhoneNumber = "";
          }
        }
      },
      error => {
        console.log(error);
      }
    );

    // console.log(this.phoneNumberValidation("+1958075951"));
  }
  get mobile() {
    return this.profileForm.get("phoneNumber");
  }
  buildForm() {
    this.profileForm = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      emailId: new FormControl(null, [Validators.required, Validators.email]),
      phoneNumber: new FormControl(null, [phoneNoValidation]),
      organizationName: new FormControl(null, [Validators.required]),
      role: new FormControl(null, [Validators.required]),
      planType: new FormControl(null, [Validators.required])
    });
  }
  verify() {
    let data = {
      userId: this.userId,
      deviceId: this.storageService.generateUUID()
    };
    this.authService.sendVerifyOtp(data).subscribe(response => {
      if (response) {
        if (response["status"] == "SUCCESS") {
          $("#VerifyContact").modal("show");
        } else {
          console.log("no status");
        }
      } else {
        console.log("no response");
      }
    });
  }

  verifyMobile(otp) {
    console.log("otp" + otp);
    let data = {
      userId: this.userId,
      deviceId: this.storageService.generateUUID(),
      otp: otp
    };
    this.authService.verifyphone(data).subscribe(res => {
      if (res) {
        if (res["status"] == "SUCCESS") {
          alert("Phone number is verfied successfully");
          $("#VerifyContact").modal("hide");
          let data = {
            userId: this.userId,
            accessToken: this.accessToken
          };
          this.authService.fetchProfile(data).subscribe(
            res => {
              if (res) {
                this.userDetails = res["user"];
                if (
                  this.userDetails.callingCode &&
                  this.userDetails.phoneNumber
                ) {
                  this.countryPhoneNumber =
                    this.userDetails.callingCode + this.userDetails.phoneNumber;
                } else {
                  this.countryPhoneNumber = "";
                }
              }
            },
            error => {
              console.log(error);
            }
          );
        } else {
          console.log("No status");
        }
      } else {
        console.log("No response");
      }
    });
  }

  onSubmit() {
    try {
      if (this.profileForm.value.phoneNumber) {
        this.profileForm.value.callingCode = this.profileForm.value.phoneNumber.substring(
          0,
          this.profileForm.value.phoneNumber.length - 10
        );
        this.profileForm.value.phoneNumber = this.profileForm.value.phoneNumber.substring(
          this.profileForm.value.phoneNumber.length - 10,
          this.profileForm.value.phoneNumber.length
        );
      }
      this.profileForm.value.deviceId = this.storageService.generateUUID();
      this.profileForm.value.organizationId = this.organizationId;
      this.profileForm.value.userId = this.userId;
      this.user = this.profileForm.value;
      console.log("this.user" + JSON.stringify(this.user));
      this.authService.updateProfile(this.user).subscribe(response => {
        console.log("response" + JSON.stringify(response));
        if (response) {
          if (response["status"]) {
            const status = response["status"];
            switch (status) {
              case "USER_NOT_FOUND":
                alert("User Doesn't exist in the system");
                break;
              case "INVALID_INPUT":
                alert("Please provide the input");
                break;
              case "ERROR":
                alert("Something went wrong");
                break;
              case "SUCCESS":
                alert("Profile updated successfully");
                let data = {
                  userId: this.userId,
                  accessToken: this.accessToken
                };
                this.authService.fetchProfile(data).subscribe(
                  res => {
                    if (res) {
                      this.userDetails = res["user"];
                      if (
                        this.userDetails.callingCode &&
                        this.userDetails.phoneNumber
                      ) {
                        this.countryPhoneNumber =
                          this.userDetails.callingCode +
                          this.userDetails.phoneNumber;
                      } else {
                        this.countryPhoneNumber = "";
                      }
                    }
                  },
                  error => {
                    console.log(error);
                  }
                );
                break;
              default:
                alert("Something went wrong");
            }
          } else {
            console.log("no status");
          }
        } else {
          console.log("response is null");
        }
      });
    } catch (e) {
      console.log(e);
      this.errorMsg = "Please enter valid phone number";
    }
  }
  phoneNumberValidation(number) {
    const phoneUtil = googleLibphonenumber.PhoneNumberUtil.getInstance();
    const phoneNumber = phoneUtil.parse(number);
    return {
      country: phoneNumber.getCountryCode(),
      phone: phoneNumber.getNationalNumber()
    };
  }
}
