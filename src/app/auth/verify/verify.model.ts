export interface VerifyModel {
  password: string;
  deviceId: string;
  token: string;
  userId: string;
  oldPassword: string;
}
