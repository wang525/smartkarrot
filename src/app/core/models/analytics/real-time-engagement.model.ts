export interface RealTimeEngagementModel {
  realTimeUsers: [
    {
      minutesBeforeNow: number,
      userCount: number
    }
  ],
  totalUsersInLast30Minutes: number
}

export interface RealTimeEngagementDetailsModel {
  "activeLocations": string[],
  "topLocation": string,
  "topLocationCode": string,
  "topLocationCount": number
}
