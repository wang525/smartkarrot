import { Component, OnInit, Input } from '@angular/core';
import { RealTimeEngagementModel, RealTimeEngagementDetailsModel } from '../../../../core/models/analytics/real-time-engagement.model';
import { AnalyticsService } from '../../../../core/service/analytics/analytics.service';
import { Subscription } from 'rxjs';
import * as d3 from 'd3';

@Component({
  selector: 'app-realtime-engagement',
  templateUrl: './realtime-engagement.component.html',
  styleUrls: ['./realtime-engagement.component.scss']
})
export class RealtimeEngagementComponent implements OnInit {
  realTimeEngagementData: RealTimeEngagementModel;
  realTimeEngagementDetails: RealTimeEngagementDetailsModel;
  appId: string = this.analyticsService.getAppId();
  activeApp : any;
  subscription: Subscription;
  flagUrl:string;

  constructor(private analyticsService: AnalyticsService) { }

  ngOnInit() {
    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();
    this.appId = this.activeApp.appId;

    if (this.analyticsService.realTimeChartData) {
      this.realTimeEngagementData = this.analyticsService.realTimeChartData;
    }

    // this.changeParams();
    this.updateGraph();

    this.subscription = this.analyticsService.getActiveAppInfo().subscribe(activeApp => { 
      this.activeApp = activeApp.app;
      this.appId = this.activeApp.appId;
      // this.changeParams();
      this.updateGraph();
      // console.log('retention graph updated',activeApp);
    });
  }

  updateGraph() {
    this.realTimeEngagementDetails = null;
    this.flagUrl = '';
    this.analyticsService.getAppRealTimeEngagementDetails({
        appId: this.appId,
        "platforms": [ "Android", "iOS" ]
      })
      .subscribe(data => {
        this.realTimeEngagementDetails = <RealTimeEngagementDetailsModel>data;
        if(this.realTimeEngagementDetails.topLocationCode){
          this.flagUrl =  'https://countryflags.io/'+this.realTimeEngagementDetails.topLocationCode+'/flat/24.png';
        }else{
          this.flagUrl = '';
        }
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
