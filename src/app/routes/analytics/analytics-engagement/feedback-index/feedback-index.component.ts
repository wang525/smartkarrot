import { Component, OnInit } from '@angular/core';
import { AnalyticsService } from '../../../../core/service/analytics/analytics.service';
import {
  FeedbackIndexModel,
  FeedbackIndexTrendingModel
} from '../../../../core/models/analytics/feedback-index.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-feedback-index',
  templateUrl: './feedback-index.component.html',
  styleUrls: ['./feedback-index.component.scss']
})
export class FeedbackIndexComponent implements OnInit {
  data: FeedbackIndexModel;
  data_trending: FeedbackIndexTrendingModel;
  startDate: string = this.analyticsService.getFormattedDate(
    new Date().setDate(new Date().getDate() - 15)
  );
  endDate: string = this.analyticsService.getFormattedDate(new Date());

  // ToDo - Update code once API works properly
  // appId: string = this.analyticsService.getAppId();
  appId: string = '4ed062f1-fc38-4f32-a714-b63dc5399626';
  unit: string = 'month';
  filterChurns: string = 'Last 0.5 Month';
  activeApp : any;
  subscription: Subscription;

  constructor(private analyticsService: AnalyticsService) {

    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();
    this.appId = this.activeApp.appId;


    this.subscription = this.analyticsService.getActiveAppInfo().subscribe(activeApp => { 
    
      this.activeApp = activeApp.app;
      this.appId = this.activeApp.appId;
      this.updateGraph();
      this.getReviews();
    });

    this.getReviews();
    this.updateGraph();
  }

  getReviews() {
    this.data = null;
    this.analyticsService.getAppFeedbackIndexFrequency({
        appId: this.appId,
        platforms: ['android', 'iOS']
      }).subscribe(data => {
        this.data = data;
      });
  }
  getLastDate(days) {
    var date = new Date();
    var last = new Date(date.getTime() - (days * 24 * 60 * 60 * 1000));
    // var day =last.getDate();
    // var month=last.getMonth()+1;
    // var year=last.getFullYear();

    return this.analyticsService.getFormattedDate(last);
}

  changeParams(){
    this.endDate = this.analyticsService.getFormattedDate(new Date());
    let d = new Date();

    let multiplier:number = 30;

    let date_string = this.filterChurns;


    let selectedDay:number = parseFloat(date_string.match(/[\d.]+/)[0]);
    let numberOfDays:number = 1;

    numberOfDays = selectedDay*multiplier;

    let last_date = this.getLastDate(numberOfDays);
    this.startDate = last_date;

    this.unit = 'month';
    
    this.updateGraph();
  }

  getFormattedDate(date): string {
    let today = new Date(date);
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!

    let yyyy = today.getFullYear();
    if (dd < 10) {
      dd = <any>'0' + dd;
    }
    if (mm < 10) {
      mm = <any>'0' + mm;
    }
    return yyyy + '-' + mm + '-' + dd;
  }

  changeParamsOld() {
    this.endDate = this.getFormattedDate(new Date());
    let d = new Date();
    if (this.filterChurns == '3m' || this.filterChurns == '6m') {
      if (d.getMonth() < 3) {
        d.setFullYear(d.getFullYear() - 1);
        d.setMonth(d.getMonth() + 12 - <any>this.filterChurns.substring(0, 1));
      } else {
        d.setMonth(d.getMonth() - <any>this.filterChurns.substring(0, 1));
      }
      this.unit = 'month';
    }
    if (this.filterChurns == '6w') {
      d.setDate(d.getDate() - 42);
      this.unit = 'week';
    }
    this.startDate = this.getFormattedDate(d);
    this.updateGraph();
  }

  updateGraph() {
    this.data_trending = null;
    this.analyticsService
      .getAppFeedbackIndexTrending({
        startDate: this.startDate,
        endDate: this.endDate,
        appId: this.appId,
        unit: this.unit,
        "platforms": [ "Android", "iOS" ]
      })
      .subscribe(data => {
        this.data_trending = data;
      });
  }

  ngOnInit() {}

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
