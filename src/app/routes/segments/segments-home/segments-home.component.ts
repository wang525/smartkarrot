import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from '../../../core/service/common/common.service';

@Component({
  selector: "app-segments",
  templateUrl: "./segments-home.component.html",
  styleUrls: ["./segments-home.component.scss"]
})
export class SegmentsComponent implements OnInit {

  headName = ["User Group", "User Segments"]
  constructor(
    public commonService: CommonService,
    private router: Router) {}

  ngOnInit() {
    /* set header name to head bar */
    this.commonService.setHeaderName(this.headName);
  }

  gotoSegmentList($event) {
    $event.preventDefault();
    this.router.navigate(["/segments/list"]);
  }
}
