import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SurveyConstants } from "../survey.constants";
import { ImageUploadService } from "../../../../core/service/image-upload/image-upload.service";
import { DomSanitizer } from "@angular/platform-browser";
import * as _ from "lodash";

@Component({
  selector: "app-config-welcome",
  templateUrl: "./config-welcome.component.html",
  styleUrls: ["./config-welcome.component.scss"],
  providers: [ImageUploadService]
})
export class ConfigWelcomeComponent implements OnInit {
  showError: boolean;
  //applyAll: boolean = true;
  fontSelected: any = {};

  @Input() welcomeData;
  @Input() surveyName: string;
  @Input() isWebApp;
  @Input() applyAll;
  @Output() surveyNameChange = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();
  @Output() tabSelectedChange = new EventEmitter();
  @Output() applySelected = new EventEmitter();

  fontList: Array<object> = [];
  isDraft: boolean = false;

  constructor(
    private imageUploadSrvc: ImageUploadService,
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.fontList = [...SurveyConstants.FontList];
    this.fontSelected = _.filter(this.fontList, {
      key: this.welcomeData.fontStyle
    })[0];
  }

  onFontSelect(event) {
    this.fontSelected = _.filter(this.fontList, { key: event })[0];
  }

  saveSurvey() {
    this.showError = !this.surveyName || (/^ *$/.test(this.surveyName)) ? true : false;
    !this.showError && this.save.emit();
  }
  cancelSurvey() {
    this.cancel.emit();
  }

  onNameChange(event) {
    this.showError = !event ? true : false;
    this.surveyNameChange.emit(event);
  }

  pageChange(event) {
    this.tabSelectedChange.emit(event);
  }

  onApplySelected(event) {
    this.applySelected.emit(event);
  }

  fileChange(event) {
    let file: File = event.target.files[0];
    if (file["type"] == "image/jpeg" || file.type == "image/png")
      this.uploadFile(file);
    else alert("Please upload Jpeg or Png format. ");
  }

  uploadFile(file) {
    console.log(file);
    this.welcomeData.imageUrl = this._sanitizer.bypassSecurityTrustUrl(
      URL.createObjectURL(file)
    );
    console.log(this.welcomeData.imageUrl);
    this.imageUploadSrvc.getImageUrl().subscribe(async (resp: any) => {
      await this.imageUploadSrvc.uploadImageUrl(resp.url, file);
      this.welcomeData.imageUrl =
        this.imageUploadSrvc.getImageLoadUrl() + resp.key;
    });
  }
}
