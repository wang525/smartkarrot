import { Component, OnInit, ViewChild } from "@angular/core";
const screenfull = require("screenfull");
const browser = require("jquery.browser");
import { ActivatedRoute, Router } from "@angular/router";
declare var $: any;

import { UserblockService } from "../sidebar/userblock/userblock.service";
import { SettingsService } from "../../core/settings/settings.service";
import { MenuService } from "../../core/menu/menu.service";
import { CommonService } from "../../core/service/common/common.service";
import { AuthService } from "../../auth/services/auth.service";
import { StorageService } from "../../auth/services/storage.service";
import { SignedOutModel } from "./sign-out-model";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  signedOutModel: SignedOutModel;
  private storedData;
  navCollapsed = true; // for horizontal layout
  menuItems = []; // for horizontal layout

  isNavSearchVisible: boolean;
  @ViewChild("fsbutton") fsbutton; // the fullscreen button

  constructor(
    public commonService: CommonService,
    public authService: AuthService,
    private storageService: StorageService,
    public menu: MenuService,
    public userblockService: UserblockService,
    public settings: SettingsService,
    private router: Router
  ) {
    // show only a few items on demo
    this.menuItems = menu.getMenu().slice(0, 4); // for horizontal layout
    this.storedData = this.storageService.getCurrentUser();
  }

  ngOnInit() {
    this.isNavSearchVisible = false;
    if (browser.msie) {
      // Not supported under IE
      this.fsbutton.nativeElement.style.display = "none";
    }
  }

  signOut() {
    console.log("log");
    try {
      console.log(this.storageService.getCurrentUser());
      const tempObj = {
        deviceId: this.storageService.generateUUID(),
        accessToken: this.storageService.getCurrentUser().accessToken
      };
      this.signedOutModel = new SignedOutModel(tempObj);
      console.log(this.signedOutModel);

      this.authService.signOut(this.signedOutModel).subscribe(
        data => {
          console.log(data);
          if (data) {
            if (data["status"] === "SUCCESS") {
              this.storageService.clearAll();
              this.router.navigate(["/auth/sign-in"]);
            } else {
            }
          } else {
          }
        },
        error => {
          console.log(error);
        }
      );
    } catch (error) {
      console.log(error);
    }
    // this.router.navigateByUrl('auth/sign-up');
  }

  alert() {
    // $("#myModal").modal({
    //   backdrop: "static",
    //   keyboard: false
    // });
    alert("This account is currently available under Sandbox ONLY");
  }

  sandboxConfigChanged(event) {
    console.log(event);
  }

  toggleUserBlock(event) {
    event.preventDefault();
    this.userblockService.toggleVisibility();
  }

  openNavSearch(event) {
    event.preventDefault();
    event.stopPropagation();
    this.setNavSearchVisible(true);
  }

  setNavSearchVisible(stat: boolean) {
    // console.log(stat);
    // this.isNavSearchVisible = stat;
  }

  getNavSearchVisible() {
    return this.isNavSearchVisible;
  }

  toggleOffsidebar() {
    this.settings.toggleLayoutSetting("offsidebarOpen");
  }

  toggleCollapsedSideabar() {
    this.settings.toggleLayoutSetting("isCollapsed");
  }

  isCollapsedText() {
    return this.settings.getLayoutSetting("isCollapsedText");
  }

  changeActive(menu, idx, url) {
    for (var i = 0; i < menu.length; i++) {
      if (i == idx) {
        menu[i]["active"] = true;
      } else {
        menu[i]["active"] = false;
      }
    }
    if (url != "") {
      this.router.navigate([url]);
    }
  }
}
