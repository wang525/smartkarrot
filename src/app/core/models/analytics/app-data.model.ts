export interface AppDataModel {
  appId: string,
  appName: string,
  platforms: string[]
}
