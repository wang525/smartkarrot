import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";
import { SettingsService } from "../../settings/settings.service";
import { Settings } from "http2";
import { UserAttr, DeviceAttr, AppEvent, TypeData, OperatorData } from "./data";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";
import { Options } from "selenium-webdriver/opera";
import { StorageService } from "../../../auth/services/storage.service";

@Injectable({
  providedIn: "root"
})
export class SegmentService {
  productAppIdList: any; /*product App Id List */

  possible_attrs = {
    user_attrs: {} /* user_attr : Hash[name][UserAttr] */,
    device_attrs: {} /* device_attr : Hash[name][DeviceAttr] */,
    app_events: {} /* app_events : Hash[name][AppEvent] */,
    types: {} /* types : Hash[name][TypeData] */
  };

  autocompleteValues: any; /* auto complete values */

  countUser: any; /* count of users in the segments */

  resSaveSegments: any; /* result of save segments */

  resUpdateSegments: any; /* update the segment data */

  deleteSegment: any; /* delete the segment data */

  fetchSingleGroup: any; /* fetch single user group content */

  duplicateSingleGroup: any; /* duplicate single user group content */

  deleteGroup: any; /* delete the Usergroup data */

  listUserSegments: any; /* list user segments */

  userList: any; /* user list */

  individualSegment: any; /* data of individual Segment */

  listUserGroup: any; /* user group list data */

  userListInGroup: any; /* user list in groups */

  resUpdateUserGroup: any; /* update user group data */

  resSaveUserGroup: any; /* result of save user group */

  dataUserList: any; /* save the user list data */

  dataUserGroupList: any; /* save the user group list data */

  dataExcludedUserEditList: any; /* save excluded user data from edit segment */

  editSegmentState = false; /* edit segment state */

  dataTmpUserList: any; /* save the temporary user list data */

  dataTmpUserSaveList: any; /* save the temporary user list data */

  private inited = false;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  /* API */
  getBaseApiUrl() {
    return environment.baseApiUrl;
  }

  getBaseApiUrlSurvey() {
    return environment.baseApiUrlSurvey;
  }

  getApiUrl(subPath) {
    return environment.baseApiUrl + subPath;
  }

  getApiUrlSurvey(subPath) {
    return environment.baseApiUrlSurvey + subPath;
  }

  getApiUrlAutoComplete(subPath) {
    return environment.baseApiUrlAutoComplete + subPath;
  }

  getApiKey() {
    return environment.apiKey;
  }

  getApiValue() {
    return environment.apiValue;
  }

  getProductAppIdList(organizationId) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userSegments/fetchOrgApps"), organizationId)
        .subscribe(
          res => {
            this.productAppIdList = res;
            resolve(this.productAppIdList);
          },
          err => {
            console.log("product App Id list failed");
            resolve(this.productAppIdList);
          }
        );
    });
  }

  getPossibleAttrs(selectedAppId) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userSegments/listPossibleAttributeValues"), {
          appId: selectedAppId
        })
        .subscribe(
          res => {
            // console.log("possible attribute res", res)
            /* Get Attributes options */
            this.possible_attrs.user_attrs =
              res["possibleAttributeValues"]["userAttributes"];
            this.possible_attrs.device_attrs =
              res["possibleAttributeValues"]["deviceAttributes"];
            this.possible_attrs.app_events =
              res["possibleAttributeValues"]["appEvents"];

            /* Make Look-up table for types */
            var types = res["types"];
            for (var i = 0; i < types.length; i++) {
              this.possible_attrs.types[types[i]["type"]] =
                types[i]["operators"];
            }
            this.inited = true;
            resolve(this.possible_attrs);
          },
          err => {
            console.log("Get Possible Attributes Error");

            resolve(this.possible_attrs);
          }
        );
    });
  }

  /* get user counts in segments */
  getCountsUserSegments(segmentId) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrlSurvey("usersegement/getCountOfUsers"), segmentId)
        // segmentId: "5ce648d3-ad73-40e3-b622-75b2eab01b5e"
        // })
        .subscribe(
          res => {
            this.countUser = res;
            resolve(this.countUser);
          },
          err => {
            console.log("get user of count failed", err);

            resolve(this.countUser);
          }
        );
    });
  }

  /* get auto complete attributes values */
  getAutoCompleteAttributeValues(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(
          this.getApiUrlSurvey("usersegement/listAutocompleteAttributeValues"),
          data
        )
        .subscribe(
          res => {
            this.autocompleteValues = res;
            resolve(this.autocompleteValues);
          },
          err => {
            console.log("Get Auto complete Values Error");
            resolve(this.autocompleteValues);
          }
        );
    });
  }

  /* get list user segments */
  getListUserSegments() {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userSegments/listUserSegments"), {
          // userId: this.storageService.getCurrentUser().user.userId
          orgId: this.storageService.getCurrentUser().user.organizationId
        })
        .subscribe(
          res => {
            if (res["status"] == "SUCCESS") {
              this.listUserSegments = res["segments"];
            }
            resolve(this.listUserSegments);
          },
          err => {
            console.log("List User Segments Error");

            resolve(this.listUserSegments);
          }
        );
    });
  }

  /* get user list */
  getUserList(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrlSurvey("usersegement/listUsersInSegment"), data)
        .subscribe(
          res => {
            this.userList = res;
            resolve(this.userList);
          },
          err => {
            console.log("Get User List Error");

            resolve(this.userList);
          }
        );
    });
  }

  getUserGroupUserList(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userGroups/fetchUserListInGroup"), data)
        .subscribe(
          res => {
            this.userList = res;
            resolve(this.userList);
          },
          err => {
            console.log("Get User List Error");
            resolve(this.userList);
          }
        );
    });
  }
  /* get individual segment */
  getIndividualSegment(id) {
    if (this.listUserSegments && this.listUserSegments.length > 0) {
      /* delete selecte segment from the screen list */
      let res =
        this.listUserSegments &&
        this.listUserSegments.find(segment => {
          return segment["segment"].segmentId == id;
        });
      return res;
    }
    return null;
  }

  /* get individual segment */
  setIndividualSegment(id) {
    if (this.listUserSegments && this.listUserSegments.length > 0) {
      /* delete selecte segment from the screen list */
      let res =
        this.listUserSegments &&
        this.listUserSegments.find(segment => {
          return segment.segmentId == id;
        });

      let segmentId = this.listUserSegments.indexOf(res);
      delete this.listUserSegments[segmentId]["filters"];
    }
  }

  /* set User List Data */
  setUserListData(data) {
    if (data) {
      this.dataUserList = data;
    }
  }

  /* set User Group List Data */
  setUserGroupListData(data) {
    if (data) {
      this.dataUserGroupList = data;
    }
  }

  /* set User Group List Data */
  setExcludedUserEditListData(data) {
    if (data) {
      this.dataExcludedUserEditList = data;
    }
  }

  /* set edit segment state */
  setEditSegmentState(data) {
    console.log("data state in the server side", data);
    this.editSegmentState = data;
  }

  /* get User List Data */
  getUserListData() {
    if (this.dataUserList) {
      return this.dataUserList;
    }
  }

  /* get User Group List Data */
  getUserGroupListData() {
    if (this.dataUserGroupList) {
      return this.dataUserGroupList;
    }
  }

  /* set User Group List Data */
  getExcludedUserEditListData() {
    if (this.dataExcludedUserEditList) {
      return JSON.parse(JSON.stringify(this.dataExcludedUserEditList));
    }
  }

  /* get edit segment state */
  getEditSegmentState() {
    if (this.editSegmentState) {
      return this.editSegmentState;
    }
  }

  /* set User List Data */
  setTmpUserListData(data) {
    console.log("data in service ==", data);
    if (data) {
      console.log("data in service1111 ==", data);
      this.dataTmpUserList = data;
    }
  }

  /* get User List Data */
  getTmpUserListData() {
    console.log(this.dataTmpUserList);
    if (this.dataTmpUserList) {
      return this.dataTmpUserList;
    }
  }

  /* set User List Data */
  setTmpUserSaveListData(data) {
    if (data) {
      this.dataTmpUserSaveList = JSON.parse(JSON.stringify(data));
    } else {
      this.dataTmpUserSaveList = null;
    }
  }

  /* get User List Data */
  getTmpUserSaveListData() {
    if (this.dataTmpUserSaveList) {
      return JSON.parse(JSON.stringify(this.dataTmpUserSaveList));
    }
  }

  /* get duplicate segment */
  getDuplicateSegment(segment) {
    if (segment) {
      let resSave = JSON.parse(JSON.stringify(segment));
      delete resSave.segmentId;
      delete resSave.createdAt;
      delete resSave.createdByOrgOfficerId;
      delete resSave.str;
      delete resSave.updatedAt;
      delete resSave.updatedByOrgOfficerId;
      //delete resSave.appId;

      let saveData = {};
      // console.log("resSave" + JSON.stringify(resSave));
      saveData["segment"] = resSave["segment"];
      saveData["segment"]["segmentName"] =
        "Copy of <" + resSave["segment"].segmentName + ">";
      saveData["userId"] = resSave.userId;
      saveData["orgId"] = resSave.orgId;
      saveData["appId"] = resSave.appId;

      return new Promise((resolve, reject) => {
        this.http
          .post(this.getApiUrl("userSegments/createSegment"), saveData)
          .subscribe(
            res => {
              if (res["status"] == "SUCCESS") {
                this.http
                  .post(this.getApiUrl("userSegments/listUserSegments"), {
                    // userId: this.storageService.getCurrentUser().user.userId
                    orgId: this.storageService.getCurrentUser().user
                      .organizationId
                  })
                  .subscribe(
                    res => {
                      if (res["status"] == "SUCCESS") {
                        this.listUserSegments = res["segments"];
                      }
                      resolve(this.listUserSegments);
                    },
                    err => {
                      console.log("List User Segments Error");
                      resolve(this.listUserSegments);
                    }
                  );
              }
            },
            err => {
              console.log("Send Save Segments Data Error");
              resolve(this.resSaveSegments);
            }
          );
      });
    }
  }

  /* send save new segments data */
  sendSaveSegments(sendData: any) {
    console.log("send data in API", sendData);
    // sendData['userId'] = this.storageService.getCurrentUser().user.userId;

    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userSegments/createSegment"), sendData)
        .subscribe(
          res => {
            console.log("save res", res);
            this.resSaveSegments = res["status"];
            resolve(this.resSaveSegments);
          },
          err => {
            console.log("Send Save Segments Data Error");
            resolve(this.resSaveSegments);
          }
        );
    });
  }

  /* update segments data */
  updateSegments(sendData: any) {
    sendData["userId"] = this.storageService.getCurrentUser().user.userId;
    sendData[
      "orgId"
    ] = this.storageService.getCurrentUser().user.organizationId;
    console.log("save send data from segemtn service==", sendData);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userSegments/updateSegment"), sendData)
        .subscribe(
          res => {
            this.resUpdateSegments = res["status"];
            resolve(this.resUpdateSegments);
          },
          err => {
            console.log("Update Segments Data Error");
            resolve(this.resUpdateSegments);
          }
        );
    });
  }

  /* fetch single user group content */
  fetchSingleUserGroup(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userGroups/fetchUserGroup"), data)
        .subscribe(
          res => {
            console.log("fetch single user group", res);
            this.fetchSingleGroup = res;
            resolve(this.fetchSingleGroup);
          },
          err => {
            console.log("Fetch Single User Group Content Error");
            resolve(this.fetchSingleGroup);
          }
        );
    });
  }

  /* delete segments data */
  deleteUserSegment(id) {
    console.log("delete the selected id", id);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userSegments/deleteSegment"), id)
        .subscribe(
          res => {
            this.deleteSegment = res["status"];
            resolve(this.deleteSegment);
          },
          err => {
            console.log("Delete Segments Data Error");

            resolve(this.deleteSegment);
          }
        );
    });
  }

  /* delete UserGroup data */
  deleteUserGroup(id) {
    console.log("delete the selected id", id);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userGroups/deleteUserGroup"), id)
        .subscribe(
          res => {
            console.log("delete res", res);
            this.deleteGroup = res["message"];
            resolve(this.deleteGroup);
          },
          err => {
            console.log("Delete UserGroup Data Error");
            resolve(this.deleteGroup);
          }
        );
    });
  }

  /* get user group list */
  getUserGroupList() {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userGroups/listUserGroups"), {
          userId: this.storageService.getCurrentUser().user.userId
        })
        .subscribe(
          res => {
            this.listUserGroup = res;
            resolve(this.listUserGroup);
          },
          err => {
            console.log("List User Group Error");

            resolve(this.listUserGroup);
          }
        );
    });
  }

  /* get user group list */
  getUserListInGroup(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userGroups/fetchUserListInApp"), data)
        .subscribe(
          res => {
            this.userListInGroup = res;
            resolve(this.userListInGroup);
          },
          err => {
            console.log("User List in Group Error");
            resolve(this.userListInGroup);
          }
        );
    });
  }

  /* duplicate single user group content */
  duplicateSingleUserGroup(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userGroups/fetchUserGroup"), data)
        .subscribe(
          res => {
            this.duplicateSingleGroup = res;
            if (this.duplicateSingleGroup) {
              delete this.duplicateSingleGroup["userGroupDetail"]["updatedBy"];
              delete this.duplicateSingleGroup["userGroupDetail"][
                "userGroupID"
              ];
              delete this.duplicateSingleGroup["userGroupDetail"]["createdBy"];
              delete this.duplicateSingleGroup["userGroupDetail"]["createdAt"];
              delete this.duplicateSingleGroup["userGroupDetail"]["updatedAt"];
              this.duplicateSingleGroup["userGroupDetail"]["userGroupName"] =
                "Copy of <" +
                this.duplicateSingleGroup["userGroupDetail"]["userGroupName"] +
                ">";
              this.duplicateSingleGroup["userGroupDetail"][
                "orgUserId"
              ] = this.storageService.getCurrentUser().user.userId;
              this.saveUserRule(
                this.duplicateSingleGroup["userGroupDetail"]
              ).then(res => {
                console.log("duplicate save res", res);
                if (res["status"] == "SUCCESS") {
                  resolve(res);
                }
              });
            }
          },
          err => {
            console.log("Fetch Single User Group Content Error");
            resolve(this.duplicateSingleGroup);
          }
        );
    });
  }

  /* update user rule as group */
  updateUserRule(data) {
    console.log("updateUserRule", data);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userGroups/updateUserGroup"), data)
        .subscribe(
          res => {
            this.resUpdateUserGroup = res;
            resolve(this.resUpdateUserGroup);
          },
          err => {
            console.log("User Rule Group Update Error");
            resolve(this.resUpdateUserGroup);
          }
        );
    });
  }

  /* save user rule as group */
  saveUserRule(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("userGroups/createUserGroup"), data)
        .subscribe(
          res => {
            this.resSaveUserGroup = res;
            resolve(this.resSaveUserGroup);
          },
          err => {
            console.log("User Rule Group Save Error");
            resolve(this.resSaveUserGroup);
          }
        );
    });
  }

  isInited() {
    return this.inited;
  }

  /* */
  getConditions(): Array<any> {
    if (this.possible_attrs.app_events["events"]) {
      return this.possible_attrs.app_events["events"];
    }
    return [];
  }

  getDurations(type): Array<any> {
    if (type && this.possible_attrs.app_events["conditionForDuration"]) {
      return this.possible_attrs.app_events["conditionForDuration"];
    }
    return [];
  }

  getOperators(type): Array<OperatorData> {
    if (type && this.possible_attrs.types[type]) {
      return this.possible_attrs.types[type];
    }
    return [];
  }

  getOperator(type, operator): OperatorData {
    let operators = this.getOperators(type);
    let op = null;
    if (operators) {
      for (let i = 0; i < operators.length; i++) {
        let oper = operators[i];
        if (oper.operator == operator) {
          op = oper;
          break;
        }
      }
    }
    return op;
  }

  getType(type): TypeData {
    let td = this.possible_attrs.types[type];
    return td;
  }

  /* User Attribute methods */
  getUserAttrs() {
    //return Object.keys(this.user_attrs);
    return this.possible_attrs.user_attrs;
  }
  getUserAttrType(attr_name): String {
    return this.possible_attrs.user_attrs[attr_name].type;
  }

  /* Device Attribute methods */
  getDeviceAttrs() {
    //return Object.keys(this.device_attrs);
    return this.possible_attrs.device_attrs;
  }
  getDeviceAttrType(attr_name): String {
    return this.possible_attrs.device_attrs[attr_name].type;
  }

  /* App Event Attribute methods */
  getAppEventAttrs() {
    //return Object.keys(this.device_attrs);
    return this.possible_attrs.app_events;
  }
  getAppEventAttrType(attr_name): String {
    return this.possible_attrs.app_events[attr_name].type;
  }
}
