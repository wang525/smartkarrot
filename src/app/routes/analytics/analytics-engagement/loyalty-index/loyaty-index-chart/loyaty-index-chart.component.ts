import { Component, OnChanges, ElementRef, ViewChild, Input } from '@angular/core';
import { LoyaltyIndexEngagementModel } from '../../../../../core/models/analytics/loyalty-index-engagement.model';
import * as d3 from 'd3';

@Component({
  selector: 'app-loyaty-index-chart',
  templateUrl: './loyaty-index-chart.component.html',
  styleUrls: ['./loyaty-index-chart.component.scss']
})
export class LoyatyIndexChartComponent implements OnChanges {

  @ViewChild('chart')
  private chartContainer: ElementRef;

  @Input()
  data: LoyaltyIndexEngagementModel;

  @Input()
  chartStyle: string = 'normal';

  margin = {top: 20, right: 20, bottom: 30, left: 40};

  constructor() { }

  ngOnChanges(): void {
    if (!this.data) { return; }

    if (this.chartStyle == 'lineCurve') {
      this.createLineChart();
    } else {
      this.createChart();
    }
  }

  private createChart(): void {
    const element = this.chartContainer.nativeElement;
    let data = this.data.loyaltyIndices;

    d3.select('svg').remove();    

    const svg = d3.select(element).append('svg')
        .attr('width', element.offsetWidth)
        .attr('height', element.offsetHeight);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    const x = d3
      .scaleBand()
      .rangeRound([0, contentWidth])
      .padding(0.6)
      .domain(data.map(d => <any>d.timeRange));

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([0, d3.max(data, d => d.churn)]);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + contentHeight + ')')
      .call(d3.axisBottom(x));

    g.selectAll('.bar')
      .data(data)
      .enter().append('rect')
        .attr('class', 'bar')
        .attr('style', 'fill: #FFB56D')
        .attr('x', d => x(<any>d.timeRange))
        .attr('y', d => y(<any>d.churn))
        .attr('width', x.bandwidth())
        .attr('height', d => contentHeight - y(d.churn));

    g.selectAll(".bartext")
      .data(data)
      .enter().append("text")
      .attr("class", "bartext")
      .attr("text-anchor", "middle")
      .attr("fill", "black")
      .attr("x", function(d, i) {
          return x(d.timeRange) + x.bandwidth() / 2;
        })
      .attr("y", function(d,i) {
          return y(d.churn) - 10;
        })
      .text(function(d){
          return d.churn.toFixed(2);
        });

    let elem = svg.selectAll("g myCircleText")
      .data(data)
  
    let elemEnter = elem.enter()
      .append("g")
      .attr("transform", (d) => {
        let r = d['sessionLength']*0.75;
        r = r < 20 ? 20 : r > 35 ? 35 : r;
        return "translate("+(x(d.timeRange) + x.bandwidth() - Math.floor(r/2))+",260)";
      })
 
    elemEnter.append("circle")
      .attr("r", function(d) {
        let r = d['sessionLength']*0.75;
        return r < 20 ? 20 : r > 35 ? 35 : r;
      })
      .attr("fill", "#5306E0");
 
    elemEnter.append("text")
      .attr("x", function(d){ return d.sessionLength > 9 ? -16 : -11 })
      .attr("y", function(d){ return 4 })
      .attr("fill", "white")
      .text(function(d){return d['sessionLength'].toFixed(2)});
  }

  private createLineChart(): void {
    const element = this.chartContainer.nativeElement;
    let data = this.data.loyaltyIndices;

    d3.select(element).select('svg').remove();

    const svg = d3.select(element).append('svg')
        .attr('width', element.offsetWidth)
        .attr('height', element.offsetHeight);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    const x = d3
      .scaleBand()
      .rangeRound([0, contentWidth])
      .padding(0.6)
      .domain(data.map(d => <any>d.timeRange));

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([0, d3.max(data, d => d.churn)]);

    const line = d3.line()
        .x((d) => { return x(d['timeRange']); })
        .y((d) => { return y(d['churn']); })
        .curve(d3.curveMonotoneX);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + contentHeight + ')')
      .call(d3.axisBottom(x));

    g.append("path")
      .attr("class", "path")
      .attr("fill", "none")
      .attr("stroke", "#5306E0")
      .attr("stroke-width", 1.5)
      .attr("class", "line")
      .attr("d", line(<any>data));

    g.selectAll(".dot")
      .data(data)
      .enter().append("circle")
      .attr("class", "dot")
      .attr("cx", function(d) { return x(d['timeRange']) })
      .attr("cy", function(d) { return y(d['churn']) })
      .attr("fill", "#5306E0")
      .attr("r", 5);
        // .on("mouseover", function(a, b, c) { 
        //   console.log(a);
        //   // this.attr('class', 'focus');
        // })
        // .on("mouseout", function() {  })
  }
}
