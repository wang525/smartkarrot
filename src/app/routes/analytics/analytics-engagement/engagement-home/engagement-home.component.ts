import { Component, OnInit } from '@angular/core';
import { EngagementScoreModel } from '../../../../core/models/analytics/engagement-score.model';
import { EngagementScoreTrendsModel } from '../../../../core/models/analytics/engagement-score-trends.model';
import { RealTimeEngagementModel } from '../../../../core/models/analytics/real-time-engagement.model';
import { AnalyticsService } from '../../../../core/service/analytics/analytics.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-engagement-home',
  templateUrl: './engagement-home.component.html',
  styleUrls: ['./engagement-home.component.scss']
})
export class EngagementHomeComponent implements OnInit {

  engScorePercentileData: EngagementScoreModel;
  engScoreTrendsData: EngagementScoreTrendsModel;
  realTimeEngagementData: RealTimeEngagementModel;
  percentileItems: number = 0;
  appId: string = '4ed062f1-fc38-4f32-a714-b63dc5399626';
  activeApp : any;
  subscription: Subscription;

  constructor(private analyticsService: AnalyticsService) {


    
    this.activeApp = this.analyticsService.getActiveAppInfoOnLoad();
    this.appId = this.activeApp.appId;
    this.updateGraph();


    this.subscription = this.analyticsService.getActiveAppInfo().subscribe(activeApp => { 
    
      
      this.activeApp = activeApp.app;
      this.appId = this.activeApp.appId;
      this.updateGraph();
    });

   }

  

  updateGraph() {
    this.engScorePercentileData = null;
    this.engScoreTrendsData = null;
    this.realTimeEngagementData = null;

    this.analyticsService.getAppEngagementScore({ appId: this.appId,
        "platforms": [ "Android", "iOS" ] })
      .subscribe(data => {
        this.engScorePercentileData = data;
        this.percentileItems = this.engScorePercentileData.scorePercentile / 10;
      });

    this.analyticsService.getAppEngagementScoreTrends({ appId: this.appId, unit: "day",
        "platforms": [ "Android", "iOS" ] })
    .subscribe(data => {
      this.engScoreTrendsData = data;
    });

    this.analyticsService.getAppRealTimeEngagement({ appId: this.appId,
        "platforms": [ "Android", "iOS" ] })
    .subscribe(data => {
      this.realTimeEngagementData = data;
      this.analyticsService.realTimeChartData = data;
    });
  }
  ngOnInit(){
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
