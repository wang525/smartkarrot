/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}
interface JQuery {
  slimScroll(options?: any): any;
}
declare var System: any;
