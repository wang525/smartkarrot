import { Component, OnChanges, ElementRef, ViewChild, Input } from '@angular/core';
import { IndicesDurationDataModel, IndicesLoyaltyDataModel, IndicesRetentionDataModel, IndicesFeedbackDataModel } from '../../../../../core/models/analytics/indices-trend-data.model';
import { DurationIndexFrequencyModel } from '../../../../../core/models/analytics/duration-index-frequency.model';
import * as d3 from 'd3';

@Component({
  selector: 'app-indices-line-chart',
  templateUrl: './indices-line-chart.component.html',
  styleUrls: ['./indices-line-chart.component.scss']
})
export class IndicesLineChartComponent implements OnChanges {

  @ViewChild('chart')
  private chartContainer: ElementRef;

  @Input()
  data: Object

  @Input()
  modelType: string

  dataKey: string;
  yValue: string;
  margin = {top: 20, right: 20, bottom: 30, left: 20};

  constructor() { }

  ngOnChanges(): void {
    if (!this.data) { return; }

    switch (this.modelType) {
      case "duration":
        this.data = <IndicesDurationDataModel>this.data;
        this.dataKey = "sessionDurations";
        this.yValue = "averageSessionLength";
        break;
      case "loyalty":
        this.data = <IndicesLoyaltyDataModel>this.data;
        this.dataKey = "loyaltyIndexTrends";
        this.yValue = "churnPercentage";
        break;
      case "retention":
        this.data = <IndicesRetentionDataModel>this.data;
        this.dataKey = "retentionIndexTrends";
        this.yValue = "percentageRetention";
        break;
      case "feedback":
      default:
        this.data = <IndicesFeedbackDataModel>this.data;
        this.dataKey = "averageRatings";
        this.yValue = "averageRating";
        break;
    }
    // console.log(this.dataKey);
    this.createChart();
  }

  private createChart(): void {
    const element = this.chartContainer.nativeElement;
    let data = this.data[this.dataKey];
    const xValue = 'daysBefore';
    const yValue = this.yValue;

    d3.select(element).select('svg').remove();

    const svg = d3.select(element).append('svg')
        .attr('width', element.offsetWidth)
        .attr('height', element.offsetHeight);

    const contentWidth = element.offsetWidth - this.margin.left - this.margin.right;
    const contentHeight = element.offsetHeight - this.margin.top - this.margin.bottom;

    data.sort((a,b) => (a['daysBefore'] < b['daysBefore']) ? 1 : ((b['daysBefore'] < a['daysBefore']) ? -1 : 0));

    data.forEach(function(d) {
      d[xValue] = d[xValue] == 0 ? "Current" : d[xValue] + " days";
      d[yValue] = +d[yValue];
    });

    const x = d3
      .scalePoint()
      // .padding(0.5)
      .rangeRound([0, contentWidth])
      .domain(data.map(d => { return d[xValue]; }));

    const y = d3
      .scaleLinear()
      .rangeRound([contentHeight, 0])
      .domain([0, <any>d3.max(data, d => d[yValue])]);

    const line = d3.line()
        .x((d) => { return x(d[xValue]); })
        .y((d) => { return y(d[yValue]); })
        .curve(d3.curveMonotoneX);

    const g = svg.append('g')
      .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');

    g.append('g')
      .attr('class', 'axis axis--x')
      .attr('transform', 'translate(0,' + contentHeight + ')')
      .call(d3.axisBottom(x).ticks(4));

    g.append("path")
      .attr("class", "path")
      .attr("fill", "none")
      .attr("stroke", "#5306E0")
      .attr("stroke-width", 1.5)
      .attr("class", "line")
      .attr("d", line(data));

    g.selectAll(".dot")
      .data(data)
      .enter().append("circle")
      .attr("class", "dot")
      .attr("cx", function(d) { return x(d[xValue]) })
      .attr("cy", function(d) { return y(d[yValue]) })
      .attr("fill", "#5306E0")
      .attr("r", 5);
        // .on("mouseover", function(a, b, c) { 
        //   console.log(a);
        //   // this.attr('class', 'focus');
        // })
        // .on("mouseout", function() {  })


    g.append('g').selectAll("text")
      .data(data)
      .enter()
      .append("text")
      .attr("x", function(d) { return x(d['daysBefore']) - 8; })
      .attr("y", function(d) { return y(d[yValue]) - 10 })
      .attr("fill", "#5306E0")
      .style("font-size", "12px")
      .text(function(d) { return d[yValue].toFixed(1) });
  }
}
