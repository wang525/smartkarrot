export interface LoyaltyIndexEngagementModel {
  "unit": string,
  "loyaltyIndices": [
    {
      "churn": number,
      "sessionLength": number,
      "timeRange": string
    }
  ]
}
