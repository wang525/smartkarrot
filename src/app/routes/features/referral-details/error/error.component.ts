import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { stepData } from "../../referral-details/referral-data";
import { DomSanitizer } from "@angular/platform-browser";
import { ImageUploadService } from "../../../../core/service/image-upload/image-upload.service";

@Component({
  selector: "app-error",
  templateUrl: "./error.component.html",
  styleUrls: ["./error.component.scss"]
})
export class ErrorComponent implements OnInit {
  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;

  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();
  @Output() draftRefData = new EventEmitter();

  errData: Object;
  constructor(
    private imageUploadSrvc: ImageUploadService,
    private _sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.errData = this.referralData["screenConfiguration"]["errorScreen"];
  }

  /* change the step status */
  changeBackSetp() {
    this.changeStepStatus.emit(stepData.STEP4_REFERRAL_COUNT);
  }

  changeNextStep() {
    this.changeStepStatus.emit(stepData.STEP9_TECH_CONFIGURATION);
  }

  saveData() {
    this.saveRefData.emit();
  }

  saveAsDraft() {
    this.draftRefData.emit();
  }

  fileChange(event) {
    let file: File = event.target.files[0];
    if (file["type"] == "image/jpeg" || file.type == "image/png")
      this.uploadFile(file);
    else alert("Please upload Jpeg or Png format. ");
  }

  uploadFile(file) {
    this.errData["errorImageURL"] = this._sanitizer.bypassSecurityTrustUrl(
      URL.createObjectURL(file)
    );
    this.imageUploadSrvc.getImageUrl().subscribe(async (resp: any) => {
      console.log(resp);
      await this.imageUploadSrvc.uploadImageUrl(resp.url, file);
      this.errData["errorImageURL"] =
        this.imageUploadSrvc.getImageLoadUrl() + resp.key;
    });
  }
}
