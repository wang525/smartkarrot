import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
//import { SegmentsComponent } from "./segments/segments.component";
import { SegmentsComponent } from "./segments-home/segments-home.component";
import { Routes, RouterModule } from "@angular/router";
import { SegmentEditComponent } from "./segment-edit/segment-edit.component";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { TooltipModule } from "ngx-bootstrap/tooltip";
import { TypeaheadModule } from "ngx-bootstrap/typeahead";
import { InfiniteScrollModule } from "ngx-infinite-scroll";

import { UserCountComponent } from "./user-count/user-count.component";
import { FilterLineComponent } from "./filter-line/filter-line.component";
import { FilterTabsComponent } from "./filter-tabs/filter-tabs.component";
import { SegmentsListComponent } from "./segments-list/segments-list.component";
import { SegmentCreateComponent } from "./segment-create/segment-create.component";
import { UserGroupComponent } from "./user-group/user-group.component";
import { UserGroupListComponent } from "./user-group-list/user-group-list.component";
import { UserGroupCountComponent } from "./user-group-count/user-group-count.component";
import { GeoFenceComponent } from "./geo-fence/geo-fence.component";

const routes: Routes = [
  // { path: '', component: SegmentsComponent },
  // { path:':id/edit', component: SegmentEditComponent },
  // { path:'new', component: SegmentEditComponent },
  // { path:'usercount', component: UserCountComponent },
  { path: "", component: SegmentsComponent },
  { path: "list", component: SegmentsListComponent },
  { path: "list/:id", component: SegmentsListComponent },
  { path: "edit/:id", component: SegmentEditComponent },
  { path: "create", component: SegmentEditComponent },
  { path: "create/:id", component: SegmentEditComponent },
  { path: "usercount", component: UserCountComponent },
  { path: "usercount/:id", component: UserCountComponent },
  { path: "usergroup", component: UserGroupComponent },
  { path: "usergroup/:groupId/:appId", component: UserGroupComponent },
  { path: "usergrouplist", component: UserGroupListComponent },
  { path: "usergroupcount", component: UserGroupCountComponent },
  { path: "geofencing", component: GeoFenceComponent }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    TypeaheadModule.forRoot(),
    TooltipModule.forRoot(),
    InfiniteScrollModule
  ],
  declarations: [
    SegmentsComponent,
    SegmentEditComponent,
    UserCountComponent,
    FilterLineComponent,
    FilterTabsComponent,
    SegmentCreateComponent,
    SegmentsListComponent,
    UserGroupComponent,
    UserGroupListComponent,
    UserGroupCountComponent,
    GeoFenceComponent
  ],
  exports: [RouterModule, TypeaheadModule]
})
export class SegmentsModule {}
