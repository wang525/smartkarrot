import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveySingleComponent } from './survey-single.component';

describe('SurveySingleComponent', () => {
  let component: SurveySingleComponent;
  let fixture: ComponentFixture<SurveySingleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveySingleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveySingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
