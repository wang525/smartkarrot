import "../../../settings/setting-module.scss";
import { Component, OnInit, AfterViewInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { StorageService } from "../../../../auth/services/storage.service";
import { AuthService } from "../../../../auth/services/auth.service";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { VerifyModel } from "../../../../auth/verify/verify.model";
declare var $: any;
@Component({
  selector: "app-change-password",
  templateUrl: "./change-password.component.html",
  styleUrls: ["./change-password.component.scss"]
})
export class ChangePasswordComponent implements OnInit {
  VerifyModel: VerifyModel = {
    password: "",
    deviceId: "",
    token: "",
    userId: "",
    oldPassword: ""
  };
  form: FormGroup;
  errorMessage: string;
  token: string;
  userId: string;

  constructor(
    private formBuilder: FormBuilder,
    private storageService: StorageService,
    private authService: AuthService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    let currentUser = this.storageService.getCurrentUser();
    let user = currentUser.user;
    this.userId = user.userId;
    console.log("3777777 change password" + this.userId);
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      curpwd: [null, Validators.required],
      rpwd: [null, Validators.required],
      newpwd: [null, Validators.required]
    });
  }

  submit() {
    console.log("this.form.value" + JSON.stringify(this.form.value));
    this.VerifyModel.password = this.form.value.newpwd;
    this.VerifyModel.oldPassword = this.form.value.curpwd;
    this.VerifyModel.deviceId = this.storageService.generateUUID();
    this.VerifyModel.userId = this.userId;

    if (this.form.value.newpwd != this.form.value.rpwd) {
      this.errorMessage = "Password mismatch";
    } else {
      try {
        this.authService.updatePassword(this.VerifyModel).subscribe(
          data => {
            console.log("datadatatad  " + JSON.stringify(data));
            const status = data["status"];
            switch (status) {
              case "INVALID_INPUT":
                alert(status);
                break;
              case "EXPIRED_TOKEN":
                alert(status);
                break;
              case "ERROR":
                alert("Something went wrong");
                break;
              case "SUCCESS":
                this.storageService.setCurrentUser(data);
                this.router.navigate(["auth/sign-in"]);
                break;
              default:
                alert("Something went wrong");
            }
          },
          error => {
            console.log(error);
          }
        );
      } catch (error) {}
    }
    console.log(this.form.value);
  }
}
