export interface DurationIndexFrequencyModel {
  "unit": string,
  "sessionFrequency": [
    {
      "date": string,
      "count": number
    }
  ]
}
