import { Platform } from "@angular/cdk/platform";
import { Url } from "url";

export interface GettingStarted {
  platform: string;
  appName: string;
  bundleIdentitfier: string;
  appStoreId: string;
  appRating: number;
  isVerified: boolean;
  appUserCount: number;
  apikey: string;
  clientId: string;
  clientSecret: string;
  refreshToken: string;
  filePassword: string;
  domainName: string;
}
