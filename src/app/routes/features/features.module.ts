import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { Routes, RouterModule } from "@angular/router";
import { OwlDateTimeModule, OwlNativeDateTimeModule } from "ng-pick-datetime";
import { TooltipModule } from "ngx-bootstrap/tooltip";

import { ReferralListComponent } from "./referral-list/referral-list.component";
import { ReferralDetailsComponent } from "./referral-details/referral-details.component";
import { LeftsidebarComponent } from "./referral-details/leftsidebar/leftsidebar.component";
import { AudienceComponent } from "./referral-details/audience/audience.component";
import { RuleConfigComponent } from "./referral-details/rule-config/rule-config.component";
import { MessageComponent } from "./referral-details/message/message.component";
import { HomeComponent } from "./referral-details/home/home.component";
import { ReferralCountComponent } from "./referral-details/referral-count/referral-count.component";
import { WithdrawComponent } from "./referral-details/withdraw/withdraw.component";
import { AddCountComponent } from "./referral-details/add-count/add-count.component";
import { WithdrawalComponent } from "./referral-details/withdrawal/withdrawal.component";
import { ErrorComponent } from "./referral-details/error/error.component";
import { TechConfigComponent } from "./referral-details/tech-config/tech-config.component";
import { SurveyListComponent } from "./survey/survey-list/survey-list.component";
import { SurveySingleComponent } from "./survey/survey-single/survey-single.component";
import { SurveyMultipleComponent } from "./survey/survey-multiple/survey-multiple.component";
import { LeftSidebarComponent } from "./survey/left-sidebar/left-sidebar.component";
import { SurveyQuestionComponent } from "./survey/survey-question/survey-question.component";
import { ConfigWelcomeComponent } from "./survey/config-welcome/config-welcome.component";
import { ConfigQuestionComponent } from "./survey/config-question/config-question.component";
import { ConfigThankyouComponent } from "./survey/config-thankyou/config-thankyou.component";
import { ActionBlockComponent } from "./survey/action-block/action-block.component";
import { RatingOptionsComponent } from "./survey/rating-options/rating-options.component";
import { QuestionComponent } from "./survey/question/question.component";

const routes: Routes = [
  { path: "referral-list", component: ReferralListComponent },
  { path: "referral-details", component: ReferralDetailsComponent },
  { path: "referral-details/:id", component: ReferralDetailsComponent },
  { path: "survey-list", component: SurveyListComponent },
  { path: "survey-single", component: SurveySingleComponent },
  { path: "survey-single/:id", component: SurveySingleComponent },
  { path: "survey-multiple", component: SurveyMultipleComponent },
  { path: "survey-multiple/:id", component: SurveyMultipleComponent }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    RouterModule.forChild(routes),
    TooltipModule.forRoot()
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [
    ReferralListComponent,
    ReferralDetailsComponent,
    LeftsidebarComponent,
    AudienceComponent,
    RuleConfigComponent,
    MessageComponent,
    HomeComponent,
    ReferralCountComponent,
    WithdrawComponent,
    AddCountComponent,
    WithdrawalComponent,
    ErrorComponent,
    TechConfigComponent,
    SurveyListComponent,
    SurveySingleComponent,
    SurveyMultipleComponent,
    LeftSidebarComponent,
    SurveyQuestionComponent,
    ConfigWelcomeComponent,
    ConfigQuestionComponent,
    ConfigThankyouComponent,
    ActionBlockComponent,
    RatingOptionsComponent,
    QuestionComponent
  ]
})
export class FeaturesModule {}
