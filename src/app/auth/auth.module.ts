import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SignUpComponent } from "./sign-up/sign-up.component";
import { Routes, RouterModule } from "@angular/router";
import { ReactiveFormsModule } from "../../../node_modules/@angular/forms";
import { FormsModule } from "@angular/forms";
import { GettingStartedComponent } from "./getting-started/getting-started.component";
import { IntlTelInputModule } from "angular-intl-tel-input";
import { SignInComponent } from "./sign-in/sign-in.component";
import { VerifyComponent } from "./verify/verify.component";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";

import { SharedModule } from "../shared/shared.module";

const routes: Routes = [
  { path: "sign-up", component: SignUpComponent },
  { path: "", pathMatch: "full", redirectTo: "sign-in" },
  { path: "sign-in", component: SignInComponent },
  { path: "getting-started", component: GettingStartedComponent },
  { path: "verify", component: VerifyComponent },
  { path: "forgotpassword", component: ForgotPasswordComponent }
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),

    ReactiveFormsModule,
    SharedModule,
    IntlTelInputModule,
    FormsModule
  ],
  declarations: [
    SignUpComponent,
    GettingStartedComponent,
    SignInComponent,
    VerifyComponent,
    ForgotPasswordComponent
  ],
  exports: [RouterModule]
})
export class AuthModule {}
