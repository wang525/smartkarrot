import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ɵConsole
} from "@angular/core";
import { stepData } from "../../referral-details/referral-data";

@Component({
  selector: "app-rule-config",
  templateUrl: "./rule-config.component.html",
  styleUrls: ["./rule-config.component.scss"]
})
export class RuleConfigComponent implements OnInit {
  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;

  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();
  @Output() draftRefData = new EventEmitter();
  @Output() attributes = new EventEmitter();
  @Output() editRefData = new EventEmitter();

  private incdecNum = 5;
  private possibleAttrValues = {};

  isShowSetEvent = false;

  gamificationRuleData = [0];
  textNotificationData = [0];
  inAppNotificaiotnData = [0];

  constructor() {}

  ngOnInit() {
    console.log(this.referralData);
    console.log(this.possibleAttr);
  }

  changeNextStep() {
    this.changeStepStatus.emit(stepData.STEP2_MESSAGE);
  }

  changeBackSetp() {
    this.changeStepStatus.emit(stepData.STEP0_AUDIENCE);
  }

  saveAsDraft() {
    this.draftRefData.emit();
  }

  saveData() {
    this.saveRefData.emit();
  }
  attributedata() {
    console.log("coming here");
    this.attributes.emit();
  }

  setEventValue(selectedEvent) {
    console.log(
      "selectedEvent",
      this.referralData["ruleConfiguration"]["referralSuccessfullOn"]
    );
  }

  clickEvents() {
    console.log("click events");
    this.isShowSetEvent = true;
  }

  gamificationRuleFunc(dir) {
    if (dir == 1) {
      this.gamificationRuleData.push(0);
    } else {
      this.gamificationRuleData.splice(this.gamificationRuleData.length - 1, 1);
    }
  }

  textNotificationFunc(dir) {
    if (dir == 1) {
      this.textNotificationData.push(0);
    } else {
      this.textNotificationData.splice(this.textNotificationData.length - 1, 1);
    }
  }

  inAppNotificaiotnFunc(dir) {
    if (dir == 1) {
      this.inAppNotificaiotnData.push(0);
    } else {
      this.inAppNotificaiotnData.splice(
        this.inAppNotificaiotnData.length - 1,
        1
      );
    }
  }

  incDecNumber(value, dir) {
    let res: Number;
    if (dir == 1) {
      res = parseInt(value) + 1;
    } else {
      res = parseInt(value) - 1;
    }
    this.referralData["ruleConfiguration"]["referralConversionOn"] = res;
  }

  editReferral() {
    this.editRefData.emit();
  }
}
