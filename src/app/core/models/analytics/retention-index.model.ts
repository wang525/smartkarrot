export interface RetentionIndexModel {
  "unit": string,
  "cohortAnalysisModels": [
    {
      "dateRange": string,
      "totalUsers": number,
      "retentions": number[]
    }
  ]
}
