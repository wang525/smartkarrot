import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "../../core/service/common/common.service";
import { AuthService } from "../../auth/services/auth.service";
import { EmailValidator } from "@angular/forms";
import { StorageService } from "../../auth/services/storage.service";
import { journeyModel } from "../journey/journey.model";
const emailREGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
declare var $: any;
@Component({
  selector: "app-notifications",
  templateUrl: "./notifications.component.html",
  styleUrls: ["./notifications.component.scss"]
})
export class NotificationsComponent implements OnInit {
  journeyModel: journeyModel = {
    emailId: "",
    deviceId: ""
  };
  headName = [{ name: "Notifications", url: "/notifications", active: true }];
  form: FormGroup;
  constructor(
    public commonService: CommonService,
    private router: Router,
    private authService: AuthService,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    /* set header name to the header bar */
    this.commonService.setHeaderName(this.headName);
  }

  requestInvite(email) {
    try {
      this.journeyModel.emailId = email;
      this.journeyModel.deviceId = this.storageService.generateUUID();
      this.authService.requestInvite(this.journeyModel).subscribe(
        data => {
          const status = data["status"];
          switch (status) {
            case "INVALID_INPUT":
              alert(status);
              break;
            case "INVALID_PASSWORD":
              alert(status);
              break;
            case "USER_NOT_FOUND":
              alert(status);
              break;
            case "ERROR":
              alert("Something went wrong");
              break;
            case "SUCCESS":
              this.storageService.setCurrentUser(data);
              $("#sucessModal").modal("show");
              break;
            default:
              alert("Something went wrong");
          }
        },
        error => {
          console.log(error);
        }
      );
    } catch (error) {}
  }
}
