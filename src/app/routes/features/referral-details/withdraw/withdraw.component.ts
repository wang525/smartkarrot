import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { stepData } from "../../referral-details/referral-data";

@Component({
  selector: "app-withdraw",
  templateUrl: "./withdraw.component.html",
  styleUrls: ["./withdraw.component.scss"]
})
export class WithdrawComponent implements OnInit {
  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;

  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();
  @Output() draftRefData = new EventEmitter();

  constructor() {}

  ngOnInit() {}

  /* change the step status */
  changeBackSetp() {
    this.changeStepStatus.emit(stepData.STEP4_REFERRAL_COUNT);
  }

  changeNextStep() {
    this.changeStepStatus.emit(stepData.STEP6_ADD_COUNT);
  }

  saveData() {
    this.saveRefData.emit();
  }

  saveAsDraft() {
    this.draftRefData.emit();
  }
}
