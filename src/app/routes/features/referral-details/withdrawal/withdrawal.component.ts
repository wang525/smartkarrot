import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { stepData } from '../../referral-details/referral-data';


@Component({
  selector: 'app-withdrawal',
  templateUrl: './withdrawal.component.html',
  styleUrls: ['./withdrawal.component.scss']
})
export class WithdrawalComponent implements OnInit {

  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;
  
  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  /* change the step status */
  changeBackSetp() {
    this.changeStepStatus.emit(stepData.STEP6_ADD_COUNT);
  }

  changeNextStep () {
    this.changeStepStatus.emit(stepData.STEP8_ERROR_SCREEN);
  }

  saveData(){
    this.saveRefData.emit();
  }
}
