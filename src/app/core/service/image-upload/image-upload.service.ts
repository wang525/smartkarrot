import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../../environments/environment';
import { of, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageUploadService {
  constructor(private http: HttpClient) {}

  getApiUrl() {
    return environment.baseApiUrlImage + 'dashboard/';
  }

  getImageLoadUrl() {
    console.log(environment.baseApiUrlImageLoad);
    return environment.baseApiUrlImageLoad;
  }

  uploadImageUrl(url, file) {
    return new Promise(function(resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.open('PUT', url);
      xhr.onload = function() {
        if (xhr.readyState == 4 && xhr.status == 200) {
          console.log('Upload Successful');
          resolve('Success');
        } else {
          console.log('Upload Failed');
          reject('Fail');
        }
      };
      xhr.send(file);
    });
  }

  getImageUrl() {
    return this.http.post(this.getApiUrl(), {
      appId: '4ed062f1-fc38-4f32-a714-b63dc5399626'
    });
  }

  getImage(url) {
    return this.http.get(url);
  }

  onFileChange(event) {
    return event.target.files[0];
  }
}
