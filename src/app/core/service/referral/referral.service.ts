import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../../environments/environment";
import { StorageService } from "../../../auth/services/storage.service";

@Injectable({
  providedIn: "root"
})
export class ReferralService {
  resSaveReferrals: any;

  possibleAttrValues: any;

  listReferral: any;

  referralIdStauts: any;

  getSingleReferral: any;

  updateSingleReferral: any;

  deleteReferralRule: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {}

  /* API */
  getBaseApiUrl() {
    return environment.baseApiUrlReferral;
  }

  getApiUrl(subPath) {
    return environment.baseApiUrlReferral + subPath;
  }

  getApiKey() {
    return environment.apiKey;
  }

  getApiValue() {
    return environment.apiValue;
  }

  /* send save new referral data */
  sendSaveReferrals(sendData: any) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("referral/createReferralRule"), sendData)
        .subscribe(
          res => {
            this.resSaveReferrals = res["status"];
            resolve(this.resSaveReferrals);
          },
          err => {
            console.log("Send Save Segments Data Error");
            resolve(this.resSaveReferrals);
          }
        );
    });
  }

  /* get possible attribute values */
  getPossibleAttrValues(appId) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("referral/listPossibleAttributeValues"), {
          appId: appId
        })
        .subscribe(
          res => {
            this.possibleAttrValues = res;
            resolve(this.possibleAttrValues);
          },
          err => {
            console.log("Get Possible Attribute Value Error");
            resolve(this.possibleAttrValues);
          }
        );
    });
  }

  /* get referral list */
  getListReferral() {
    console.log(this.storageService.getCurrentUser().user.organizationId);
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("referral/listAllreferralRule"), {
          orgId: this.storageService.getCurrentUser().user.organizationId
        })
        .subscribe(
          res => {
            this.listReferral = res;
            console.log(res);
            resolve(this.listReferral);
          },
          err => {
            console.log("List User Referral Error");

            resolve(this.listReferral);
          }
        );
    });
  }

  /* get referral list */
  putReferralIdStatus(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("referral/updateReferralRuleStatus"), data)
        .subscribe(
          res => {
            this.referralIdStauts = res;

            console.log("referral ID status change is ", this.referralIdStauts);
            resolve(this.referralIdStauts);
          },
          err => {
            console.log("referral ID status change is Error");
            resolve(this.referralIdStauts);
          }
        );
    });
  }

  /* get single referral rule */
  getSingleReferralRule(id) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("referral/fetchReferralRule"), id)
        .subscribe(
          res => {
            this.getSingleReferral = res;
            resolve(this.getSingleReferral);
          },
          err => {
            console.log("get single referral rule error");

            resolve(this.getSingleReferral);
          }
        );
    });
  }

  /* update Referral Rule */
  updateReferralRule(data) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("referral/updateReferralRule"), data)
        .subscribe(
          res => {
            this.updateSingleReferral = res;

            console.log("update single referral rule success", res);
            resolve(this.updateSingleReferral);
          },
          err => {
            console.log("update single referral rule error");

            resolve(this.updateSingleReferral);
          }
        );
    });
  }

  /* delete referral Rule */
  deleteReferralRul(id) {
    return new Promise((resolve, reject) => {
      this.http
        .post(this.getApiUrl("referral/deleteReferralRule"), id)
        .subscribe(
          res => {
            this.deleteReferralRule = res;

            console.log("referral Rule delete is success ");
            resolve(this.deleteReferralRule);
          },
          err => {
            console.log("referral Rule delete id error");
            resolve(this.deleteReferralRule);
          }
        );
    });
  }
}
