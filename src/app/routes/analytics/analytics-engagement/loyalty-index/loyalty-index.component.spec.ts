import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoyaltyIndexComponent } from './loyalty-index.component';

describe('LoyaltyIndexComponent', () => {
  let component: LoyaltyIndexComponent;
  let fixture: ComponentFixture<LoyaltyIndexComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoyaltyIndexComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoyaltyIndexComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
