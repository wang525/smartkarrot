/// <reference types="@types/googlemaps" />
import { Component, OnChanges, Input, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-realtime-map-chart',
  templateUrl: './realtime-map-chart.component.html',
  styleUrls: ['./realtime-map-chart.component.scss']
})

export class RealtimeMapChartComponent implements OnChanges {

  @ViewChild('gmap')
  private gMap: ElementRef;

  @Input()
  data: any;

  map: google.maps.Map;
  geocoder: google.maps.Geocoder;

  constructor() {
    this.geocoder = new google.maps.Geocoder();
  }

  ngOnChanges(): void {
    if (!this.data) { return; }

    var mapProp = {
      center: new google.maps.LatLng(52.519325, 13.392709),
      zoom: 1,
      minZoom: 1,
      zoomControl: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gMap.nativeElement, mapProp);
    this.setActiveLocations();
  }

  setActiveLocations() {
    this.data.activeLocations.map((d) => {
      this.geocoder.geocode({ 'address': d }, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
          var marker = new google.maps.Marker({
              map: this.map,
              position: results[0].geometry.location
              // position: new google.maps.LatLng(51.1626, 4.9908)
          });
          // var marker2 = new google.maps.Marker({
          //     map: this.map,
          //     position: results[0].geometry.location
          //     position: new google.maps.LatLng(30.2672, 97.7431)
          // });
        } else {
          console.log("Geocode unsuccessful");
        }
      });
    });
  }
}
