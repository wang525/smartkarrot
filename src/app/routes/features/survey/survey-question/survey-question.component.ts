import { Component, OnInit, Input, QueryList, ViewChildren, Output, EventEmitter } from '@angular/core';
import { QuestionComponent } from '../question/question.component';
import { SurveyConstants } from '../survey.constants';
import * as _ from 'lodash';
declare var $;

@Component({
  selector: 'app-survey-question',
  templateUrl: './survey-question.component.html',
  styleUrls: ['./survey-question.component.scss']
})
export class SurveyQuestionComponent implements OnInit {
  showError: boolean;

  constructor() { }

  @Input() questionsList: Array<Object>;
  @Input() surveyMode: string;
  @Input() surveyName: string;
  @Output() surveyNameChange = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() save = new EventEmitter();
  @Input() isWebApp;
  @ViewChildren(QuestionComponent) questions: QueryList<QuestionComponent>

  isDraft: boolean = false;
  questionSelected: object = {};
  questionError: boolean = false;

  ngOnInit() {
    this.updateQuestionId();
    this.updateFollowUpQuestionId();
    this.questionSelected = this.questionsList[0];
  }

  updateQuestionId() {
    this.questionsList.map((ques, index) => {
      ques['qid'] = index + 1001;
    })
  }

  updateFollowUpQuestionId() {
    this.questionsList.map((ques: any) => {
      if (ques.followUp && ques.followUp.length) {
        ques.followUp.map((followupQues, index) => {
          followupQues['qid'] = index + 101;
        })
      }
    })
  }
  removeFollowQuestion(question, followQuestion) {
    let index = question.followUp.indexOf(followQuestion);
    question.followUp.splice(index, 1);
    //this.questionSelected = this.questionsList[0];
    this.updateFollowUpQuestionId();
  }

  removeQuestion(question) {
    let index = this.questionsList.indexOf(question);
    this.questionsList.splice(index, 1);
    this.questionSelected = this.questionsList[0];
    this.updateQuestionId();
  }

  onQuestionChange(event) {
    this.questionsList = this.questionsList.map((ques: any) => {
      if (ques.qid == event.qid)
        ques = Object.assign({}, ques, event)
      return ques;
    });
    this.questionSelected = event;
  }

  selectQuestion(question) {
    this.checkEmptyQuestion();
    if (!this.questionError)
      this.questionSelected = _.cloneDeep(question);
  }

  addNewQuestion() {
    this.checkEmptyQuestion();
    if (!this.questionError) {
      let newQuestionObj = Object.assign({}, _.cloneDeep(SurveyConstants.defaultQuestion), { qid: this.questionsList.length + 1001 });
      this.questionsList.push(newQuestionObj);
      this.onQuestionChange(newQuestionObj);
    }
  }

  checkEmptyQuestion() {
    this.questionError = !this.questionSelected['question'] || (/^ *$/.test(this.questionSelected['question'])) ? true : false;
    this.questionSelected['followUp'] && this.questionSelected['followUp'].map(fques => {
      if (!fques.question || (/^ *$/.test(fques.question)))
        this.questionError = true;
    });
  }

  saveSurvey() {
    this.showError = !this.surveyName || (/^ *$/.test(this.surveyName)) ? true : false;
    !this.showError && this.save.emit();
  }

  cancelSurvey() {
    this.cancel.emit();
  }

  customTrackBy(index: number, obj: any): any {
    return index;
  }

  onNameChange(event) {
    this.showError = !event ? true : false;
    this.surveyNameChange.emit(event);
  }

  @Output() tabSelectedChange = new EventEmitter();
  pageChange(event) {
    this.tabSelectedChange.emit(event)
  }

}
