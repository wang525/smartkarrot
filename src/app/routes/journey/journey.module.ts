import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { JourneyComponent } from "./journey.component";
import { TypeaheadModule } from "ngx-bootstrap/typeahead";
import { FormsModule } from "@angular/forms";

const routes: Routes = [{ path: "", component: JourneyComponent }];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    TypeaheadModule.forRoot(),
    FormsModule
  ],

  declarations: [JourneyComponent],
  exports: [RouterModule, TypeaheadModule]
})
export class JourneyModule {}
