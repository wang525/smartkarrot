import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges } from '@angular/core';
import { SurveyConstants } from '../survey.constants';
import * as _ from 'lodash';
import { ImageUploadService } from '../../../../core/service/image-upload/image-upload.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-rating-options',
  templateUrl: './rating-options.component.html',
  styleUrls: ['./rating-options.component.scss'],
  providers: [ImageUploadService]
})
export class RatingOptionsComponent implements OnInit {

  @Input() ratingDetails: { inputType: string, question: string, imageUrl: any, options: Array<string>, modelSelected: any, smileyNumber: string, removedSmiley: string, followUp?: Array<object> };
  @Output() onRatingDetailsChange = new EventEmitter();
  ratingOptions = SurveyConstants.surveyInputs;
  isFollowup: boolean = false;
  constructor(
    private imageUploadSrvc: ImageUploadService,
    private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    if (this.ratingDetails) {
      this.isFollowup = this.ratingDetails.hasOwnProperty('followUp') && !this.ratingDetails.hasOwnProperty('followUpQuestionId') ? false : true;
      this.ratingDetails.modelSelected = this.updateModelSelected(this.ratingDetails)
      this.ratingDetails.smileyNumber = this.ratingDetails.options.length == 4 ? 'four' : 'three';
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.ratingDetails && !changes.ratingDetails.isFirstChange()) {
      this.isFollowup = this.ratingDetails.hasOwnProperty('followUp') && !this.ratingDetails.hasOwnProperty('followUpQuestionId') ? false : true;
      this.ratingDetails.modelSelected = this.updateModelSelected(this.ratingDetails);
      this.ratingDetails.smileyNumber = this.ratingDetails.options.length == 4 ? 'four' : 'three';
    }
  }

  updateModelSelected(ratingDetails) {
    let modelSelected = _.filter(this.ratingOptions, { inputType: ratingDetails.inputType })[0];
    if (modelSelected) {
      this.updateImage(ratingDetails);
      return Object.assign({}, modelSelected, ratingDetails)['id'];
    }
  }

  updateImage(model) {
    if (model.imageUrl)
      this.ratingDetails.imageUrl = model.imageUrl
  }

  emitRatingModel() {
    this.onRatingDetailsChange.emit(this.ratingDetails);
  }

  setValue(value) {
    this.ratingDetails = Object.assign(this.ratingDetails, {
      inputType: value.inputType,
      // question: value.question,
      // imageUrl: value.imageUrl,
      options: [...[], ...value.options]
    })

    this.emitRatingModel();
  }

  onSelect(event) {
    this.setValue(_.filter(this.ratingOptions, { id: event })[0]);
  }

  removeOption(option) {
    this.ratingDetails.options.splice(this.ratingDetails.options.indexOf(option), 1);
  }

  addOption() {
    this.ratingDetails.options.push('')
  }

  deleteImage() {

  }

  trimData(question) {
    return (/^ *$/.test(question))
  }

  valueChange(event) {
    this.emitRatingModel();
  }

  onRadioChange(event) {
    // this.ratingSrvc.setOption('smileyNumber', event);
    this.ratingDetails.smileyNumber = event;
    if (event == 'three') {
      // this.ratingSrvc.setOption('removedSmiley', this.ratingDetails.options.splice(3, 1)[0]);
      this.ratingDetails.removedSmiley = this.ratingDetails.options.splice(3, 1)[0];
    } else {
      this.ratingDetails.options.push(this.ratingDetails.removedSmiley.length ? this.ratingDetails.removedSmiley : "New Value");
    }
    this.emitRatingModel();
  }

  customTrackBy(index: number, obj: any): any {
    return index;
  }

  fileChange(event) {
    let file: File = event.target.files[0];
    if (file['type'] == 'image/jpeg' || file.type == 'image/png')
      this.uploadFile(file);
    else
      alert('Please upload Jpeg or Png format. ')
  }

  uploadFile(file) {
    // this.ratingSrvc.setOption('imageUrl', this._sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file)));
    // this.ratingDetails.imageUrl = this.ratingSrvc.getOption('imageUrl');

    this.ratingDetails.imageUrl = this._sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));

    this.imageUploadSrvc.getImageUrl().subscribe(async (resp: any) => {
      await this.imageUploadSrvc.uploadImageUrl(resp.url, file)
      this.ratingDetails.imageUrl = this.imageUploadSrvc.getImageLoadUrl() + resp.key;
    });
  }
}
