import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ɵConsole
} from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { CommonService } from "../../../core/service/common/common.service";
import { ReferralService } from "../../../core/service/referral/referral.service";
import { ImageUploadService } from "../../../core/service/image-upload/image-upload.service";
import { stepData } from "../referral-details/referral-data";
import { StorageService } from "../../../auth/services/storage.service";
import {
  DateTimeAdapter,
  OWL_DATE_TIME_FORMATS,
  OWL_DATE_TIME_LOCALE
} from "ng-pick-datetime";
// import { MomentDateTimeAdapter } from "ng-pick-datetime-moment";

declare var $;

// export const MY_CUSTOM_FORMATS = {
//   parseInput: "DD-MM-YYYY",
//   fullPickerInput: "DD-MM-YYYY",
//   datePickerInput: "DD-MM-YYYY",
//   timePickerInput: "DD-MM-YYYY",
//   monthYearLabel: "DD-MM-YYYY",
//   dateA11yLabel: "DD-MM-YYYY",
//   monthYearA11yLabel: "DD-MM-YYYY"
// };

@Component({
  selector: "app-referral-details",
  templateUrl: "./referral-details.component.html",
  styleUrls: ["./referral-details.component.scss"]
  // providers: [
  //   {
  //     provide: DateTimeAdapter,
  //     useClass: MomentDateTimeAdapter,
  //     deps: [OWL_DATE_TIME_LOCALE]
  //   },
  //   { provide: OWL_DATE_TIME_FORMATS, useValue: MY_CUSTOM_FORMATS }
  // ]
})
export class ReferralDetailsComponent implements OnInit {
  headName = [
    { name: "Referral", url: "/features/referral-list", active: true }
  ];

  currentStepStatus = 0;
  checkState = { val: false };

  private stepData;

  private referralData = {};
  private possibleAttrValues = {};
  private singleReferral = {};

  /* get path and segment id */
  private path: any;
  private id: any;
  @Input() welcomeData;

  private filter = {
    productApp: ""
  };
  constructor(
    public commonService: CommonService,
    public referralService: ReferralService,
    private router: Router,
    private route: ActivatedRoute,
    private storageService: StorageService
  ) {}

  ngOnInit() {
    this.stepData = stepData;
    this.commonService.setHeaderName(this.headName);

    var date = new Date();
    var curDate =
      date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
    this.referralData = {
      webAppOnly: false,
      programId: "ACCOUNT_REFERRAL_003",
      isDraft: false,
      createdBy: this.storageService.getCurrentUser().user.userId,
      ruleStatus: true,
      appId: this.filter.productApp,
      ruleConfiguration: {
        startDate: curDate.toString(),
        endDate: "",
        referralSuccessfullOn: "",
        referralConversionOn: "5",
        actor1: "Referrer",
        actor2: "Referral",
        callbackActionURL: "https://5tfgrt.xyz.com/axn",
        incentive: {
          enableIncentive: true,
          amountPaidToActor1: 0.99,
          currencyPaidToActor1: "usd",
          amountPaidToActor2: 0.99,
          currencyPaidToActor2: "usd"
        },
        gamification: { enableGamification: false, gamificationRules: [] },
        notification: {
          enableNotification: true,
          textNotification: [],
          inAppNotification: [
            {
              inAppNotificationText: "Welcom Onboard..!!",
              inAppNotificationTo: "actor2"
            }
          ]
        },
        donation: {
          enableDonation: true,
          donationAmount: 0.49,
          donationCurrency: "usd",
          onBehalfOf: "actor1",
          charityName: "qwerty charity"
        }
      },
      screenConfiguration: {
        message: {
          referralMessageText: "Check this awesome app.",
          referralLinkPrefix: "accntNme"
        },
        home: {
          enableEarningScreen: true,
          referralHomeScreen: {
            colorPalatteCode: "#6b8e23",
            applyToAllScreens: false
          },
          referralHomeText: {
            fontStyle: "Arial",
            headerText: "Referral",
            bodyText: "Refer this app to your friends and earn rewards.",
            logoIconURL: "assets/images/referal_icon.png",
            applyToAllScreens: false
          }
        },
        referralCount: {
          enableWithdrawlScreen: true,
          colorPalatteCode: "#6b8e23",
          fontStyle: "Times New Roman"
        },
        withdraw: {
          colorPalatteCode: "#6b8e23",
          fontStyle: "Times New Roman",
          CTAColor: "#6b8e23",
          logoIconURL: "https://aygcknjec.s3.dchkbc.com/dkcnkj"
        },
        addAccount: {
          colorPalatteCode: "#6b8e23",
          CTAColor: "#6b8e23",
          fields: [{ fieldName: "Account Number" }]
        },
        withdrawl: {
          colorPalatteCode: "#6b8e23",
          fontStyle: "Times New Roman",
          withdrawLimitAmount: 10,
          withdrawLimitCurrency: "usd",
          earningsWithdrawlTo: "to_bank_account"
        },
        errorScreen: {
          errorText: "Something went wrong.",
          errorImageURL: "assets/images/error_illus.png"
        }
      },
      techConfiguration: {
        ios: {
          userPhoneHasApp: { iosURIScheme: "AppName://" },
          userPhoneDoesnotHaveApp: { appStoreSearch: "AppName" },
          universalLink: {
            bundleIdentifier: "com.company.appname",
            appleAppPrefix: "app_prefix"
          }
        },
        android: {
          userPhoneHasApp: { androidURIScheme: "android_uri_scheme" },
          userPhoneDoesnotHaveApp: { playStoreSearch: "AndroidAppName" },
          appLink: { sha256CertFingerprint: "sha256_cert" }
        },
        web: { defaultURLWeb: "www.AppName.comn" }
      }
    };

    /* get the referral id of the individual referral */
    this.path = this.route.params.subscribe(params => {
      this.id = params["id"];
    });
    if (this.id) {
      let data = {};
      data["referralRuleId"] = this.id;
      this.referralService.getSingleReferralRule(data).then(res => {
        this.singleReferral = res;
        if (this.singleReferral) {
          this.referralData = this.singleReferral;
          this.referralService
            .getPossibleAttrValues(this.singleReferral["appId"])
            .then(response => {
              this.possibleAttrValues = response;
            });
        }
      });
    }
    if (this.id) {
      this.referralData["programId"] = "";
    }
  }

  setChangedStepStatus(step) {
    this.currentStepStatus = step;
  }

  showPossibleAttr() {
    this.referralService
      .getPossibleAttrValues(this.referralData["appId"])
      .then(res => {
        this.possibleAttrValues = res;
      });
  }

  saveReferralData() {
    var tmpRefData = this.referralData;
    if (
      !tmpRefData ||
      tmpRefData["programId"] == "" ||
      tmpRefData["ruleConfiguration"]["startDate"] == "" ||
      tmpRefData["ruleConfiguration"]["referralSuccessfullOn"] == "" ||
      tmpRefData["ruleConfiguration"]["referralConversionOn"] == "" ||
      tmpRefData["ruleConfiguration"]["actor1"] == "" ||
      tmpRefData["ruleConfiguration"]["actor2"] == "" ||
      tmpRefData["screenConfiguration"]["referralLinkPrefix"] == ""
    ) {
      $("#emptyReferral").trigger("click");
    } else {
      var date = new Date();
      this.referralData["createdAt"] = date.toString();
      var startDate = new Date(tmpRefData["ruleConfiguration"]["startDate"]),
        mnth = ("0" + (startDate.getMonth() + 1)).slice(-2),
        day = ("0" + startDate.getDate()).slice(-2);
      var startSaveDate = [day, mnth, startDate.getFullYear()].join("-");

      var endDate = new Date(tmpRefData["ruleConfiguration"]["endDate"]),
        mnth = ("0" + (endDate.getMonth() + 1)).slice(-2),
        day = ("0" + endDate.getDate()).slice(-2);
      var endSaveDate = [day, mnth, endDate.getFullYear()].join("-");
      tmpRefData["ruleConfiguration"]["startDate"] =
        tmpRefData["ruleConfiguration"]["startDate"];
      tmpRefData["ruleConfiguration"]["endDate"] =
        tmpRefData["ruleConfiguration"]["endDate"];
      if (this.id) {
        //this.referralData['ruleStatus'] = "enabled";
        $("#saveReferral").trigger("click");
      } else {
        this.sendReferralData();
      }
    }
  }

  sendReferralData() {
    this.referralData["isDraft"] = false;
    this.referralData[
      "orgId"
    ] = this.storageService.getCurrentUser().user.organizationId;
    if (this.id) {
      delete this.referralData["createdAt"];
      this.referralService.updateReferralRule(this.referralData).then(res => {
        if (res["status"] == "SUCCESS") {
          console.log("update data success.");
          this.router.navigate(["/features/referral-list"]);
          return;
        } else {
          console.log("update data failed.");
          $("#failSaveSeg").trigger("click");
        }
      });
    } else {
      this.referralService.sendSaveReferrals(this.referralData).then(res => {
        if (res == "SUCCESS") {
          console.log("save data success.", res);
          this.router.navigate(["/features/referral-list"]);
          return;
        } else {
          console.log("save data failed.");
          $("#failSaveSeg").trigger("click");
        }
      });
    }
  }

  /* Save as Draft when click cancel button */
  gotoSaveAsDraft($event) {
    var tmpRefData = this.referralData;
    if (
      !tmpRefData ||
      tmpRefData["programId"] == "" ||
      tmpRefData["ruleConfiguration"]["startDate"] == "" ||
      tmpRefData["ruleConfiguration"]["referralSuccessfullOn"] == "" ||
      tmpRefData["ruleConfiguration"]["referralConversionOn"] == "" ||
      tmpRefData["ruleConfiguration"]["actor1"] == "" ||
      tmpRefData["ruleConfiguration"]["actor2"] == "" ||
      tmpRefData["screenConfiguration"]["referralLinkPrefix"] == ""
    ) {
      alert("Please fill  startdate, Referral Successfull On etc..,");
    } else {
      var date = new Date();
      this.referralData["isDraft"] = true;
      this.referralData["createdAt"] = date.toString();
      tmpRefData["ruleConfiguration"]["startDate"] =
        tmpRefData["ruleConfiguration"]["startDate"];
      tmpRefData["ruleConfiguration"]["endDate"] =
        tmpRefData["ruleConfiguration"]["endDate"];
      this.referralData[
        "orgId"
      ] = this.storageService.getCurrentUser().user.organizationId;
      if (this.id) {
        this.referralData["referralRuleId"] = this.id;
        this.referralService.updateReferralRule(this.referralData).then(res => {
          if (res["status"] == "SUCCESS") {
            console.log("update data success.");
            this.router.navigate(["/features/referral-list"]);
            return;
          } else {
            console.log("update data failed");
            $("#failSaveSeg").trigger("click");
          }
        });
      } else {
        console.log("26444444444444" + JSON.stringify(this.referralData));
        /* create new data as draft*/
        this.referralService.sendSaveReferrals(this.referralData).then(res => {
          if (res == "SUCCESS") {
            console.log("save data success.", res);
            this.router.navigate(["/features/referral-list"]);
            return;
          } else {
            console.log("save data failed.");
            $("#failSaveSeg").trigger("click");
          }
        });
      }
    }
  }
}
