import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingTrendGraphComponent } from './rating-trend-graph.component';

describe('RatingTrendGraphComponent', () => {
  let component: RatingTrendGraphComponent;
  let fixture: ComponentFixture<RatingTrendGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingTrendGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingTrendGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
