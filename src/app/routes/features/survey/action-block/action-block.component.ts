import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-action-block',
  templateUrl: './action-block.component.html',
  styleUrls: ['./action-block.component.scss']
})
export class ActionBlockComponent implements OnInit {
  showError: boolean;

  constructor() { }

  @Input() isWebApp;
  @Output() tabSelectedChange = new EventEmitter();
  @Output() save = new EventEmitter();
  transferList: Array<object> = [];
  textList: Array<object> = [];
  notificationList: Array<object> = [];
  emailList: Array<object> = [];

  ngOnInit() {
    this.transferList = [{
      text: 1
    }]
    this.emailList = [{}];
    this.notificationList = [{}]
    this.textList = [{}]
  }

  pageChange(event) {
    this.tabSelectedChange.emit(event)
  }

  onSave() {
    this.showError = !event ? true : false;
    this.save.emit();
  }

  customTrackBy(index: number, obj: any): any {
    return index;
  }

  addMoreTransfer() {
    this.transferList.push({ text: 1 })
  }
  removeTransfer(transfer) {
    this.transferList.splice(this.transferList.indexOf(transfer), 1);
  }

  addMoreText() {
    this.textList.push({})
  }
  removeText(text) {
    this.textList.splice(this.textList.indexOf(text), 1);
  }

  addMoreNotification() {
    this.notificationList.push({})
  }
  removeNotification(notification) {
    this.notificationList.splice(this.notificationList.indexOf(notification), 1);
  }

  addMoreEmail() {
    this.emailList.push({})
  }
  removeEmail(email) {
    this.emailList.splice(this.emailList.indexOf(email), 1);
  }

}
