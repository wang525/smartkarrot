export class UserAttr
{
    constructor(
        public name : String,         
        public type : String          // Number, String, StringSet
    ) {}
};

export class DeviceAttr
{
    constructor(
        public name : String,         
        public type : String          // Number, String, StringSet
    ) {}
};

export class AppEvent
{
    constructor(
        public name : String,         
        public attributes : any[]          // Number, String, StringSet
    ) {}
};

export class ParamInfo
{
    constructor(
        public name : String,       /* From, To, ...*/
        public connector : String   /* To */
    ){}
};

export class OperatorData
{
    constructor(
        public operator : String,   /* */
        public name : String,       /* */
        public count : Number,       /* count of values */
        public param_info : Array<ParamInfo>
    ){}
};

export class TypeData
{
    constructor(
        public type: String,
        public operators: Array<OperatorData>,
        public selectable : boolean,
        public options : any[],
        public unit : String        /* Years, Times, ... */
    ) {}
};

export class Tab
{
    constructor(
        public id : Number,
        public name : String
    ) {}
};

export class FilterData
{
    public static LOGIC_NONE = 0;
    public static LOGIC_SELECTABLE = 1;
    public static LOGIC_AND = 2;
    public static LOGIC_NOT = 3;

    public static TAB_USER_ATTR = 0;
    public static TAB_DEVICE_ATTR = 1;
    public static TAB_APP_EVENT_ATTR = 2;

    constructor(
        public attr_kind : Number,          // one of TAB_USER_ATTR, TAB_DEVICE_ATTR, TAB_APP_EVENT
        public attr_name : String,          // name of UserAttr, DeviceAttr, AppEvent
        public attr_type : String,          // Number, String, StringSet
        public attr_cond : String,          // String for app events
        public attr_dura : String,          // String for app events for duraton
        public has_attr : boolean,          // has attribute for app events 
        public used_attr : boolean,         // used for attributes for app events
        public cnt_times : String,          // times for app events
        public cnt_days : String,           // days for app events
        public event_attr_name : String,    // event attributes name
        public event_attr_type : String,    // event attributes type
        public event_attr_value : String,   // event attributes value
        public operator : String,           // operator of types. depends on attr_type. only Used when attr_type is Number
        public params : Array<any>=[],                                 // value of operator. depends on operator. 
        public logic_status: Number         // Can be LOGIC_NON, ... LOGIC_NOT
    
    ) {}
};
