import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { stepData } from "../../referral-details/referral-data";

@Component({
  selector: "app-referral-count",
  templateUrl: "./referral-count.component.html",
  styleUrls: ["./referral-count.component.scss"]
})
export class ReferralCountComponent implements OnInit {
  @Input() stepStatus: Number;
  @Input() possibleAttr: Object;
  @Input() referralData: Object;
  @Input() applyAll;

  @Output() changeStepStatus = new EventEmitter();
  @Output() saveRefData = new EventEmitter();
  @Output() applySelected = new EventEmitter();
  @Output() draftRefData = new EventEmitter();
  @Output() editRefData = new EventEmitter();

  refCountData: Object;
  constructor() {}

  ngOnInit() {
    console.log(this.referralData);
    this.refCountData = this.referralData["screenConfiguration"].referralCount;
  }

  /* change the step status */
  changeBackSetp() {
    this.changeStepStatus.emit(stepData.STEP3_HOME);
  }

  changeNextStep() {
    this.changeStepStatus.emit(stepData.STEP8_ERROR_SCREEN);
  }

  saveData() {
    this.saveRefData.emit();
  }
  saveAsDraft() {
    this.draftRefData.emit();
  }

  onApplySelected(event) {
    this.applySelected.emit(event);
  }

  editReferral() {
    this.editRefData.emit();
  }
}
