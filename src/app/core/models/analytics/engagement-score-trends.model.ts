export interface EngagementScoreTrendsModel {
  "unit": string,
  "scores": [{
    "daysBeforeToday": any,
    "score": number
  }]
}
