import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyticsComponent } from './analytics-home/analytics-home.component';
import { Routes, RouterModule } from "@angular/router";
import { AnalyticsHeaderComponent } from './analytics-header/analytics-header.component';
import { FormsModule } from '@angular/forms';
import { AnalyticsEngagementComponent } from './analytics-engagement/analytics-engagement.component';
import { DurationIndexComponent } from './analytics-engagement/duration-index/duration-index.component';
import { LoyaltyIndexComponent } from './analytics-engagement/loyalty-index/loyalty-index.component';
import { RetentionIndexComponent } from './analytics-engagement/retention-index/retention-index.component';
import { FeedbackIndexComponent } from './analytics-engagement/feedback-index/feedback-index.component';
import { EngagementHomeComponent } from './analytics-engagement/engagement-home/engagement-home.component';
import { RatingTrendGraphComponent } from './analytics-engagement/feedback-index/rating-trend-graph/rating-trend-graph.component';
import { ScorePieChartComponent } from './analytics-engagement/engagement-home/score-pie-chart/score-pie-chart.component';
import { ScoreTrendChartComponent } from './analytics-engagement/engagement-home/score-trend-chart/score-trend-chart.component';
import { RealtimeChartComponent } from './analytics-engagement/engagement-home/realtime-chart/realtime-chart.component';
import { LoyatyIndexChartComponent } from './analytics-engagement/loyalty-index/loyaty-index-chart/loyaty-index-chart.component';
import { DurationIndexChartComponent } from './analytics-engagement/duration-index/duration-index-chart/duration-index-chart.component';
import { RatingSidebarWidgetComponent } from './analytics-engagement/feedback-index/rating-sidebar-widget/rating-sidebar-widget.component';
import { RetentionBarChartComponent } from './analytics-engagement/retention-index/retention-bar-chart/retention-bar-chart.component';
import { EngScoreRelativeComponent } from './analytics-engagement/eng-score-relative/eng-score-relative.component';
import { IndicesTrendComponent } from './analytics-engagement/indices-trend/indices-trend.component';
import { RealtimeEngagementComponent } from './analytics-engagement/realtime-engagement/realtime-engagement.component';
import { IndicesLineChartComponent } from './analytics-engagement/indices-trend/indices-line-chart/indices-line-chart.component';
import { RealtimeMapChartComponent } from './analytics-engagement/realtime-engagement/realtime-map-chart/realtime-map-chart.component';

const routes: Routes = [
  { path: "", component: AnalyticsComponent, 
    children: [
    { path: "", pathMatch: "full", redirectTo: 'engagement' },
    { path: "engagement", component: AnalyticsEngagementComponent,
      children: [
        { path: "", pathMatch: "full", redirectTo: 'overview' },
        { path: "overview", component: EngagementHomeComponent },
        { path: "relative-performance", component: EngScoreRelativeComponent },
        { path: "indices-trend", component: IndicesTrendComponent },
        { path: "realtime-engagement", component: RealtimeEngagementComponent },
        { path: "duration-index", component: DurationIndexComponent },
        { path: "loyalty-index", component: LoyaltyIndexComponent },
        { path: "retention-index", component: RetentionIndexComponent },
        { path: "feedback-index", component: FeedbackIndexComponent }
      ]
    },
    { path: "", component: AnalyticsEngagementComponent, 
      children: [
        { path: "", component: EngagementHomeComponent, pathMatch: "full" }
      ]
  },]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule
  ],

  declarations: [
    AnalyticsComponent, 
    AnalyticsHeaderComponent, 
    AnalyticsEngagementComponent, 
    DurationIndexComponent, 
    LoyaltyIndexComponent, 
    RetentionIndexComponent, 
    FeedbackIndexComponent, 
    EngagementHomeComponent, 
    RatingTrendGraphComponent, 
    ScorePieChartComponent, 
    ScoreTrendChartComponent, 
    RealtimeChartComponent, 
    LoyatyIndexChartComponent, 
    DurationIndexChartComponent, 
    RatingSidebarWidgetComponent, 
    RetentionBarChartComponent, 
    EngScoreRelativeComponent, 
    IndicesTrendComponent, 
    RealtimeEngagementComponent, 
    IndicesLineChartComponent, 
    RealtimeMapChartComponent],
  exports: [RouterModule]
})
export class AnalyticsModule { }
