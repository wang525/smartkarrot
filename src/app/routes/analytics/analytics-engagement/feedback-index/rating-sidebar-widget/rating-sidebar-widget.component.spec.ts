import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RatingSidebarWidgetComponent } from './rating-sidebar-widget.component';

describe('RatingSidebarWidgetComponent', () => {
  let component: RatingSidebarWidgetComponent;
  let fixture: ComponentFixture<RatingSidebarWidgetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RatingSidebarWidgetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RatingSidebarWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
