import { Component, OnInit, AfterViewInit } from "@angular/core";
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators
} from "@angular/forms";
import { AuthService } from "../services/auth.service";
import { StorageService } from "../services/storage.service";
import { Router } from "@angular/router";
declare var $: any;
import * as googleLibphonenumber from "google-libphonenumber";
import { phoneNoValidation } from "./custom-validator-phone";
const emailREGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
export class User {
  name: string;
  emailId: string;
  phoneNumber: string;
  callingCode: string;
  organizationName: string;
  deviceId: string;

  public constructor(init?: Partial<User>) {
    Object.assign(this, init);
  }
}
@Component({
  selector: "app-sign-up",
  templateUrl: "./sign-up.component.html",
  styleUrls: ["./sign-up.component.scss"]
})
export class SignUpComponent implements OnInit, AfterViewInit {
  errorMsg = "";
  form: FormGroup;
  user: User;
  loadingGif: string = "../assets/images/loading.gif";
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private storageService: StorageService,
    private router: Router
  ) {
    $(window).on("load", function() {
      $("#SignUp").modal("show");
    });
  }

  ngAfterViewInit(): void {
    $("body").click();
  }

  ngOnInit() {
    $("#SignUp").modal({ backdrop: "static", keyboard: false });
    // console.log(this.generateUUID());
    $("#SignUp").modal("show");
    this.form = this.formBuilder.group({
      name: [null, [Validators.required]],
      emailId: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(emailREGEX)
        ])
      ],
      // phoneNumber: [null, [Validators.required, phoneNoValidation]],
      // organizationName: [null, Validators.required],
      termsOfUse: [null, Validators.required],
      phoneNumber: [null, [phoneNoValidation]],
      organizationName: [null],
      deviceId: [null]
    });
    // alert('erntrnm');
    console.log(this.phoneNumberValidation("+1958075951"));
  }

  get mobile() {
    //   console.log(this.form.get('phoneNumber'));
    return this.form.get("phoneNumber");
  }

  phoneNumberValidation(number) {
    const phoneUtil = googleLibphonenumber.PhoneNumberUtil.getInstance();
    const phoneNumber = phoneUtil.parse(number);
    console.log(phoneNumber.getCountryCode(), phoneNumber.getNationalNumber());
    return {
      country: phoneNumber.getCountryCode(),
      phone: phoneNumber.getNationalNumber()
    };
  }

  cancel() {
    this.form.reset();
  }

  onSubmit(model: User, form) {
    console.log(form);
    this.errorMsg = "";
    try {
      console.log(this.form.value);
      if (this.form.value.phoneNumber) {
        this.form.value.callingCode = this.form.value.phoneNumber.substring(
          0,
          this.form.value.phoneNumber.length - 10
        );
        this.form.value.phoneNumber = this.form.value.phoneNumber.substring(
          this.form.value.phoneNumber.length - 10,
          this.form.value.phoneNumber.length
        );
      }
      this.form.value.deviceId = this.storageService.generateUUID();
      this.user = new User(this.form.value);
      console.log(this.user);
      $("#loadingicon").modal("show");
      this.authService.signUp(this.user).subscribe(
        data => {
          console.log(JSON.stringify(data));
          if (data) {
            if (data["status"]) {
              const status = data["status"];
              switch (status) {
                case "USER_ALREADY_EXISTS":
                  $("#loadingicon").modal("hide");
                  alert(status);
                  break;
                case "INVALID_INPUT":
                  $("#loadingicon").modal("hide");
                  alert(status);
                  break;
                case "ERROR":
                  $("#loadingicon").modal("hide");
                  alert("Something went wrong");
                  break;
                case "SUCCESS":
                  $("#SignUp").modal("hide");
                  $("#loadingicon").modal("hide");
                  this.storageService.setCurrentUser(data);
                  $("#SignUppwdsent").modal({
                    backdrop: "static",
                    keyboard: false
                  });
                  break;
                default:
                  alert("Something went wrong");
              }
            } else {
              console.log("no status");
            }
          } else {
            console.log("response is null");
          }
        },
        error => {
          console.log(error);
        }
      );
    } catch (e) {
      console.log(e);
      this.errorMsg = "Please enter valid phone number";
    }
  }

  close() {
    this.router.navigate(["auth/sign-in"]);
  }
}
