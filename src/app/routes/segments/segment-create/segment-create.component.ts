import { Component, OnInit } from "@angular/core";

declare var jquery: any;
declare var $: any;

@Component({
  selector: "app-segment-create",
  templateUrl: "./segment-create.component.html",
  styleUrls: ["./segment-create.component.scss"]
})
export class SegmentCreateComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  showHowToAddAttributes($event) {
    $("#add-new-attribute-Widget").css("display", "block");
  }
}
