import { Component, ElementRef, Input, OnChanges, ViewChild, ViewEncapsulation } from '@angular/core';
import * as d3 from 'd3';
import { FeedbackIndexTrendingModel, FeedbackIndexTrendingModel2 } from '../../../../../core/models/analytics/feedback-index.model';

@Component({
  selector: 'app-rating-trend-graph',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './rating-trend-graph.component.html',
  styleUrls: ['./rating-trend-graph.component.scss']
})

export class RatingTrendGraphComponent implements OnChanges {

  @ViewChild('chart') private chartContainer: ElementRef;

  @Input() data: FeedbackIndexTrendingModel;

  margin = { top: 20, right: 20, bottom: 50, left: 40 };

  constructor() { }

  ngOnChanges(): void {
    if (!this.data) { return; }


    this.createChart();
  }
  onResize() {
    this.createChart();
  }

  private createChart(): void {
    d3.select('svg').remove();

    const element = this.chartContainer.nativeElement;

    const dataa = this.data['iOSRatings'];

    const data = [];
    for (var i in dataa) {
      data.push(
          {
             date: new Date(dataa.date), //date
             value: +dataa.average //convert string to number
          });
    }

  


    

    //   const svg = d3.select(element).append('svg')
    // 	  .attr('width', element.offsetWidth)
    // 	//   .attr('height', element.offsetHeight
    // 	  .attr('height', 400
    // 	  );

      const width = element.offsetWidth - this.margin.left - this.margin.right;
      let height = (element.offsetHeight ? element.offsetHeight :340);
      // height = 300;
      //const height = (element.offsetHeight ? element.offsetHeight :250) - this.margin.top - this.margin.bottom;

    var height2 = height - this.margin.top - this.margin.bottom;

    

    d3.select(element).select('svg').remove();

          var svg = d3.select(element).append("svg")
            .attr("width", width)
            .attr("height", height);

            var g = svg.append("g")
            .attr("transform", 
               "translate(" + this.margin.left + "," + this.margin.top + ")"
            );

            var x = d3.scaleTime().rangeRound([0, width]);
            var y = d3.scaleLinear().domain([5,0]).range([height2,0]);


            var line = d3.line<FeedbackIndexTrendingModel2>()
                .x(function(d) { return x(d.date)})
                .y(function(d) { return y(d.value)})
                x.domain(d3.extent(data, function(d) { return d.date }));
                y.domain(d3.extent(data, function(d,i) { return (i =(i >5) ? 5 : i) }));

              g.append("g")
              .attr("transform", "translate(0," + height2 + ")")
              .call(d3.axisBottom(x).tickFormat(d3.timeFormat("%b %d")));
              // .select(".domain")
              // .remove();

              g.append("g")
              .call(d3.axisLeft(y))
              .append("text")
              .attr("fill", "#000")
              .attr("transform", "rotate(-90)")
              .attr("y", 6)
              .attr("dy", "0.71em")
              .attr("text-anchor", "end")
              .text("Ratings");

              g.append("path")
              .datum(data)
              .attr("fill", "none")
              .attr("stroke", "steelblue")
              .attr("stroke-linejoin", "round")
              .attr("stroke-linecap", "round")
              .attr("stroke-width", 1.5)
              .attr("d", line);
  }
}
