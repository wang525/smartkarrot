import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScorePieChartComponent } from './score-pie-chart.component';

describe('ScorePieChartComponent', () => {
  let component: ScorePieChartComponent;
  let fixture: ComponentFixture<ScorePieChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScorePieChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScorePieChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
