import { LayoutComponent } from "../layout/layout.component";

export const routes = [
  { path: "", pathMatch: "full", redirectTo: "auth" },
  {
    path: "auth",
    loadChildren: "../auth/auth.module#AuthModule"
  },
  {
    path: "",
    component: LayoutComponent,
    children: [
      // { path: "", redirectTo: "home", pathMatch: "full" },
      { path: "home", loadChildren: "./home/home.module#HomeModule" },
      {
        path: "segments",
        loadChildren: "./segments/segments.module#SegmentsModule"
      },
      {
        path: "analytics",
        loadChildren: "./analytics/analytics.module#AnalyticsModule"
      },
      {
        path: "features",
        loadChildren: "./features/features.module#FeaturesModule"
      },
      {
        path: "notifications",
        loadChildren: "./notifications/notifications.module#NotificationsModule"
      },
      {
        path: "settings",
        loadChildren: "./settings/settings.module#SettingsModule"
      },
      {
        path: "journey",
        loadChildren: "./journey/journey.module#JourneyModule"
      }
    ]
  }

  // Not found
  // { path: "**", redirectTo: "home" }
];
