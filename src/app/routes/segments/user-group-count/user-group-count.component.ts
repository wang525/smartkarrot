import { Component, OnInit, OnDestroy } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { SettingsService } from "../../../core/settings/settings.service";
import { SegmentService } from "../../../core/service/segment/segment.service";
declare var $;

@Component({
  selector: "app-user-group-count",
  templateUrl: "./user-group-count.component.html",
  styleUrls: ["./user-group-count.component.scss"]
})
export class UserGroupCountComponent implements OnInit, OnDestroy {
  userList: any;
  userID = "";
  totalUserCount: any;
  isExcluded = "";
  path: any;
  excludedUserList: Array<any>=[];
  groupInfo: any;
  saveFlag = false;
  searchText: any;

  nextToken: any;
  moreUserList: any;

  constructor(
    public segmentService: SegmentService,
    public settings: SettingsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.settings.setLayoutSetting("sidebarHidden", true);
    this.settings.setLayoutSetting("topbarHidden", true);
  }

  ngOnInit() {
    this.path = this.route.params.subscribe(params => {
      this.isExcluded = params["id"];
    });

    this.groupInfo = this.segmentService.getUserGroupListData();
    console.log("group info ===", this.groupInfo)
    if (this.groupInfo) {
      let data = {
        appId: this.groupInfo.appId,
        groupId: this.groupInfo.groupId
      };
      
      /* get user group lists */
      this.segmentService.getUserGroupUserList(data).then(res => {
        console.log("res====>", res);
        this.totalUserCount = res["totalUserCount"];
        
        this.nextToken = res["nextToken"];
        this.showExcludedUsers(res);
        this.userList = res["users"];
        this.showDatatableContent();
      });      
    }

    $(function() {
      $(".includeState").on("click", function() {
        if ($(this).hasClass("active")) {
          $(this).removeClass("active");
          $(this).text("Include Back");
        } else {
          $(this).addClass("active");
          $(this).text("Exclude");
        }
      });
    });
  }

  showExcludedUsers(res) {
    if (res["excludedUserIds"] && res["excludedUserIds"].length > 0) {
      this.excludedUserList = res["excludedUserIds"];
    }
    else {
      this.excludedUserList = [];
    }
  }

  showDatatableContent() {
    $(function() {
      $("#data-table-content").DataTable({
        paging: false,
        info: false,
        scrollY: "calc(100vh - 130px)",
        scrollCollapse: true,
        searching: false,
        responsive: true
      });
    });
  }

  searchInUsercount(searchText) {
    if (this.saveFlag == false) {
      this.saveFlag = true;
      if (this.groupInfo) {
        let data: any;
        if (searchText != '') {
          data = {
            appId: this.groupInfo.appId,
            groupId: this.groupInfo.groupId,
            searchText: searchText
          };
        }
        else {
          data = {
            appId: this.groupInfo.appId,
            groupId: this.groupInfo.groupId
          };
        }
        
        this.segmentService.getUserGroupUserList(data).then(res => {
          this.saveFlag = false;
          this.userList = res["users"];
          this.showExcludedUsers(res);
        });
      }
    }
  }

  fetchMoreUsersInSegment($event) {
    if (this.saveFlag == false) {
      this.saveFlag = true;
      if (this.groupInfo) {
        let data = {
          appId: this.groupInfo.appId,
          groupId: this.groupInfo.groupId,
          nextToken: this.nextToken
        };
        
        this.segmentService.getUserGroupUserList(data).then(res => {
          this.saveFlag = false;
          if (res["excludedUserIds"] && res["excludedUserIds"].length > 0) {
            this.excludedUserList = this.excludedUserList.concat(res["excludedUserIds"]);
          }
          
          this.nextToken = res["nextToken"];
          this.totalUserCount = res["totalUserCount"];
          this.moreUserList = res["users"];
          this.userList = this.userList.concat(this.moreUserList);
          console.log("Total ", this.userList.length);
        });
      }
    }
  }

  ngOnDestroy() {
    this.settings.setLayoutSetting("sidebarHidden", false);
    this.settings.setLayoutSetting("topbarHidden", false);
  }

  /* Cancel function */
  setCancelFunc($event) {
    $event.preventDefault();
    let tmpdata = this.segmentService.getTmpUserListData();
    this.router.navigate(["/segments/usergrouplist"]);
  }

  /* Done function */
  setDoneFunc($event) {
    $event.preventDefault();
    if (this.saveFlag == false) {
      this.saveFlag = true;

      if (this.groupInfo.appId && this.groupInfo.groupId) {
        let saveData = {};

        saveData["appId"] = this.groupInfo.appId;
        saveData["userGroupName"] = this.groupInfo.groupName;
        saveData["source"] = this.groupInfo.source;
        saveData["isDraft"] = this.groupInfo.isDraft;
        saveData["excludedUserIds"] = this.excludedUserList;
        saveData["userGroupId"] = this.groupInfo.groupId;
        
        let users = [];
        if (this.userList) {
          this.userList.map(user => {
            users.push(user.userId);
          })
        }
        saveData["userIds"] = users;

        console.log("save data content==", saveData);
        this.segmentService.updateUserRule(saveData).then(res => {
          this.saveFlag = false;
          this.router.navigate(["/segments/usergrouplist"]);
        });
      }
    }
  }

  changeUserStatus(userInfo, status) {
    userInfo.isExcluded = status;
    if (status == true) {
      this.excludedUserList.push(userInfo.userId);
    }
    else {
      let resRemoveIdx = this.excludedUserList.indexOf(userInfo.userId);
      this.excludedUserList.splice(resRemoveIdx, 1);
    }
    console.log("this.excludedUserList===",this.excludedUserList)
  }
}
